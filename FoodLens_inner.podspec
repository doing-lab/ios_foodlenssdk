#
# Be sure to run `pod lib lint FoodLens.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name            = "FoodLens_inner"
  s.version         = "2.5.0.2Dev"
  s.summary         = "FoodLensSDK for iOS"
  s.description     = "FoodLens SDK provide functionlay for food recognition. FoodLens SDK consist of network API and UI API"
  s.homepage        = "http://www.doinglab.com"
  s.license         = { :type => "MIT", :file => "LICENSE" }
  s.author          = { "hyunsuk.lee@doinglab.com" => "leeint@gmail.com" }
  s.source          = { :git => "https://bitbucket.org/doing-lab/ios_foodlenssdk.git", :tag => s.version }
  s.swift_version   = '4.2'
  s.module_name     = 'FoodLens'
  s.platform        = :ios, '12.0'
  s.ios.deployment_target = '12.0'

  s.source_files = 'Sources/FoodLens/Classes/**/*.{swift,strings,xib,storyboard}'
  s.resources = 'Sources/FoodLens/Assets/*','Sources/FoodLens/Classes/images/*'
  
  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '4.2' }
  s.dependency 'Alamofire', '~> 5.5'
end
