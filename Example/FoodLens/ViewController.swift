//
//  ViewController.swift
//  FoodLens
//
//  Created by hyunsuk.lee@doinglab.com on 01/29/2019.
//  Copyright (c) 2019 hyunsuk.lee@doinglab.com. All rights reserved.
//

import UIKit
import FoodLens

class ViewController: UIViewController {
    let uiService = FoodLens.createUIService(accessToken: "<access Token here>")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func didAddStart(_ sender: Any) {
        let selectedColor : UIColor = UIColor(red:68/255, green:128/255, blue:238/255, alpha:1.0)
        let navTheme = NavigationBarTheme(foregroundColor : selectedColor, backgroundColor : selectedColor)
        let toolbarTheme = ToolBarButtonTheme(backgroundColor: UIColor.white, buttonTheme: ButtonTheme(backgroundColor: selectedColor, textColor: UIColor.white, borderColor: UIColor.clear))
        
        let buttonTheme = ButtonTheme(backgroundColor: selectedColor, textColor: selectedColor, borderColor: selectedColor)
        let widgetButtonTheme = ButtonTheme(backgroundColor: selectedColor, textColor: selectedColor, borderColor: selectedColor)
 
        // Options
        FoodLens.isEnableCameraOrientation = false
        FoodLens.isEnableManualInput = true
        FoodLens.isEnablePhtoGallery = true
        FoodLens.isUseImageRecordDate = true
        let calendar = Calendar.autoupdatingCurrent
        FoodLens.isSaveToGallery = false
        FoodLens.uiServiceMode = .userSelectedWithCandidates
        FoodLens.eatType = .lunch
        uiService.startUIService(parent: self, completionHandler: CallbackObject())
    }
    
    @available(iOS 10.0, *)
    @IBAction func didEditStart(_ sender: Any) {
        var mealData = PredictionResult()

        if (resultString == nil) {

            mealData = makeObjectFromString()


        } else {
            mealData = PredictionResult.create(json: resultString!)
        }

        FoodLens.eatType = .breakfast
        
        uiService.startEditUIService(mealData, parent: self, completionHandler: CallbackObject())
    }
    
    @IBAction func startWithoutDateandType(_ sender: Any) {
        uiService.startUIService(parent: self, completionHandler: CallbackObject())
    }
    
    @IBAction func didTapSearchFoodBUtton(_ sender: Any) {
        uiService.startSearchUIService(parent: self, completionHandler: CallbackObject())
    }
    
    @IBAction func didTapShowGalleryButton(_ sender: Any) {
        uiService.startGalleryUIService(parent: self, completionHandler: CallbackObject())
    }
    
    func makeObjectFromString() -> PredictionResult {
        /*let jsonString = "{\"eatDate\":\"2019-07-11 10:53:14\",\"eatType\":3,\"foodPositionList\":[{\"eatAmount\":1.0,\"foodCandidates\":[{\"foodId\":12155,\"foodName\":\"음식아님\",\"keyName\":\"\",\"nutrition\":{\"calcium\":0.0,\"calories\":0.0,\"carbonhydrate\":0.0,\"cholesterol\":0.0,\"dietrayfiber\":0.0,\"fat\":0.0,\"protein\":0.0,\"rawCalories\":0.0,\"rawTotalGram\":0.0,\"saturatedfat\":0.0,\"sodium\":0.0,\"sugar\":0.0,\"totalgram\":0.0,\"transfat\":0.0,\"unit\":\"0g\",\"vitamina\":0.0,\"vitaminb\":0.0,\"vitaminc\":0.0,\"vitamind\":0.0,\"vitamine\":0.0}},{\"foodId\":38,\"foodName\":\"쌀밥\",\"keyName\":\"\"},{\"foodId\":45,\"foodName\":\"찰밥\",\"keyName\":\"\"},{\"foodId\":1647,\"foodName\":\"시금치나물\",\"keyName\":\"\"},{\"foodId\":1648,\"foodName\":\"시금치나물(고추장무침)\",\"keyName\":\"\"},{\"foodId\":2053,\"foodName\":\"배추김치\",\"keyName\":\"\"},{\"foodId\":1605,\"foodName\":\"김구이\",\"keyName\":\"\"}],\"foodImagepath\":\"/data/user/0/com.doinglab.foodlens.sample/files/temp/0.jpg\",\"imagePosition\":{\"xmax\":1024,\"xmin\":0,\"ymax\":1023,\"ymin\":0},\"userSelectedFood\":{\"foodId\":12155,\"foodName\":\"음식아님\",\"nutrition\":{\"calcium\":0.0,\"calories\":0.0,\"carbonhydrate\":0.0,\"cholesterol\":0.0,\"dietrayfiber\":0.0,\"fat\":0.0,\"protein\":0.0,\"rawCalories\":0.0,\"rawTotalGram\":0.0,\"saturatedfat\":0.0,\"sodium\":0.0,\"sugar\":0.0,\"totalgram\":0.0,\"transfat\":0.0,\"unit\":\"0g\",\"vitamina\":0.0,\"vitaminb\":0.0,\"vitaminc\":0.0,\"vitamind\":0.0,\"vitamine\":0.0}}}],\"mealType\":\"snack\",\"predictedImagePath\":\"/data/user/0/com.doinglab.foodlens.sample/files/temp/orgimg.jpg\"}"*/
        
        let jsonString = "{\"eatDate\":\"2019-08-01 01:36:00+0000\",\"eatType\":1,\"foodPositionList\":[{\"eatAmount\":0.75,\"foodCandidates\":[{\"foodId\":12155,\"foodName\":\"음식아님\",\"keyName\":\"nonefood\"},{\"foodId\":38,\"foodName\":\"쌀밥\",\"keyName\":\"rice_streamed\"},{\"foodId\":45,\"foodName\":\"찰밥\",\"keyName\":\"rice_streamed\"},{\"foodId\":2053,\"foodName\":\"배추김치\",\"keyName\":\"kimchi\"},{\"foodId\":1605,\"foodName\":\"김구이\",\"keyName\":\"gim_gui\"},{\"foodId\":1572,\"foodName\":\"삶은계란\",\"keyName\":\"eggs_boiled\"},{\"foodId\":1573,\"foodName\":\"삶은계란(흰자)\",\"keyName\":\"eggs_boiled\"},{\"foodId\":1571,\"foodName\":\"삶은계란(노른자)\",\"keyName\":\"eggs_boiled\"}],\"foodImagepath\":\"/var/mobile/Containers/Data/Application/B27B61A3-9E5F-46F4-8852-BFB3F98A445D/Documents/foodlensStore/b93c84a16c84221383b929c8d60cf1ac96a1c833cd62a258cae2db13ff66cc91.jpg\",\"imagePosition\":{\"xmax\":1024,\"xmin\":0,\"ymax\":1023,\"ymin\":0},\"userSelectedFood\":{\"foodId\":79,\"foodName\":\"순대국밥\",\"nutrition\":{\"calcium\":169,\"calories\":773,\"carbonhydrate\":103.8,\"cholesterol\":2516,\"dietrayfiber\":2516,\"fat\":26.8,\"protein\":25.9,\"Calories\":773,\"TotalGram\":645,\"saturatedfat\":0.448,\"sodium\":180.3,\"sugar\":1.8,\"totalgram\":645,\"transfat\":0,\"unit\":\"1인분\",\"vitamina\":25,\"vitaminb\":0,\"vitaminc\":8,\"vitamind\":0,\"vitamine\":0}}}],\"mealType\":\"lunch\",\"predictedImagePath\":\"/var/mobile/Containers/Data/Application/B27B61A3-9E5F-46F4-8852-BFB3F98A445D/Documents/foodlensStore/970eea02697b95bd76ef1a526bd1fd23f0c4a99cb9c3e253f0c021904497fa44.jpg\"}"
        
        let predictResult = PredictionResult.create(json: jsonString)
        return predictResult
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

func predictResult(result : PredictionResult?, status : ProcessStatus) {
    print(status)
    
}

func serarchresult(data :  [FoodSearchList]) {
    for item in data {
        print(item.id)
        print(item.foodname)
 
    }
}

var resultString:String? = nil

class CallbackObject : UserServiceResultHandler {
    func onSuccess(_ result : RecognitionResult) {
        //NSLog("\(result.getDate())")
        //NSLog("\(result.getMealType())")
        //NSLog("\(result.getFoodPositions())")
        resultString = result.toJSONString()!
        debugPrint("\(resultString)")
        
        
        let LocaleCurrent = Locale.current

        debugPrint("FoodLense success LocaleCurrent : \(LocaleCurrent)")
        NSLog("Success")
    }
    
    func onCancel() {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            let alertController = UIAlertController(title: "FoodLens", message: "Canceled", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            
            }
            alertController.addAction(okAction)
            topController.present(alertController, animated: true, completion: nil)
        }
        
        NSLog("Cancel")
    }
    
    func onError(_ error :BaseError) {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            let alertController = UIAlertController(title: "FoodLens", message: "Error", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                
            }
            alertController.addAction(okAction)
            topController.present(alertController, animated: true, completion: nil)
        }
        NSLog("Error")
    }
}
