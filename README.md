# FoodLens

<!-- [![CI Status](https://img.shields.io/travis/hyunsuk.lee@doinglab.com/FoodLens.svg?style=flat)](https://travis-ci.org/hyunsuk.lee@doinglab.com/FoodLens)
[![Version](https://img.shields.io/cocoapods/v/FoodLens.svg?style=flat)](https://cocoapods.org/pods/FoodLens)
[![License](https://img.shields.io/cocoapods/l/FoodLens.svg?style=flat)](https://cocoapods.org/pods/FoodLens)
[![Platform](https://img.shields.io/cocoapods/p/FoodLens.svg?style=flat)](https://cocoapods.org/pods/FoodLens) -->
## This is FoodLens iOS SDK

## Requirements

* iOS Ver 12.0 or higher
* Swift Version 4.2 or higher

## Documents  
[API Documents](https://doinglab.github.io/ios/index.html)

## Author

hyunsuk.lee@doinglab.com

## License

FoodLens is available under the MIT license. See the LICENSE file for more info.
