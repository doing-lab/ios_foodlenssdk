//
//  UIViewExtension.swift
//  Alamofire
//
//  Created by HwangChun Lee on 19/04/2019.
//

import UIKit
extension UIView {
    func createImage() -> UIImage? {
        let rect: CGRect = self.frame
        UIGraphicsBeginImageContext(rect.size)
        let context: CGContext = UIGraphicsGetCurrentContext()!
        self.layer.render(in: context)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img
    }
}
