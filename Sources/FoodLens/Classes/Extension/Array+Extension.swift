//
//  Array+Extension.swift
//  FoodLens
//
//  Created by 박병호 on 2023/10/16.
//

import Foundation

extension Collection {

    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
