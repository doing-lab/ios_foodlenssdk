//
//  UIImageExtension.swift
//  Alamofire
//
//  Created by HwangChun Lee on 19/04/2019.
//

import UIKit

extension UIImage {
    static func from(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    func resize(maxLengthOfShortSide: Double) -> UIImage {
        if self.size.width <= maxLengthOfShortSide, self.size.height <= maxLengthOfShortSide {
            return self
        }

        var newWidth: Double = 0.0
        var newHeight: Double = 0.0

        if self.size.width < self.size.height {
            let scale = maxLengthOfShortSide / self.size.width
            newHeight = self.size.height * scale
            newWidth = maxLengthOfShortSide
        } else {
            let scale = maxLengthOfShortSide / self.size.height
            newWidth = self.size.width * scale
            newHeight = maxLengthOfShortSide
        }

        let newSize = CGSize(width: newWidth, height: newHeight)
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage ?? self
    }
    
    func removeRotationForImage(orientation : UIImage.Orientation) -> UIImage? {
        let image = self
        let tempImage = UIImage(cgImage: image.cgImage!, scale: CGFloat(1.0), orientation: orientation)
        UIGraphicsBeginImageContextWithOptions(tempImage.size, false, tempImage.scale)
        tempImage.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: tempImage.size))
        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return normalizedImage;
    }
    
    func cropToBounds(posX: CGFloat, posY : CGFloat, width: CGFloat, height: CGFloat) -> UIImage {
        let image = self
        let cgwidth: CGFloat = CGFloat(width)
        let cgheight: CGFloat = CGFloat(height)
        let rect: CGRect = CGRect(x: posX, y: posY, width: cgwidth, height: cgheight)
        guard let imageRef = image.cgImage!.cropping(to: rect) else { return UIImage() }
        let ret_image: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)

        return ret_image
    }
    
    func cropToBounds(width: CGFloat, height: CGFloat) -> UIImage {
        let image = self
        let contextImage: UIImage = UIImage.init(cgImage: image.cgImage!)
        
        let contextSize: CGSize = contextImage.size //CGSize(width: image.size.width, height: image.size.height)//contextImage.size
        
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        let cgwidth: CGFloat = CGFloat(width)
        let cgheight: CGFloat = CGFloat(height)
        
        // See what size is longer and create the center off of that
        /*if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
        }*/

        if contextSize.width > cgwidth {
            posX = ((contextSize.width - cgwidth) / 2)
        }
        if contextSize.height > cgheight {
            posY = ((contextSize.height - cgheight) / 2)
        }
        let rect: CGRect = CGRect(x: posX, y: posY, width: cgwidth, height: cgheight)
        let imageRef = image.cgImage!.cropping(to: rect)
        let resultImage: UIImage = UIImage(cgImage: imageRef!, scale: image.scale, orientation: image.imageOrientation)
        
        return resultImage
    }
    
    func resizeImage(targetSize: CGSize) -> UIImage {
        let image = self
        let size = image.size
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize (width: size.width * heightRatio, height : size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height :  size.height * widthRatio)
        }
        //newSize = CGSize(width: targetSize.width, height :  targetSize.height)
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func resizeImageWithBlank(targetSize : CGSize) -> UIImage? {
        let image = self
        let backgroundColor = UIColor.white
        UIGraphicsBeginImageContextWithOptions(targetSize, false, 0)
        
        let xMin = (targetSize.width - image.size.width) / 2
        let yMin = (targetSize.height - image.size.height) / 2
        
        let areaSize = CGRect(x: 0, y: 0, width : targetSize.width, height : targetSize.height)
        let imageSize = CGRect(x: xMin, y: yMin, width: image.size.width, height: image.size.height)
        backgroundColor.setFill()
        UIRectFill(areaSize)
        image.draw(in: imageSize, blendMode: CGBlendMode.normal, alpha: 1.0)
        let newImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
}
