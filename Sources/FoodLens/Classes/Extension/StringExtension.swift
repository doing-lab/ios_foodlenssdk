//
//  StringExtensions.swift
//  DietCameraAI
//
//  Created by HwangChun Lee on 16/01/2019.
//  Copyright © 2019 leeint. All rights reserved.
//

import UIKit

extension String{
    func getArrayAfterRegex(regex: String) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: self,
                                        range: NSRange(self.startIndex..., in: self))
            return results.map {
                String(self[Range($0.range, in: self)!])
            }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    var toDate: Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        var date = self
        if !hasTime(date: date) {
            date = self.toAddCurrentTime
        }
        return dateFormatter.date(from: date) ?? Date()
    }
    var toAddCurrentTime: String {
        var date = self
        date.append(" ")
        let todayTime = Date().getTime
        print(todayTime)
        date.append(todayTime)
        return date
    }
    
    func hasTime(date: String) -> Bool {
        let date = self
        let onlyDate = date.split(separator: " ")
        if onlyDate.count == 1 { // time is nil. only Date(yyyy-MM-dd)
            return false
        }
        return true
    }
    
    var getOnlyDate: Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.date(from: self) ?? Date()
    }
    
    func localized(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
        return NSLocalizedString(self, tableName: tableName, value: "**\(self)**", comment: "")
    }
}

extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String, size : CGFloat) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont.boldSystemFont(ofSize : size)]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
        let normal = NSAttributedString(string: text)
        append(normal)
        
        return self
    }
}
