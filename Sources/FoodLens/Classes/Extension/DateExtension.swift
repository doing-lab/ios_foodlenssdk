//
//  DateExtension.swift
//  FoodLens
//
//  Created by Eunjin on 2020/12/16.
//

import UIKit

extension Date {
    var toString: String {
        // 기본 설정 : selecte Date + 현재 시간
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: self)
    }
    var getTime: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = self
        let splitDate = date.toString.split(separator: " ")
        return String(splitDate[1])
    }
    var getOnlyDate: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: self)
    }
    var getDay: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d"
        return dateFormatter.string(from: self)
    }
    var getMonth: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLL"
        return dateFormatter.string(from: self)
    }
    var getYearAndWeeks: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyww"
        return dateFormatter.string(from: self)
    }
    var getWeekDay: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "e"
        return dateFormatter.string(from: self)
    }
}
