//
//  Services.swift
//  FoodLens
//
//  Created by HwangChun Lee on 01/04/2019.
//

import UIKit
import Photos
/**
 When you want to use UI served by FoodLens, use this class
 */
@available(iOS 10.0, *)
open class UIService {
    private static var userServiceCallback : UserServiceResultHandler? = nil
    private static let bundle: Bundle = foodlensResourceBundle
    
    private static let IMG_STORE_PATH:String = "foodlensStore";
    
    /**
     Start UI service from taking photoes or picking from gallery
     
     - parameter parent : view controller to return after UIService ended
     - parameter completionHandler : Delegation protocol to be called when recognition is succeed, failed, or canceled
    */

    @available(*, deprecated, message: "use `startCameraUIService(parent:completionHandler:)`")
    public func startUIService(
        parent : UIViewController,
        completionHandler : UserServiceResultHandler
    ) {
        self.showCameraView(parent, completionHandler: completionHandler)
    }
    
    public func startCameraUIService(
        parent : UIViewController,
        completionHandler : UserServiceResultHandler
    ) {
        self.showCameraView(parent, completionHandler: completionHandler)
    }
    
    public func startGalleryUIService(
        parent : UIViewController,
        completionHandler : UserServiceResultHandler
    ) {
        self.showGallery(parent, completionHandler: completionHandler)
    }
    
    public func startSearchUIService(
        parent : UIViewController,
        completionHandler : UserServiceResultHandler
    ) {
        self.showSearchFood(parent, completionHandler: completionHandler)
    }
    
    private func showCameraView(_ parent: UIViewController, completionHandler : UserServiceResultHandler) {
        UIService.userServiceCallback = completionHandler
        let navi = UIStoryboard(name:"Main", bundle: UIService.bundle).instantiateViewController(withIdentifier:"Start") as! UINavigationController
        navi.modalPresentationStyle = .fullScreen
        parent.present(navi, animated: true, completion: nil)
    }
    
    private func showGallery(_ parent: UIViewController, completionHandler : UserServiceResultHandler) {
        UIService.userServiceCallback = completionHandler
        guard let cameraViewController = UIStoryboard(name:"Main", bundle: UIService.bundle).instantiateViewController(withIdentifier: "CameraViewController") as? CameraViewController else {
            return
        }
        cameraViewController.isDirect = true

        if let navigationCV = UIStoryboard(name:"Main", bundle: UIService.bundle).instantiateViewController(withIdentifier:"Start") as? UINavigationController {
            navigationCV.modalPresentationStyle = .fullScreen
            navigationCV.viewControllers.removeAll()
            navigationCV.viewControllers = [cameraViewController]
            
            parent.present(navigationCV, animated: true, completion: nil)
        }
    }
    
    private func showSearchFood(_ parent: UIViewController, completionHandler : UserServiceResultHandler) {
        UIService.userServiceCallback = completionHandler
        guard let cameraViewController = UIStoryboard(name:"Main", bundle: UIService.bundle).instantiateViewController(withIdentifier: "CameraViewController") as? CameraViewController else {
            return
        }
        guard let foodSearchViewController = UIStoryboard(name:"Main", bundle: UIService.bundle).instantiateViewController(withIdentifier: "FoodSearchViewController") as? FoodSearchViewController else {
            return
        }
        foodSearchViewController.manualInputProtocal = cameraViewController
        foodSearchViewController.searchType = .addFood
        foodSearchViewController.isDirect = true

        if let navigationCV = UIStoryboard(name:"Main", bundle: UIService.bundle).instantiateViewController(withIdentifier:"Start") as? UINavigationController {
            navigationCV.modalPresentationStyle = .fullScreen
            navigationCV.viewControllers.removeAll()
            navigationCV.viewControllers = [cameraViewController, foodSearchViewController]
            parent.present(navigationCV, animated: true, completion: nil)
        }
    }
    
    /**
     Edit UI Service, if you intend to modify RecognitionResult, call this function
     
     - parameter mealData : Instance of PredictionResult to edit
     - parameter parent : view controller to return after UIService ended
     - parameter completionHandler : Delegation protocol to be called when recognition is succeed, failed, or canceled
     */
    public func startEditUIService(_ mealData: PredictionResult, parent : UIViewController, completionHandler : UserServiceResultHandler) {
        CameraViewController.predictResult = mealData
        
        UIService.userServiceCallback = completionHandler
        
        let (errorMsg, result) = mealData.validateDatas()
        
        if result == false {
            UIService.userServiceCallback?.onError(BaseError(message: errorMsg))
            return
        }
        
        if let imagePath = mealData.predictedImagePath?.split(separator: "/").last, let image = Util.load(path: UIService.IMG_STORE_PATH, fileName: String(imagePath)) {
            for index in 0 ..< mealData.foodPositionList.count {
                let xmin = mealData.foodPositionList[index].imagePosition?.xmin ?? 0
                let xmax = mealData.foodPositionList[index].imagePosition?.xmax ?? 0
                let ymin = mealData.foodPositionList[index].imagePosition?.ymin ?? 0
                let ymax = mealData.foodPositionList[index].imagePosition?.ymax ?? 0
                
                if xmax - xmin > 0 && ymax - ymin > 0 {
                    let filename : String = "parted_\(index)"
                    let showImg:UIImage = image.cropToBounds(posX: CGFloat(xmin), posY: CGFloat(ymin), width: CGFloat(xmax - xmin), height: CGFloat(ymax - ymin))
                    let partName = Util.save(image: showImg, path: UIService.IMG_STORE_PATH, fileName: filename)
                    mealData.foodPositionList[index].foodImagepath = partName
                }
            }
        }
        
        let navi = UIStoryboard(name:"Main", bundle:UIService.bundle).instantiateViewController(withIdentifier:"Edit") as! UINavigationController
        parent.present(navi, animated: true, completion: nil)
    }
    
    internal static func setCompletion(completion : UserServiceResultHandler) {
        userServiceCallback = completion
    }
    
    internal static func getCompletion() -> UserServiceResultHandler? {
        return userServiceCallback
    }
}
