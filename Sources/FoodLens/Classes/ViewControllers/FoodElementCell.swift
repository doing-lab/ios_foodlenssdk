//
//  FoodElementCell.swift
//  DietCameraAI
//
//  Created by HwangChun Lee on 31/12/2018.
//  Copyright © 2018 leeint. All rights reserved.
//

import UIKit

@available(iOS 10.0, *)
class FoodElementCell: UITableViewCell {
    @IBOutlet weak var foodImage: DesignableImageView!
    @IBOutlet weak var foodName: UILabel!
    @IBOutlet weak var unit: UILabel!
    var positionId : Int = 0
    var foodInfoId : Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.white
        self.selectedBackgroundView = backgroundView
        
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
