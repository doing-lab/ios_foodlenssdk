//
//  TotalNutritionViewController.swift
//  DietCameraAI
//
//  Created by HwangChun Lee on 16/11/2018.
//  Copyright © 2018 leeint. All rights reserved.
//

import UIKit

@available(iOS 10.0, *)
class TotalNutritionViewController: UITableViewController {
    @IBOutlet weak var servingSize: UILabel!
    @IBOutlet weak var totalCalorie: UILabel!
    @IBOutlet weak var totalFatGram: UILabel!
    @IBOutlet weak var saturatedFatGram: UILabel!
    @IBOutlet weak var transFatGram: UILabel!
    @IBOutlet weak var cholesterolGram: UILabel!
    @IBOutlet weak var sodiumGram: UILabel!
    @IBOutlet weak var carbsGram: UILabel!
    @IBOutlet weak var sugarGram: UILabel!
    @IBOutlet weak var proteinGram: UILabel!
    
    
    @IBOutlet weak var totalFatPercent: UILabel!
    @IBOutlet weak var saturatedFatPercent: UILabel!
    @IBOutlet weak var transFatPercent: UILabel!
    @IBOutlet weak var cholesterolPercent: UILabel!
    @IBOutlet weak var sodiumPercent: UILabel!
    @IBOutlet weak var totalCarbPercent: UILabel!
    @IBOutlet weak var sugarPercent: UILabel!
    @IBOutlet weak var proteinPercent: UILabel!
    
    @IBOutlet weak var totalFatText: UILabel!
    @IBOutlet weak var saturatedFatText: UILabel!
    @IBOutlet weak var transFatText: UILabel!
    @IBOutlet weak var cholesterolText: UILabel!
    @IBOutlet weak var sodiumText: UILabel!
    @IBOutlet weak var totalCarbsText: UILabel!
    @IBOutlet weak var sugarText: UILabel!
    @IBOutlet weak var proteinText: UILabel!
    
    let bundle: Bundle = foodlensResourceBundle
    
    let foodAmount : Float = 1.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.translatesAutoresizingMaskIntoConstraints = false
        self.accessibilityScroll(.down)
        initializeLabels()
        
        let foodinfo = makeTotalFoodInfo()
        getFoodInfoResult(foodInfo : foodinfo)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //light 모드로 강제진행
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = .white
            self.navigationController?.navigationBar.standardAppearance = appearance;
            self.navigationController?.navigationBar.scrollEdgeAppearance = self.navigationController?.navigationBar.standardAppearance

        }
    }
    
    func makeTotalFoodInfo() -> Nutrition {
        if #available(iOS 10.0, *) {
            let total : Nutrition = CameraViewController.predictResult.foodPositionList
                .filter { $0.userSelectedFood != nil }
                .compactMap { $0.userSelectedFood?.nutrition }
                .reduce(Nutrition()) { (result, food) -> Nutrition in
                    var reduceResult : Nutrition = result
                    reduceResult.calcium += food.calcium
                    reduceResult.calories += food.calories
                    reduceResult.carbonhydrate += food.carbonhydrate
                    reduceResult.cholesterol += food.cholesterol
                    reduceResult.dietrayfiber += food.dietrayfiber
                    reduceResult.fat += food.fat
                    reduceResult.protein += food.protein
                    reduceResult.saturatedfat += food.saturatedfat
                    reduceResult.sodium += food.sodium
                    reduceResult.sugar += food.sugar
                    reduceResult.totalgram += food.totalgram
                    reduceResult.transfat += food.transfat
                    reduceResult.vitamina += food.vitamina
                    reduceResult.vitaminb += food.vitaminb
                    reduceResult.vitaminc += food.vitaminc
                    reduceResult.vitamind += food.vitamind
                    reduceResult.vitamine += food.vitamine
                    return reduceResult
                }
            
            return total
        } else {
            // Fallback on earlier versions
            return Nutrition()
        }
    }
    
    func resolutionFontSize(size : CGFloat) -> CGFloat {
        if UIScreen.main.bounds.width >= 375 {
            return size
        }
        
        let size_formatter = size / 375
        let result = UIScreen.main.bounds.width * size_formatter
        return result
    }
    
    func initializeLabels() {
        var fontSize = resolutionFontSize(size: servingSize.font.pointSize)
        servingSize.font = servingSize.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: totalCalorie.font.pointSize)
        totalCalorie.font = totalCalorie.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: totalFatGram.font.pointSize)
        totalFatGram.font = totalFatGram.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: totalFatPercent.font.pointSize)
        totalFatPercent.font = totalFatPercent.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: saturatedFatGram.font.pointSize)
        saturatedFatGram.font = saturatedFatGram.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: saturatedFatPercent.font.pointSize)
        saturatedFatPercent.font = saturatedFatPercent.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: transFatGram.font.pointSize)
        transFatGram.font = transFatGram.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: transFatPercent.font.pointSize)
        transFatPercent.font = transFatPercent.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: cholesterolGram.font.pointSize)
        cholesterolGram.font = cholesterolGram.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: cholesterolPercent.font.pointSize)
        cholesterolPercent.font = cholesterolPercent.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: sodiumGram.font.pointSize)
        sodiumGram.font = sodiumGram.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: sodiumPercent.font.pointSize)
        sodiumPercent.font = sodiumPercent.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: carbsGram.font.pointSize)
        carbsGram.font = carbsGram.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: totalCarbPercent.font.pointSize)
        totalCarbPercent.font = totalCarbPercent.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: sugarGram.font.pointSize)
        sugarGram.font = sugarGram.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: sugarPercent.font.pointSize)
        sugarPercent.font = sugarPercent.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: proteinGram.font.pointSize)
        proteinGram.font = proteinGram.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: proteinPercent.font.pointSize)
        proteinPercent.font = proteinPercent.font.withSize(fontSize)
        
        fontSize = resolutionFontSize(size: totalFatText.font.pointSize)
        totalFatText.font = totalFatText.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: saturatedFatText.font.pointSize)
        saturatedFatText.font = saturatedFatText.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: transFatText.font.pointSize)
        transFatText.font = transFatText.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: cholesterolText.font.pointSize)
        cholesterolText.font = cholesterolText.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: sodiumText.font.pointSize)
        sodiumText.font = sodiumText.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: totalCarbsText.font.pointSize)
        totalCarbsText.font = totalCarbsText.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: sugarText.font.pointSize)
        sugarText.font = sugarText.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: proteinText.font.pointSize)
        proteinText.font = proteinText.font.withSize(fontSize)
    }
    
    func getFoodInfoResult(foodInfo : Nutrition?) {
        if foodInfo == nil {
            return
        }

        let recCarb: Float = 324
        let recPro: Float = 55
        let recFat: Float = 54
        let recSatuFat: Float = 15
        let recTransFat: Float = 2
        let recSodium: Float = 2000
        let recChole: Float = 300
        let recSugar: Float = 100
        
        servingSize.text = String(format:"%.1fg", (foodInfo?.totalgram)!*foodAmount)
        totalCalorie.text = String(format:"%.0f", (foodInfo?.calories)!*foodAmount)
        totalFatGram.text = String(format:"(%.1fg)", (foodInfo?.fat)!*foodAmount)
        totalFatPercent.text = String(format:"%.0f", ((foodInfo?.fat)!*foodAmount/recFat*100))
        saturatedFatGram.text = String(format:"(%.1fg)", (foodInfo?.saturatedfat)!*foodAmount)
        saturatedFatPercent.text = String(format:"%.0f", ((foodInfo?.saturatedfat)!*foodAmount/recSatuFat*100))
        transFatGram.text = String(format:"(%.1fg)", (foodInfo?.transfat)!*foodAmount)
        transFatPercent.text = String(format:"%.0f", ((foodInfo?.transfat)!*foodAmount/recTransFat*100))
        cholesterolGram.text = String(format:"(%.1fmg)", (foodInfo?.cholesterol)!*foodAmount)
        cholesterolPercent.text = String(format:"%.0f", ((foodInfo?.cholesterol)!*foodAmount/recChole*100))
        sodiumGram.text = String(format:"(%.1fmg)", (foodInfo?.sodium)!*foodAmount)
        sodiumPercent.text = String(format:"%.0f", ((foodInfo?.sodium)!*foodAmount/recSodium*100))
        carbsGram.text = String(format:"(%.1fg)", (foodInfo?.carbonhydrate)!*foodAmount)
        totalCarbPercent.text = String(format:"%.0f", ((foodInfo?.carbonhydrate)!*foodAmount/recCarb*100))
        sugarGram.text = String(format:"(%.1fg)", (foodInfo?.sugar)!*foodAmount)
        sugarPercent.text = String(format:"%.0f", ((foodInfo?.sugar)!*foodAmount/recSugar*100))
        proteinGram.text = String(format:"(%.1fg)", (foodInfo?.protein)!*foodAmount)
        proteinPercent.text = String(format:"%.0f", ((foodInfo?.protein)!*foodAmount/recPro*100))
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
