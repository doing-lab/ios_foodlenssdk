//
//  DetailNutritionTableViewController.swift
//  DietCameraAI
//
//  Created by HwangChun Lee on 16/11/2018.
//  Copyright © 2018 leeint. All rights reserved.
//

import UIKit

@available(iOS 10.0, *)
class DetailNutritionTableViewController: UITableViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    private let IMG_STORE_PATH:String = "foodlensStore";
    var foodData : FoodItemInfo? = nil
    
    var isFromPredict : Bool = false
    
    var nutrition : [Int:FoodInfo] = [:]
    var foodImageList : [UIImage] = []
    let bundle: Bundle = foodlensResourceBundle
    lazy var noPicImage : UIImage =  {
        UIImage(named: "ico_no_pic", in: self.bundle, compatibleWith: nil)!
    }()
    var selectedIndex : Int = 0
    
    @IBOutlet weak var servingSize: UILabel!
    @IBOutlet weak var totalCalorie: UILabel!
    
    @IBOutlet weak var totalFatGram: UILabel!
    @IBOutlet weak var saturatedFatGram: UILabel!
    @IBOutlet weak var transFatGram: UILabel!
    @IBOutlet weak var cholesterolGram: UILabel!
    @IBOutlet weak var sodiumGram: UILabel!
    @IBOutlet weak var totalCarbGram: UILabel!
    @IBOutlet weak var sugarGram: UILabel!
    @IBOutlet weak var proteinGram: UILabel!
    
    @IBOutlet weak var totalFatPercent: UILabel!
    @IBOutlet weak var saturatedFatPercent: UILabel!
    @IBOutlet weak var transFatPercent: UILabel!
    @IBOutlet weak var cholesterolPercent: UILabel!
    @IBOutlet weak var sodiumPercent: UILabel!
    @IBOutlet weak var totalCarbPercent: UILabel!
    @IBOutlet weak var sugarPercent: UILabel!
    @IBOutlet weak var proteinPercent: UILabel!
    
    @IBOutlet weak var foodNameLabel: UILabel!
    
    
    @IBOutlet weak var totalFatText: UILabel!
    @IBOutlet weak var saturatedFatText: UILabel!
    @IBOutlet weak var transFatText: UILabel!
    @IBOutlet weak var cholesterolText: UILabel!
    @IBOutlet weak var sodiumText: UILabel!
    @IBOutlet weak var totalCarbsText: UILabel!
    @IBOutlet weak var sugarText: UILabel!
    @IBOutlet weak var proteinText: UILabel!
    
    let positionList:[FoodPosition] = CameraViewController.predictResult.foodPositionList
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        initializeLabels()
        self.view.translatesAutoresizingMaskIntoConstraints = false
        //light 모드로 강제진행
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return positionList.count
    }
    
    func makeMultiPredictCellFromPredictDataSet(collectionView: UICollectionView, indexPath: IndexPath) -> FoodIconCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FoodCell", for: indexPath) as! FoodIconCell
        if indexPath.row < positionList.count {
            if let foodName = positionList[indexPath.row].userSelectedFood?.foodName, foodName.count > 0 {
                cell.foodName.text = foodName
            }
            
            if let imageFilename = positionList[indexPath.row].foodImagepath?.split(separator: "/").last {
                cell.foodImage.image = Util.load(path: IMG_STORE_PATH, fileName: String(imageFilename))
                cell.foodImage.contentMode = .scaleToFill
            }
            else {
                cell.foodImage.image = UIImage(named: "pdf_no_pic", in: bundle, compatibleWith: nil)
                cell.foodImage.contentMode = .scaleAspectFit
            }
            
            cell.makeImageViewRound()
            
            if indexPath.row == selectedIndex {
                cell.selectedImage()
                if let nutrition = positionList[indexPath.row].userSelectedFood?.nutrition {
                    let amount = positionList[indexPath.row].eatAmount
                    let foodname = positionList[indexPath.row].userSelectedFood?.foodName ?? ""
                    refreshNutritionView(nutritionInfo : nutrition, amount : amount, foodName : foodname)
                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return makeMultiPredictCellFromPredictDataSet(collectionView:collectionView, indexPath: indexPath)
    }
    
    func resolutionFontSize(size : CGFloat) -> CGFloat {
        if UIScreen.main.bounds.width >= 375 {
            return size
        }
        
        let size_formatter = size / 375
        let result = UIScreen.main.bounds.width * size_formatter
        return result
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        collectionView.reloadData()
    }
    
    func initializeLabels() {
        var fontSize = resolutionFontSize(size: servingSize.font.pointSize)
        servingSize.font = servingSize.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: totalCalorie.font.pointSize)
        totalCalorie.font = totalCalorie.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: totalFatGram.font.pointSize)
        totalFatGram.font = totalFatGram.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: totalFatPercent.font.pointSize)
        totalFatPercent.font = totalFatPercent.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: saturatedFatGram.font.pointSize)
        saturatedFatGram.font = saturatedFatGram.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: saturatedFatPercent.font.pointSize)
        saturatedFatPercent.font = saturatedFatPercent.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: transFatGram.font.pointSize)
        transFatGram.font = transFatGram.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: transFatPercent.font.pointSize)
        transFatPercent.font = transFatPercent.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: cholesterolGram.font.pointSize)
        cholesterolGram.font = cholesterolGram.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: cholesterolPercent.font.pointSize)
        cholesterolPercent.font = cholesterolPercent.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: sodiumGram.font.pointSize)
        sodiumGram.font = sodiumGram.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: sodiumPercent.font.pointSize)
        sodiumPercent.font = sodiumPercent.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: totalCarbGram.font.pointSize)
        totalCarbGram.font = totalCarbGram.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: totalCarbPercent.font.pointSize)
        totalCarbPercent.font = totalCarbPercent.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: sugarGram.font.pointSize)
        sugarGram.font = sugarGram.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: sugarPercent.font.pointSize)
        sugarPercent.font = sugarPercent.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: proteinGram.font.pointSize)
        proteinGram.font = proteinGram.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: proteinPercent.font.pointSize)
        proteinPercent.font = proteinPercent.font.withSize(fontSize)
        
        fontSize = resolutionFontSize(size: totalFatText.font.pointSize)
        totalFatText.font = totalFatText.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: saturatedFatText.font.pointSize)
        saturatedFatText.font = saturatedFatText.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: transFatText.font.pointSize)
        transFatText.font = transFatText.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: cholesterolText.font.pointSize)
        cholesterolText.font = cholesterolText.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: sodiumText.font.pointSize)
        sodiumText.font = sodiumText.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: totalCarbsText.font.pointSize)
        totalCarbsText.font = totalCarbsText.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: sugarText.font.pointSize)
        sugarText.font = sugarText.font.withSize(fontSize)
        fontSize = resolutionFontSize(size: proteinText.font.pointSize)
        proteinText.font = proteinText.font.withSize(fontSize)
    }
    
    func refreshNutritionView(nutritionInfo : Nutrition, amount : Float, foodName : String) {        
        foodNameLabel.text = foodName
        
        let recCarb: Float = 324
        let recPro: Float = 55
        let recFat: Float = 54
        let recSatuFat: Float = 15
        let recTransFat: Float = 2
        let recSodium: Float = 2000
        let recChole: Float = 300
        let recSugar: Float = 100
        

        servingSize.text = String(format: "%.1fg", nutritionInfo.totalgram * amount)
        totalCalorie.text = String(format: "%.0f", nutritionInfo.calories * amount)
        
        totalFatGram.text = String(format:"(%.1fg)", nutritionInfo.fat * amount)
        saturatedFatGram.text = String(format:"(%.1fg)", nutritionInfo.saturatedfat * amount)
        transFatGram.text = String(format: "(%.1fg)", nutritionInfo.transfat * amount)
        cholesterolGram.text = String(format: "(%.1fmg)", nutritionInfo.cholesterol * amount)
        sodiumGram.text = String(format:"(%.1fmg)", nutritionInfo.sodium * amount)
        totalCarbGram.text = String(format:"(%.1fg)", nutritionInfo.carbonhydrate * amount)
        sugarGram.text = String(format:"(%.1fg)", nutritionInfo.sugar * amount)
        proteinGram.text = String(format:"(%.1fg)", nutritionInfo.protein * amount)

        
        totalFatPercent.text = String(format:"%.0f", (nutritionInfo.fat * amount/recFat*100))
        saturatedFatPercent.text = String(format:"%.0f", (nutritionInfo.saturatedfat * amount/recSatuFat*100))
        transFatPercent.text = String(format:"%.0f", (nutritionInfo.transfat * amount/recTransFat*100))
        cholesterolPercent.text = String(format:"%.0f", (nutritionInfo.cholesterol * amount/recChole*100))
        sodiumPercent.text = String(format:"%.0f", (nutritionInfo.sodium * amount/recSodium*100))
        totalCarbPercent.text = String(format:"%.0f", (nutritionInfo.carbonhydrate * amount/recCarb*100))
        sugarPercent.text = String(format:"%.0f", (nutritionInfo.sugar * amount/recSugar*100))
        proteinPercent.text = String(format:"%.0f", (nutritionInfo.protein * amount/recPro*100))
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
