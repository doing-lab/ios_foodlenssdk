//
//  FoodIconCell.swift
//  DietCameraAI
//
//  Created by HwangChun Lee on 16/11/2018.
//  Copyright © 2018 leeint. All rights reserved.
//

import UIKit

@available(iOS 10.0, *)
class FoodIconCell: UICollectionViewCell {
    @IBOutlet weak var foodName: UILabel!
    @IBOutlet weak var foodImage: UIImageView!
    
    let selectedColor : UIColor = UIColor(red:0.80, green:0.17, blue:0.18, alpha:1.0)
    
    override func awakeFromNib() {
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
    }
    
    func makeImageViewRound() {
        foodImage.layer.borderColor = UIColor.clear.cgColor
        foodImage.layer.borderWidth = 1.0
        foodImage.layer.cornerRadius = foodImage.bounds.width / 2
        foodImage.layer.masksToBounds = true
        foodImage.clipsToBounds = true
    }
    
    func selectedImage() {
        foodImage.layer.borderColor = selectedColor.cgColor
        foodImage.layer.borderWidth = 2.0
        foodImage.layer.cornerRadius = foodImage.bounds.width / 2
        foodImage.layer.masksToBounds = true
        foodImage.clipsToBounds = true
    }
}
