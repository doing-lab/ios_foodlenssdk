//
//  AmountFrameViewController.swift
//  DietCameraAI
//
//  Created by HwangChun Lee on 10/04/2019.
//  Copyright © 2019 leeint. All rights reserved.
//

import UIKit

class AmountFrameViewController: UIViewController {
    private let IMG_STORE_PATH:String = "foodlensStore";
    
    var isUpdate : Bool = true
    weak var amountView : AmountViewController? = nil
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var doneButton: UIButton!
    
    @IBOutlet var deleteWarning: DesignableLabel!
    @IBOutlet var premiumPopup: DesignableLabel!
    @IBOutlet weak var backButton: UIButton!
    
    var isDirect: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //light 모드로 강제진행
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        
        // 버튼 셋팅
        backButton.tintColor = FoodLens.navigationBarTheme.foregroundColor
        bottomView.backgroundColor = FoodLens.toolbarButtonTheme.backgroundColor
        
        //donebutton
        doneButton.backgroundColor = FoodLens.toolbarButtonTheme.buttonTheme.backgroundColor
        //평소 color
        doneButton.setTitleColor(FoodLens.toolbarButtonTheme.buttonTheme.textColor, for: .normal)
        //selected color
        doneButton.setTitleColor(FoodLens.toolbarButtonTheme.buttonTheme.textColor, for: .selected)
        
        setNavigationBarTransparent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let image = self.isDirect ? FoodLens.navigationBarTheme.closeImage : FoodLens.navigationBarTheme.backImage
        backButton.setImage(image, for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if FoodLens.isShowHelp == true {
            self.performSegue(withIdentifier: "AmountHelp", sender: nil)
        }
    }
    
    func showDeleteWarning() {
        deleteWarning.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        deleteWarning.textColor = UIColor.white
        deleteWarning.textAlignment = .center;
        deleteWarning.alpha = 1.0
        deleteWarning.layer.cornerRadius = 15;
        deleteWarning.clipsToBounds  =  true
        deleteWarning.alpha = 1.0
        
        view.bringSubviewToFront(premiumPopup)
        
        deleteWarning.sizeToFit()
        deleteWarning.frame.origin.x = (UIScreen.main.bounds.width / 2) - (deleteWarning.frame.size.width / 2)
        deleteWarning.frame.size.width += 20
        deleteWarning.frame.size.height = 30
        
        deleteWarning.frame.origin.y = 0
        
        bottomView.addSubview(deleteWarning)
        bottomView.bringSubviewToFront(deleteWarning)
        
        UIView.animate(withDuration: 4.0, delay: 0.0, options: .curveEaseOut, animations: {
            self.deleteWarning.alpha = 0.0
        }, completion: {(isCompleted) in
            self.deleteWarning.removeFromSuperview()
        })
    }
    
    func setNavigationBarTransparent() {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.from(color: UIColor.clear), for: .topAttached, barMetrics: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    func setNavigationBarColor(color : UIColor) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.from(color: color), for: .topAttached, barMetrics: .default)
        /*if color.cgColor.alpha == 1 {
            self.navigationController?.navigationBar.isTranslucent = false
        }
        else {
            self.navigationController?.navigationBar.isTranslucent = true
        }*/
    }
    
    func showToast(message : String) {
        premiumPopup.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        premiumPopup.textColor = UIColor.white
        premiumPopup.textAlignment = .center;
        premiumPopup.text = message
        premiumPopup.alpha = 1.0
        premiumPopup.layer.cornerRadius = 15;
        premiumPopup.clipsToBounds  =  true
        premiumPopup.alpha = 1.0
        
        view.bringSubviewToFront(premiumPopup)
        
        premiumPopup.frame.size.width = self.bottomView.frame.size.width - 40
        premiumPopup.frame.origin.x = (self.bottomView.frame.size.width / 2) - (premiumPopup.frame.size.width / 2)
        //premiumPopup.frame.origin.y = screen.height - premiumPopup.bounds.height
        
        self.bottomView.addSubview(premiumPopup)
        self.bottomView.bringSubviewToFront(premiumPopup)
        
        UIView.animate(withDuration: 4.0, delay: 0.0, options: .curveEaseOut, animations: {
            self.premiumPopup.alpha = 0.0
        }, completion: {(isCompleted) in
            self.premiumPopup.removeFromSuperview()
        })
    }
    
    @IBAction func didBackPressed(_ sender: Any) {
        if self.isDirect {
            dismiss(animated: true)
        }
        
        if let naviCon = self.navigationController {
            FoodLens.eatType = nil
            FoodLens.eatDate = nil

            if naviCon.viewControllers.count <= 1 {
                //dismiss(animated: true, completion: nil)

                dismiss(animated: true) {
                    if let completion = UIService.getCompletion() {
                        completion.onCancel()
                    }
                }
            }
            else {
                naviCon.popViewController(animated: true)
            }
        }
        
        if isUpdate == false {
            //sendAddFoodLog(param1 : 1)
        }
    }
    
    @IBAction func didDonePressed(_ sender: Any) {
        //insertPredictionLog
        #if DEBUG
        //do nothing, do not send the log
        #else
        sendPredictionLog()
        #endif
        
        CameraViewController.predictResult.foodPositionList =
            CameraViewController.predictResult.foodPositionList.filter{ $0.isDelete != true }
        if FoodLens.uiServiceMode == .userSelectedOnly {
            let count:Int = CameraViewController.predictResult.foodPositionList.count
            for i in 0 ..< count {
                let selectedFoodId = CameraViewController.predictResult.foodPositionList[i].userSelectedFood?.foodId
                let list = CameraViewController.predictResult.foodPositionList[i].foodCandidates
                CameraViewController.predictResult.foodPositionList[i].foodCandidates = list.filter{ $0.foodId == selectedFoodId }
            }
        }
        
        /*if FoodLens.isSaveToGallery == true {
            UIImageWriteToSavedPhotosAlbum(, nil, nil, nil)
        }*/
        
        if FoodLens.isSaveToGallery == true {
            if let imagePath = CameraViewController.predictResult.predictedImagePath?.split(separator: "/").last,
                let fullImage = Util.load(path: IMG_STORE_PATH, fileName: String(imagePath)) {
                UIImageWriteToSavedPhotosAlbum(fullImage, nil, nil, nil)
            }
        }
        
        FoodLens.eatType = nil
        FoodLens.eatDate = nil
        
        self.dismiss(animated: true) {
            if let callback = UIService.getCompletion() {
                self.doneButton.isEnabled = false
                callback.onSuccess(CameraViewController.predictResult)
            }
        }
    }
    
    func sendPredictionLog() {
        var predictionLog = PostPredictionLog()
        predictionLog.full_image_path = CameraViewController.predictResult.fullImageS3Path ?? ""
        predictionLog.device_id = FoodLens.deviceId
        predictionLog.sdk_version = FoodLens.sdkVersion
        predictionLog.loglist = CameraViewController.predictResult.getFoodPositions().map {
            PredictionLogUnit(foodPosition: $0)
        }
        HttpHelper.insertPredictionLog(predictionLog: predictionLog)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "AmountView" {
            if let destVC = segue.destination as? AmountViewController {
                //destVC.selectedIndex = sender as! Int
                amountView = destVC
                destVC.frameView = self
            }
        }
    }
}
