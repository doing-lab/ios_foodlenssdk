//
//  AmountInputViewController.swift
//  DietCameraAI
//
//  Created by HwangChun Lee on 02/01/2019.
//  Copyright © 2019 leeint. All rights reserved.
//

import UIKit

@available(iOS 10.0, *)
class AmountInputViewController: CustomTabbarVC {
    private let IMG_STORE_PATH:String = "foodlensStore";
    
    var changeLogSended : Bool = false
    
    let normalColor : UIColor = UIColor(red:0.13, green:0.13, blue:0.13, alpha:1.0)
    let selectedColor : UIColor = UIColor(red:0.80, green:0.17, blue:0.18, alpha:1.0)
    
    let normalTabColor : UIColor = UIColor(red:0.53, green:0.53, blue:0.53, alpha:1.0)
    let selectedTabColor : UIColor = UIColor(red:0.20, green:0.20, blue:0.20, alpha:1.0)
    
    weak var simpleInputViewController : SimpleInputViewController? = nil
    weak var detailInputViewController : DetailInputViewController? = nil
    
    let bundle: Bundle = foodlensResourceBundle
    
    weak var superViewController : AmountViewController? = nil
    
    override func viewDidLoad() {
        let simpleInput = UIStoryboard(name:"Main", bundle:bundle).instantiateViewController(withIdentifier:"SimpleAmountInput") as! SimpleInputViewController
        let detailInput = UIStoryboard(name:"Main", bundle:bundle).instantiateViewController(withIdentifier:"DetailAmountInput") as! DetailInputViewController
        
        simpleInput.superViewController = superViewController
        detailInput.superViewController = superViewController
        
        simpleInputViewController = simpleInput
        detailInputViewController = detailInput
        
        self.viewControllers = [simpleInput, detailInput]
        let simpleInputTitle:String = NSLocalizedString("simple_input", tableName: nil, bundle: bundle, value: "", comment: "")
        let detailInputTitle:String = NSLocalizedString("detail_input", tableName: nil, bundle: bundle, value: "", comment: "")
        self.customMenuBar = .init(titles: [simpleInputTitle, detailInputTitle])
        self.customMenuBar.isFillWidth = false
        self.customMenuBar.titleFont = .boldSystemFont(ofSize: 15)
        self.customMenuBar.normalTabColor = normalTabColor
        self.customMenuBar.selectedTabColor = selectedTabColor
        self.selectedIndexHandler = { index in
            switch index {
            case 0:
                if simpleInput.isViewLoaded {
                    simpleInput.viewWillAppear(true)
                }
            case 1:
                if detailInput.isViewLoaded {
                    detailInput.viewWillAppear(true)
                }
            default:
                break
            }
        }
        
        super.viewDidLoad()
        
        //light 모드로 강제진행
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
    }
    
    func changeFoodPosition( index : Int ) {
        simpleInputViewController?.selectedIndex = index
        detailInputViewController?.selectedIndex = index
        
        if index < CameraViewController.predictResult.foodPositionList.count {
            simpleInputViewController?.selectedFoodPosition = CameraViewController.predictResult.foodPositionList[index]
            detailInputViewController?.selectedFoodPosition = CameraViewController.predictResult.foodPositionList[index]
        }
        
        if simpleInputViewController?.isViewLoaded == true {
            simpleInputViewController?.setUnitView()
            simpleInputViewController?.setServingUnitView()
            simpleInputViewController?.setAmountSelected()
        }
        
        if detailInputViewController?.isViewLoaded == true {
            detailInputViewController?.setUnitView()
            detailInputViewController?.setGramView()
            detailInputViewController?.setAmountView()
            detailInputViewController?.setCalorieView()
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
