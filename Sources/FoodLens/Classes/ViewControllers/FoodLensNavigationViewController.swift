//
//  FoodLensNavigationViewController.swift
//  FoodLens
//
//  Created by HwangChun Lee on 22/04/2019.
//

import UIKit

class FoodLensNavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //self.navigationController?.navigationBar.barTintColor = FoodLens.navigationBarTheme.backgroundColor
        navigationBar.barTintColor = FoodLens.navigationBarTheme.backgroundColor
        
        //light 모드로 강제진행
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
    }
    
    open override var shouldAutorotate: Bool {
        return true
    }
    
    open override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
