//
//  DetailNutritionFrameViewController.swift
//  DietCameraAI
//
//  Created by HwangChun Lee on 16/11/2018.
//  Copyright © 2018 leeint. All rights reserved.
//

import UIKit

@available(iOS 10.0, *)
class NutritionFrameViewController: CustomTabbarVC {
    let normalColor : UIColor = UIColor(red:0.13, green:0.13, blue:0.13, alpha:1.0)
    let selectedColor : UIColor = UIColor(red:0.80, green:0.17, blue:0.18, alpha:1.0)
    
    let bundle: Bundle = foodlensResourceBundle
    
    @IBOutlet weak var backButton: UIButton!
    
    override func viewDidLoad() {
        
        let total = UIStoryboard(name:"Main", bundle:bundle).instantiateViewController(withIdentifier:"TotalNutrition") as! TotalNutritionViewController
        let detail = UIStoryboard(name:"Main", bundle:bundle).instantiateViewController(withIdentifier:"DetailNutrition") as! DetailNutritionTableViewController
        
        let totalNutritionInfoTitle:String = NSLocalizedString("total_nutrition_info", tableName: nil, bundle: bundle, value: "", comment: "")
        let detailNutritionInfoTitle:String = NSLocalizedString("detail_nutrition_info", tableName: nil, bundle: bundle, value: "", comment: "")

        self.customMenuBar = .init(titles: [totalNutritionInfoTitle, detailNutritionInfoTitle], height: 40.0)
        self.customMenuBar.titleFont = .boldSystemFont(ofSize: 14)
        self.customMenuBar.normalTabColor = normalColor
        self.customMenuBar.selectedTabColor = selectedColor
        self.customMenuBar.height = 40.0
        
        self.viewControllers = [total, detail]
        
        backButton.tintColor = FoodLens.navigationBarTheme.foregroundColor
        
        super.viewDidLoad()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        backButton.setImage(FoodLens.navigationBarTheme.backImage, for: .normal)
        //light 모드로 강제진행
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = .white
            self.navigationController?.navigationBar.standardAppearance = appearance;
            self.navigationController?.navigationBar.scrollEdgeAppearance = self.navigationController?.navigationBar.standardAppearance
        }
    }
    
    @IBAction func didBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
