//
//  AmountHelpViewController.swift
//  Alamofire
//
//  Created by HwangChun Lee on 03/07/2019.
//

import UIKit

class AmountHelpViewController: UIViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    var pageController : UIPageViewController?
    var pageContent : [UIViewController] = []
    var currentIndex : Int = 0
    
    @IBOutlet weak var imageAreaView: UIView!
    
    let bundle: Bundle = foodlensResourceBundle
    let imgList_KR:[String] = ["help_image_1_kr", "help_image_2_kr"]
    let imgList_EN:[String] = ["help_image_1_en", "help_image_2_en"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //light 모드로 강제진행
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        createHelpPageViewController()
    }
    
    func createHelpPageViewController() {
        pageController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageController?.delegate = self
        pageController?.dataSource = self
        
        let startVC = viewControllerAtIndex(index: 0)
        let viewControllers = [startVC]
        pageController?.setViewControllers(viewControllers, direction: .forward, animated: true, completion: nil)
        
        let pageRect = imageAreaView.bounds
        pageController?.view.frame = pageRect
        pageController?.didMove(toParent: self)
        
        addChild(pageController!)
        imageAreaView.addSubview(self.pageController!.view)
    }
    
    @IBAction func didClosePressed(_ sender: Any) {
        FoodLens.isShowHelp = false
        self.dismiss(animated: true, completion: nil)
    }
    
    func getImageList() -> [String] {
        let locale = NSLocale.current.languageCode
        if locale == "ko" {
            return imgList_KR
        } else {
            return imgList_EN
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let vc = viewController as! AmountHelpImageViewController
        var index = vc.index as Int
        if index == 0 || index == NSNotFound {
            return nil
        }
        
        index -= 1
        
        return viewControllerAtIndex(index: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let imgList = getImageList()
        let vc = viewController as! AmountHelpImageViewController
        var index = vc.index as Int
        
        if index + 1  == imgList.count || index == NSNotFound {
            return nil
        }
        
        index += 1
        
        return viewControllerAtIndex(index: index)
    }
    
    func viewControllerAtIndex(index : Int) -> AmountHelpImageViewController {
        if let imageList = FoodLens.helpImages {
            guard let vc = UIStoryboard(name: "Main", bundle: bundle).instantiateViewController(withIdentifier: "AmountHelpImageViewController") as? AmountHelpImageViewController else {
                    return AmountHelpImageViewController()
            }
            
            if index < imageList.count {
                vc.image = imageList[index]
            }
            vc.index = index
            return vc
        }
        else {
            let imgList = getImageList()
            guard let vc = UIStoryboard(name: "Main", bundle: bundle).instantiateViewController(withIdentifier: "AmountHelpImageViewController") as? AmountHelpImageViewController,
                let image = UIImage(named: imgList[index], in: bundle, compatibleWith: nil) else {
                    return AmountHelpImageViewController()
            }
            
            vc.image = image
            vc.index = index
            return vc
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
