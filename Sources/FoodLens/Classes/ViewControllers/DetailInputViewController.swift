//
//  DetailInputViewController.swift
//  DietCameraAI
//
//  Created by HwangChun Lee on 02/01/2019.
//  Copyright © 2019 leeint. All rights reserved.
//

import UIKit

protocol ChangeAmountProtocol {
    func changeAmount(amount : String)
    func manualInputSelected()
}

@available(iOS 10.0, *)
class DetailInputViewController: UIViewController, ChangeAmountProtocol {
    weak var superViewController : AmountViewController? = nil
    var amountSelectPopup : AmountSelectPopup? = nil
    var amountValue : Float = 1.0
    var originY : CGFloat?
    @IBOutlet weak var amountView: UITextField!
    @IBOutlet weak var gramView: UITextField!
    @IBOutlet weak var calorieView: UITextField!
    
    @IBOutlet weak var unitView: UILabel!
    
    @IBOutlet weak var unitTitleView: UIView!
    @IBOutlet weak var unitSeperatorView: UIView!
    @IBOutlet weak var unitInputView: UIView!
    @IBOutlet weak var unitButtonView: UIView!
    
    @IBOutlet weak var amountTitleView: UIView!
    @IBOutlet weak var amountSeperatorView: UIView!
    @IBOutlet weak var amountInputView: UIView!
    @IBOutlet weak var amountRightView: UIView!
    
    @IBOutlet weak var calorieTitleView: UIView!
    @IBOutlet weak var calorieSeperatorView: UIView!
    @IBOutlet weak var calorieInputView: UIView!
    @IBOutlet weak var calorieRightView: UIView!
    
    @IBOutlet weak var bowlStackView: UIStackView!
    
    @IBOutlet weak var gView: UILabel!
    
    @IBOutlet weak var paddingStackView: UIStackView!
    var backupValue : String? = nil
    weak var activeField : UITextField? = nil
    
    var selectedFoodPosition : FoodPosition? = nil
    var selectedIndex : Int = 0
    
    let bundle: Bundle = foodlensResourceBundle
    
    var undoMgr : UndoManager = UndoManager()
    
    func addShadow(view : UIView) {
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 0.0
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        undoMgr.removeAllActions()
        addConfirmButtonToTextField(textField: amountView, action: #selector(didAmountConfirmPressed))
        addConfirmButtonToTextField(textField: gramView, action: #selector(didGramConfirmPressed))
        addConfirmButtonToTextField(textField: calorieView, action: #selector(didCalorieConfirmPressed))
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        addShadow(view: unitTitleView)
        addShadow(view: unitSeperatorView)
        addShadow(view: unitInputView)
        addShadow(view: unitButtonView)
        
        addShadow(view: amountTitleView)
        addShadow(view: amountSeperatorView)
        addShadow(view: amountInputView)
        addShadow(view: amountRightView)
        
        addShadow(view: calorieTitleView)
        addShadow(view: calorieSeperatorView)
        addShadow(view: calorieInputView)
        addShadow(view: calorieRightView)
        
        self.view.translatesAutoresizingMaskIntoConstraints = false
        
        //light 모드로 강제진행
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let selectedIndex: Int = superViewController?.selectedIndex {
            selectedFoodPosition = superViewController?.predictResult?.foodPositionList[safe: selectedIndex] ?? .init()
        }
        
        setUnitView()
        setAmountView()
        setGramView()
        setCalorieView()
    }
    
    func setUnitView() {
        if let food = selectedFoodPosition?.userSelectedFood?.nutrition {
            let unitList = Util.parseStandardUnit(standardUnit: food.unit)
            if unitList.count > 0 && unitList[0].1.count > 0 {
                if unitList[0].1.lowercased() == "g" || unitList[0].1.lowercased() == "ml" {
                    setBowlStackViewHidden(isHidden : true)
                }
                else {
                    if unitView != nil {
                        unitView.text = "\(unitList[0].1)"
                    }
                    
                    setBowlStackViewHidden(isHidden : false)
                    
                }
                
                if unitList[0].1.lowercased() == "ml" {
                    if gView != nil {
                        gView.text = "ml"
                    }
                }
                else {
                    if gView != nil {
                        gView.text = "g"
                    }
                }
            }
            else {
                if unitView != nil {
                    unitView.text = "인분"
                }
            }
        }
        else {
            if unitView != nil {
                unitView.text = NSLocalizedString("eatamount", tableName: nil, bundle: bundle, value: "", comment: "")
            }
            setBowlStackViewHidden(isHidden : false)
        }
    }
    
    func setBowlStackViewHidden(isHidden : Bool) {
        if bowlStackView != nil {
            bowlStackView.isHidden = isHidden
        }
        
        if paddingStackView != nil {
            paddingStackView.isHidden = !isHidden
        }
    }
    
    func setAmountView() {
        if let selected = self.selectedFoodPosition {
            switch selected.eatAmount {
            case 0.25:
                amountView.text = "1/4"
            case 0.5:
                amountView.text = "1/2"
            case 0.75:
                amountView.text = "3/4"
            default:
                amountView.text = String(format: "%.1f", selected.eatAmount)
            }
        }
    }
    
    func setAmountView(amount : Float) {
        switch amount {
        case 0.25:
            amountView.text = "1/4"
        case 0.5:
            amountView.text = "1/2"
        case 0.75:
            amountView.text = "3/4"
        default:
            amountView.text = String(format: "%.1f", amountValue)
        }
    }
    
    func setGramView() {
        guard let totalGram = selectedFoodPosition?.userSelectedFood?.nutrition?.totalgram, let eatAmount = selectedFoodPosition?.eatAmount else {
            gramView.text = "0.0"
            return
        }
        gramView.text = String(format: "%.1f", totalGram * eatAmount)
    }
    
    func setCalorieView() {
        guard let calories = selectedFoodPosition?.userSelectedFood?.nutrition?.calories, let eatAmount = selectedFoodPosition?.eatAmount else {
            calorieView.text = "0.0"
            return
        }
        let calorie = calories * eatAmount
        calorieView.text = String(format: "%.1f", calorie.isNaN ? 0 : calorie)
    }
    
    func changeAmount(amount: String) {
        var amountFloat : Float? = nil
        switch amount {
        case "1/4":
            amountFloat = 0.25
        case "1/2":
            amountFloat = 0.5
        case "3/4":
            amountFloat = 0.75
        default:
            amountFloat = Float(amount)
        }
        
        if amountFloat != nil {
            amountView.text = amount
            amountValue = amountFloat!
            updateGramAndCalorieByAmount()
        }
        else  {
            amountView.text = "1.00"
            amountValue = 1.0
            updateGramAndCalorieByAmount()
        }
    }
    
    func manualInputSelected() {
        amountView.becomeFirstResponder()
    }
    
    @objc func keyboardWillShow(_ sender:Notification){
        if originY ==  nil {
            originY = superViewController?.view.frame.origin.y
            superViewController?.view.frame.origin.y = -200
        }
    }
    
    @objc func keyboardWillHide(_ sender:Notification){
        if let originY {
            superViewController?.view.frame.origin.y = originY
        }
        originY = nil
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        rollbackChanges()
    }
    
    func rollbackChanges() {
        if let field = activeField {
            field.endEditing(true)
            if let undoString = backupValue {
                field.text = String(undoString)
                backupValue = nil
            }
            activeField = nil
        }
    }
    
    @IBAction func didBowlPressed(_ sender: Any) {
            
        let popup : AmountSelectPopup = UINib(nibName: "AmountSelectPopup", bundle: bundle).instantiate(withOwner: self, options: nil)[0] as! AmountSelectPopup
        popup.changeAmountProtocol = self
        amountSelectPopup = popup
        
        if let parentView = self.superViewController?.view.superview {
            popup.frame = parentView.frame
            parentView.addSubview(popup)
        } else {
            popup.frame = self.view.frame
            self.view.addSubview(popup)
        }
    }
    
    func addConfirmButtonToTextField(textField : UITextField, action: Selector?) {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let confirmButton = UIBarButtonItem(title: "OK  ", style: .done, target: self, action: action)
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.items = [flexibleSpace, confirmButton]
        textField.inputAccessoryView = toolbar
    }
    
    @objc func didAmountConfirmPressed() {
        amountView.endEditing(true)
    }
    
    @objc func didGramConfirmPressed() {
        gramView.endEditing(true)
    }
    
    @objc func didCalorieConfirmPressed() {
        calorieView.endEditing(true)
    }
    
    func didManualInputPressed() {
        backupValue = String(amountView.text ?? "")
        activeField = amountView
        amountView.becomeFirstResponder()
        if let popup = amountSelectPopup {
            popup.removeFromSuperview()
        }
        amountSelectPopup = nil
    }
    
    func didQuarterPressed() {
        amountView.text = "1/4"
        amountValue = 0.25
        if let popup = amountSelectPopup {
            popup.removeFromSuperview()
        }
        updateGramAndCalorieByAmount()
        amountSelectPopup = nil
    }
    
    func didHalfPressd() {
        amountView.text = "1/2"
        amountValue = 0.5
        if let popup = amountSelectPopup {
            popup.removeFromSuperview()
        }
        updateGramAndCalorieByAmount()
        amountSelectPopup = nil
    }
    
    func didThreeFourthPressed() {
        amountView.text = "3/4"
        amountValue = 0.75
        if let popup = amountSelectPopup {
            popup.removeFromSuperview()
        }
        updateGramAndCalorieByAmount()
        amountSelectPopup = nil
    }
    
    func didOnePressed() {
        amountView.text = "1"
        amountValue = 1.0
        if let popup = amountSelectPopup {
            popup.removeFromSuperview()
        }
        updateGramAndCalorieByAmount()
        amountSelectPopup = nil
    }
    
    func updateGramAndCalorieByAmount() {
        if let food = selectedFoodPosition?.userSelectedFood?.nutrition {
            let gram = food.totalgram * amountValue
            let calorie = food.calories * amountValue
            gramView.text = String(format: "%.1f", gram)
            calorieView.text = String(format: "%.1f", calorie)
        }
        else {
            gramView.text = "0.0"
            calorieView.text = "0.0"
        }
        
        selectedFoodPosition?.eatAmount = amountValue
        
        if let foodID: Int = selectedFoodPosition?.userSelectedFood?.foodId,
           let eatAmount: Float = selectedFoodPosition?.eatAmount {
            _ = superViewController?.calculateCalories(foodID: foodID, eatAmount: eatAmount)
        }
    }
    
    func syncGram(calorie : Float) {
        if let food = selectedFoodPosition?.userSelectedFood?.nutrition {
            let factor = food.calories == 0 ? 0 : calorie / food.calories
            let gram = food.totalgram * factor
            gramView.text = String(format: "%.1f", gram)
        }
        else {
            gramView.text = "0.0"
        }
    }
    
    func syncCalorie(gram : Float) {
        if let food = selectedFoodPosition?.userSelectedFood?.nutrition {
            let factor = food.totalgram == 0 ? 0 : gram / food.totalgram
            let calorie = food.calories * factor
            calorieView.text = String(format: "%.1f", calorie)
        }
        else {
            calorieView.text = "0.0"
        }
        
        if let foodID: Int = selectedFoodPosition?.userSelectedFood?.foodId,
           let eatAmount: Float = selectedFoodPosition?.eatAmount {
            _ = superViewController?.calculateCalories(foodID: foodID, eatAmount: eatAmount)
        }
    }
    
    func syncAmount(gram : Float) {
        if let food = selectedFoodPosition?.userSelectedFood?.nutrition {
            let factor = food.totalgram == 0 ? 0 : gram / food.totalgram
            amountView.text = String(format: "%.1f", factor)
            selectedFoodPosition?.eatAmount = factor
        }
        else {
            amountView.text = "0.0"
        }
        
        if let foodID: Int = selectedFoodPosition?.userSelectedFood?.foodId,
           let eatAmount: Float = selectedFoodPosition?.eatAmount {
            _ = superViewController?.calculateCalories(foodID: foodID, eatAmount: eatAmount)
        }
    }
    
    func syncAmount(calorie : Float) {
        if let food = selectedFoodPosition?.userSelectedFood?.nutrition {
            let factor = food.calories == 0 ? 0 : calorie / food.calories
            amountView.text = String(format: "%.1f", factor)
            selectedFoodPosition?.eatAmount = factor
        }
        else {
            amountView.text = "0.0"
        }
        
        if let foodID: Int = selectedFoodPosition?.userSelectedFood?.foodId,
           let eatAmount: Float = selectedFoodPosition?.eatAmount {
            _ = superViewController?.calculateCalories(foodID: foodID, eatAmount: eatAmount)
        }
    }
    
    @IBAction func didGramPressed(_ sender: Any) {
        gramView.becomeFirstResponder()
    }
    
    @IBAction func didCaloriePressed(_ sender: Any) {
        calorieView.becomeFirstResponder()
    }
    
    @IBAction func didGramEditBegin(_ sender: Any) {
        let nowValue = gramView.text ?? "1"
        undoMgr.registerUndo(withTarget: gramView) { $0.text = nowValue }
    }
    
    @IBAction func didCalorieEditBegin(_ sender: Any) {
        let nowValue = calorieView.text ?? "1"
        undoMgr.registerUndo(withTarget: calorieView) { $0.text = nowValue }
        activeField = calorieView
        backupValue = String(calorieView.text ?? "")
    }
    
    @IBAction func didAmountEditBegin(_ sender: Any) {
        let nowValue = amountView.text ?? "1"
        undoMgr.registerUndo(withTarget: amountView) { $0.text = nowValue }
        rollbackChanges()
        activeField = amountView
    }
    
    
    @IBAction func didAmountEditEnd(_ sender: Any) {
        activeField = nil
        if let text = amountView.text, NSString(string: text).floatValue > 0 {
            amountValue = NSString(string: text).floatValue
            updateGramAndCalorieByAmount()
        }
        else {
            undoMgr.undo()
        }
    }
    
    @IBAction func didGramEditEnd(_ sender: Any) {
        activeField = nil
        if let gram = Float(gramView.text!) {
            syncCalorie(gram: gram)
            syncAmount(gram: gram)
        }
        else {
            undoMgr.undo()
        }
    }
    
    @IBAction func didCalorieEditEnd(_ sender: Any) {
        activeField = nil
        if let calorie  = Float(calorieView.text!) {
            syncGram(calorie: calorie)
            syncAmount(calorie: calorie)
        }
        else {
            undoMgr.undo()
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
