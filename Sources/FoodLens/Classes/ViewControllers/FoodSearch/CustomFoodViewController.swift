//
//  CustomFoodViewController.swift
//  FoodLens
//
//  Created by Eunjin on 01/04/2020.
//

import UIKit

class CustomFoodViewController: UIViewController {
    @IBOutlet weak var addMyFoodView: UIStackView!
    @IBOutlet weak var customFoodTableView: UITableView!
    @IBOutlet weak var addMyFoodLabel: UILabel!
    let cellIdentifier: String = "customFoodCell"
    let customFoodManager: CustomFoodManager = CustomFoodManager.getInstance()
    var customFoodList: [FoodInfo] = []
    var selectedIndex: Int = 0
    var foodAddProtocol : FoodAddProtocol? = nil
    var changeProtocol : FoodChangeProtocol? = nil
    var manualInputProtocol : MaunalInputProtocol? = nil
    let bundle: Bundle = foodlensResourceBundle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = false
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.addMyFoodClicked))
        self.addMyFoodView.addGestureRecognizer(gesture)
        self.addMyFoodLabel.text = NSLocalizedString("foodSearch_myfood_addmyfood",bundle:bundle, comment: "")
        
        makeCustomFoodList()

        self.view.translatesAutoresizingMaskIntoConstraints = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        makeCustomFoodList()
        self.view.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func makeCustomFoodList() {
        self.customFoodList = []
        customFoodManager.makeCustomFoodList() { list in
            self.customFoodList = list
            self.customFoodList.removeAll(where: { $0.id == 0 })
            self.customFoodTableView.reloadData()
        }
    }
    
    @objc func addMyFoodClicked(sender : UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        guard let detailCustomFoodViewController = storyboard.instantiateViewController(withIdentifier: "ToDetailCustomFood") as? DetailCustomFoodViewController else { return }
        self.navigationController?.pushViewController(detailCustomFoodViewController, animated: true)
    }
    
    
    /*func testFood() {
        //var foods:[String:FoodInfo] = [:]
        let foodInfo1 : FoodInfo = FoodInfo()
        foodInfo1.id = 0
        foodInfo1.foodname = "test1"
        foodInfo1.calories = 100
        let foodInfo2 : FoodInfo = FoodInfo()
        foodInfo2.id = 1
        foodInfo2.foodname = "test2"
        foodInfo2.calories = 200
        
        //foods.updateValue(foodInfo1, forKey: String(foodInfo1.id))
        //foods.updateValue(foodInfo2, forKey: String(foodInfo2.id))
        
        customFoodManager.addCustomFood(foodInfo: foodInfo1.toDictionary() as! [String : Any]) { result in
            
        }

        customFoodManager.addCustomFood(foodInfo: foodInfo2.toDictionary() as! [String : Any]) { result in
            
        }

    }*/
}

extension CustomFoodViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customFoodList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = customFoodTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CustomFoodTableViewCell else { return UITableViewCell() }
        cell.controlButton.tag = indexPath.row
        cell.foodTitleLabel.text = customFoodList[indexPath.row].foodname
        cell.controlButton.addTarget(self, action: #selector(showAlert(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        guard indexPath.row < customFoodList.count else { return }
        makeFoodInfo(by: customFoodList[indexPath.row].id)
    }
    
    func makeFoodInfo(by foodId: Int) {
        var food : Food = Food()
        if changeProtocol != nil ||
            foodAddProtocol != nil ||
            manualInputProtocol != nil {
            
            customFoodManager.getCustomFoodInfo(foodId: foodId) { foodInfo in
                guard let foodInfo = foodInfo else { return }
                food.foodId = foodInfo.id
                food.foodName = foodInfo.foodname
                let nutrition : Nutrition = Nutrition(foodInfo: foodInfo)
                //self.copyAttr(soruce : foodInfo, dest : nutrition)
                food.nutrition = nutrition
                //food.nutrition =  foodInfo
                //food.source =  .manual
                
                if let addprotocol = self.foodAddProtocol {
                    addprotocol.addFood(foodInfo: foodInfo, searchedFoodName: foodInfo.foodname, keyType: .manual)
                    self.navigationController?.popViewController(animated: true)
                }
                else if let changeProtocol = self.changeProtocol {
                    changeProtocol.changeFood(food: food, keyType: .manual)
                    self.navigationController?.popViewController(animated: true)
                }
                else if let manualProtocol = self.manualInputProtocol {
                    manualProtocol.manualInputComplete(foodInfo : foodInfo, searchedFoodName: foodInfo.foodname, keyType: .manual)
                }
            }
        }
    }
    
    /*func copyAttr(soruce:Any, dest:Any) {
        
        let originalReflect = Mirror(reflecting: soruce)
        let selfReflect = Mirror(reflecting: dest)
         for (_, originalAttr) in originalReflect.children.enumerated() {
            for (_, attr) in selfReflect.children.enumerated() {
                if originalAttr.label == attr.label {
                    //now I know the value of property and its value as well in original
                    //but how will I set it to self?
                    //If A was NSObject I could have said
                    attr.value = originalAttr.value


                }
            }
        }
    }*/
    
    @objc func showAlert(sender: UIButton) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancelTitle = NSLocalizedString("cancel", bundle:bundle,comment: "")
        let modifyTitle = NSLocalizedString("modify", bundle:bundle,comment: "")
        let deleteTitle = NSLocalizedString("delete", bundle:bundle,comment: "")
        
        let cancel = UIAlertAction(title: cancelTitle, style: .cancel, handler: nil)
        let modify = UIAlertAction(title: modifyTitle, style: .default, handler: { action in
            self.modifyCustomFood()
        })
        let delete = UIAlertAction(title: deleteTitle, style: .destructive, handler: { action in
            self.deleteCustomFood()
        })
        self.selectedIndex = sender.tag
        
        alert.addAction(cancel)
        alert.addAction(modify)
        alert.addAction(delete)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func modifyCustomFood() {
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        guard let detailCustomFoodViewController = storyboard.instantiateViewController(withIdentifier: "ToDetailCustomFood") as? DetailCustomFoodViewController else { return }
        detailCustomFoodViewController.selectedCustomFood = customFoodList[selectedIndex]
        detailCustomFoodViewController.mode = .edit
        self.navigationController?.pushViewController(detailCustomFoodViewController, animated: true)
    }
    
    func deleteCustomFood() {
        let id = customFoodList[selectedIndex].id
        customFoodManager.deleteCustomFood(id: id) { isCompleteDelete in
            if !isCompleteDelete { return }
            self.customFoodList.remove(at: self.selectedIndex)
            self.customFoodTableView.reloadData()
        }
    }
    
}

extension NSDictionary {
    var swiftDictionary: Dictionary<String, Any> {
        var swiftDictionary = Dictionary<String, Any>()

        for key : Any in self.allKeys {
            let stringKey = key as! String
            if let keyValue = self.value(forKey: stringKey){
                swiftDictionary[stringKey] = keyValue
            }
        }
        return swiftDictionary
    }
}
