//
//  SearchViewController.swift
//  FoodLens
//
//  Created by Eunjin on 01/04/2020.
//

import UIKit

class SearchViewController: UIViewController {
    @IBOutlet weak var searchTextField: ThrottleTextField!
    @IBOutlet weak var searchTextFieldView: UIView!
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var searchImageView: UIImageView!
    var identifierOfTitleCell = "RecentSearchesTitleCell"
    var identifierOfRecentSearchesCell = "RecentSearchesContentsCell"
    var identifierOfRealTimeSearchesCell = "RealTimeSearchesCell"
    var candidateList: [Food] = []
    var recentSearchList: [FoodSearchList] = []
    var realTimeMenuList: [FoodSearchList] = []
    var tableType: TableType? = nil
    let searchManager: SearchManager = SearchManager()
    var previousSearchKeyword: String = ""
    var foodAddProtocol : FoodAddProtocol? = nil
    var changeProtocol : FoodChangeProtocol? = nil
    var manualInputProtocol : MaunalInputProtocol? = nil
    let bundle: Bundle = foodlensResourceBundle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
            self?.searchTextField.becomeFirstResponder()
        }
        
        self.tableType = .recent
        guard let keyword = SystemInfo.getRecentKeyword() else { return }
        self.recentSearchList = keyword
        self.recentSearchList.removeAll(where: { $0.id == 0 })
        searchTextField.placeholder = NSLocalizedString("foodSearch_search_placeholder", bundle: bundle,comment: "")
        searchTextFieldView.makeShadow()
        searchTableView.isUserInteractionEnabled = true
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.searchTableView.translatesAutoresizingMaskIntoConstraints = false
        self.view.translatesAutoresizingMaskIntoConstraints = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        resetTableView()
        self.view.translatesAutoresizingMaskIntoConstraints = false
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
          self.view.endEditing(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    @IBAction func didEditingChanged(_ sender: Any) {
        self.makeTableViewType()
        searchTextField.throttle(delay: 0.35) { [weak self] text in
            guard let `self` = self, let text = text else { return }
            
            if text == self.previousSearchKeyword { return } // 직전 검색어와 같다면 return
            self.previousSearchKeyword = text
            self.searchManager.makeFoodSearchList(by: text) { result in // 데이터 load가 complete 되면 실행
                self.realTimeMenuList = result
                self.searchTableView.reloadData()
            }
        }
    }
    
    func resetTableView() {
        self.realTimeMenuList = []
        self.searchTableView.reloadData()
    }
    
    func makeTableViewType() {
        guard let text = searchTextField.text else { return }
        if text.isEmpty {
            tableType = TableType.recent
            searchImageView.isHidden = false
        }else {
            tableType = TableType.realtime
            searchImageView.isHidden = true
        }
        searchTableView.reloadData()
    }
    
    func makeSuitableString(search: String?) -> String? {
        guard let searchString = search, !searchString.isEmpty else { return nil }
        guard let lastCharacter = searchString.last else { return nil }
        guard let unicodeValue = UnicodeScalar(String(lastCharacter))?.value else { return searchString }
        
        if (unicodeValue < 0xAC00 || unicodeValue > 0xD7A3), searchString.isKoreanAlphabet() { // 한글 한 글자 입력 시
            return nil
        }
        return searchString
    }

}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableType == TableType.recent {
            return recentSearchList.count+1
        }else if tableType == TableType.realtime {
            return realTimeMenuList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableType == TableType.recent {
            if indexPath.row > 0,
               let recentSearchItem = recentSearchList[safe: indexPath.row - 1] {
                guard let recentSearchesCell = searchTableView.dequeueReusableCell(withIdentifier: identifierOfRecentSearchesCell) as? RecentSearchesTableViewCell else { return UITableViewCell() }
                recentSearchesCell.foodNameLabel.text = recentSearchItem.foodname
                recentSearchesCell.foodDeleteButton.tag = indexPath.row
                recentSearchesCell.foodDeleteButton.addTarget(self, action: #selector(deleteRecentFoodName(sender:)), for: .touchUpInside)
                return recentSearchesCell
            } else {
                guard let titleCell = searchTableView.dequeueReusableCell(withIdentifier: identifierOfTitleCell) as? RecentTitleTableViewCell else { return UITableViewCell() }
                // Localized
                titleCell.titleLabel.text = NSLocalizedString("foodSearch_search_tableViewTitle", bundle: bundle,comment: "")
                return titleCell
            }
        }else if tableType == TableType.realtime {
            guard let realTimeMenuCell = searchTableView.dequeueReusableCell(withIdentifier: identifierOfRealTimeSearchesCell) as? RealTimeSearchesTableViewCell else { return UITableViewCell() }
            
            if let realTimeMenu = realTimeMenuList[safe: indexPath.row] {
                if realTimeMenu.manufacturer == "" {
                    realTimeMenuCell.foodNameLabel.text = realTimeMenu.foodname
                }else {
                    realTimeMenuCell.foodNameLabel.text = realTimeMenu.foodname + " (" + realTimeMenu.manufacturer + ")"
                }
            }
            
            realTimeMenuCell.makeEmphaSizeString(of: self.previousSearchKeyword)
            return realTimeMenuCell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row >= 0 {
            if tableType == TableType.recent {
                guard (indexPath.row) != 0 else { return }
                //guard (indexPath.row-1) < recentSearchList.count else { return }
                makeFoodInfo(by: recentSearchList[indexPath.row-1].id, searchedFoodName: recentSearchList[indexPath.row-1].foodname)
            }else if tableType == TableType.realtime {
                guard indexPath.row < realTimeMenuList.count else { return }
                makeFoodInfo(by: realTimeMenuList[indexPath.row].id, searchedFoodName: realTimeMenuList[indexPath.row].foodname)
            }
        }
    }
    
    @objc func deleteRecentFoodName(sender: UIButton) {
        self.recentSearchList.remove(at: sender.tag-1)
        searchTableView.reloadData()
    }
    
    func makeFoodInfo(by foodId: Int, searchedFoodName :String) {
        //let food : Food = Food()
        if let addProtocol = foodAddProtocol {
            HttpHelper.getFoodInfo(foodId: foodId) { (foodInfo, reason) in
                guard let foodInfo = foodInfo else {
                    return
                }
                
                self.saveRecentSearchList(foodInfo: foodInfo, searchedFoodName : searchedFoodName)
                addProtocol.addFood(foodInfo: foodInfo, searchedFoodName : searchedFoodName, keyType: .db)
            }
            self.navigationController?.popViewController(animated: true)
        }
        else if let changeProtocol = changeProtocol {
            HttpHelper.getFoodInfo(foodId: foodId) { (foodInfo, reason) in
                guard let foodInfo = foodInfo else { return }
                var food : Food = Food()
                food.foodId = foodInfo.id
                food.foodName = searchedFoodName
                
                self.saveRecentSearchList(foodInfo: foodInfo, searchedFoodName : searchedFoodName)
                changeProtocol.changeFood(food: food, keyType: .db)
            }
            self.navigationController?.popViewController(animated: true)
        }else if let manualProtocol = manualInputProtocol {
            HttpHelper.getFoodInfo(foodId: foodId) { (foodInfo, reason) in
                guard let foodInfo = foodInfo else {
                    return
                }
                self.saveRecentSearchList(foodInfo: foodInfo, searchedFoodName : searchedFoodName)
                manualProtocol.manualInputComplete(foodInfo: foodInfo, searchedFoodName: searchedFoodName, keyType: .db)
            }
        }
//        self.navigationController?.popViewController(animated: true)
    }
    
    func saveRecentSearchList(foodInfo: FoodInfo, searchedFoodName : String) {
        var keyword = FoodSearchList()
        keyword.id = foodInfo.id
        keyword.foodname = searchedFoodName
        
        self.recentSearchList.removeAll(where: { $0.id == 0 })
        if self.recentSearchList.count > 9 {
            self.recentSearchList.removeLast()
        }
        if self.isOverlapFood(keyword: keyword) {
            if let index = self.recentSearchList.firstIndex(of: keyword) {
                self.recentSearchList.remove(at: index)
            }
        }
        self.recentSearchList.insert(keyword, at: 0)
        
        SystemInfo.setRecentKeyword(keywordList: self.recentSearchList)
        SystemInfo.setFoodInfo(foodInfo: foodInfo)
    }
    
    func isOverlapFood(keyword: FoodSearchList) -> Bool {
        return self.recentSearchList.contains(keyword)
    }
}

extension SearchViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        searchTableView.estimatedRowHeight = 0
        let indexPath = NSIndexPath(row: NSNotFound, section: 0)
        searchTableView.scrollToRow(at: indexPath as IndexPath, at: .top, animated: false)
        searchTableView.reloadData()
        return true
    }
}

extension String {
    func isKoreanAlphabet() -> Bool {
        do{
            let regex = try NSRegularExpression(pattern: "^[가-힣ㄱ-ㅎㅏ-ㅣ\\s]$", options: .caseInsensitive)
            if let _ = regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSMakeRange(0, self.count)){
                return true
            }
        }catch{
            print(error.localizedDescription)
            return false
        }
        return false
    }
    
    var htmlToAttributed: NSAttributedString? {
        do {
            let modifiedFont = NSString(format:"<span style=\"font-family: Helvetica Neue; font-size: \(13)\">%@</span>" as NSString, self)
            guard let data = modifiedFont.data(using: String.Encoding.utf8.rawValue) else {
                return nil
            }
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
}

extension UIColor {
    static func hexStringToUIColor(hex: String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

extension UIView {
    func makeShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor(red:0.80, green:0.80, blue:0.80, alpha:1.0).cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowRadius = 1.0
    }
}

enum TableType {
    case recent, realtime
}
