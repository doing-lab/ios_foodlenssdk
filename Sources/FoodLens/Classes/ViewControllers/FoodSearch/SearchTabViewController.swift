//
//  SearchTabViewController.swift
//  FoodLens
//
//  Created by Eunjin on 01/04/2020.
//

import UIKit

class SearchTabViewController: CustomTabbarVC {
    var candidateList: [Food] = []
    var changeProtocol : FoodChangeProtocol?
    var addProtocol : FoodAddProtocol?
    var manualInputProtocal : MaunalInputProtocol?
    //setting color
    let normalTabColor : UIColor = UIColor(red:0.68, green:0.68, blue:0.68, alpha:1.0)//#444
    let selectedTabTextColor : UIColor = UIColor(red:0.170, green:0.170, blue:0.170, alpha:1.0)//#aaa
    let selectedTabColor : UIColor = UIColor(red: 0.66, green: 0.16, blue: 0.17, alpha: 1.0)
    let bundle: Bundle = foodlensResourceBundle
    
    let recommend = 0
    let search = 1
    let myFood = 2
    
    var searchType: SearchType?

    override func viewDidLoad() {
        
        //탭바 마진, 색 ... 셋팅
        //음식 추천
        let recommendViewController = UIStoryboard(name:"Main", bundle:bundle).instantiateViewController(withIdentifier:"RecommendViewController") as! RecommendViewController
        recommendViewController.changeProtocol = self.changeProtocol
        recommendViewController.candidateList = self.candidateList
        
        
        //음식 검색
        let searchViewController = UIStoryboard(name:"Main", bundle:bundle).instantiateViewController(withIdentifier:"SearchViewController") as! SearchViewController
        searchViewController.changeProtocol = self.changeProtocol
        searchViewController.foodAddProtocol = self.addProtocol
        searchViewController.manualInputProtocol = self.manualInputProtocal
        
        //내 음식
        let customFoodViewController = UIStoryboard(name:"Main", bundle:bundle).instantiateViewController(withIdentifier:"CustomFoodViewController") as! CustomFoodViewController
        customFoodViewController.changeProtocol = self.changeProtocol
        customFoodViewController.foodAddProtocol = self.addProtocol
        customFoodViewController.manualInputProtocol = self.manualInputProtocal
        
        //식사습관 / 기록습관 / 영양습관
        if searchType == SearchType.addFood {
            let foodSearchSearchTitle:String = NSLocalizedString("foodSearch_search_title", bundle:bundle, comment: "")
            let foodSearchMyfoodTitle:String = NSLocalizedString("foodSearch_myfood_title", bundle:bundle,comment: "")
            
            self.customMenuBar = .init(titles: [foodSearchSearchTitle, foodSearchMyfoodTitle], height: 40.0)
            self.customMenuBar.titleFont = .boldSystemFont(ofSize: 15)
            self.customMenuBar.normalTabColor = self.normalTabColor
            self.customMenuBar.selectedTabColor = self.selectedTabTextColor
            self.customMenuBar.height = 40.0
        
            self.viewControllers = [searchViewController, customFoodViewController]
            self.pageCollectionView.isScrollEnabled = false
            self.selectedIndexHandler = { index in
                if index == 0 && searchViewController.isViewLoaded {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak searchViewController] in
                        searchViewController?.searchTextField.becomeFirstResponder()
                    }
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                        self?.view.endEditing(true)
                    }
                }
            }
        }else if searchType == SearchType.changeFood {
            let foodSearchRecommendTitle:String = NSLocalizedString("foodSearch_recommend_title", bundle: bundle, comment: "")
            let foodSearchSearchTitle:String = NSLocalizedString("foodSearch_search_title", bundle:bundle, comment: "")
            let foodSearchMyfoodTitle:String = NSLocalizedString("foodSearch_myfood_title", bundle:bundle,comment: "")
            
            self.customMenuBar = .init(titles: [foodSearchRecommendTitle, foodSearchSearchTitle, foodSearchMyfoodTitle], height: 40.0)
            self.customMenuBar.titleFont = .boldSystemFont(ofSize: 15)
            self.customMenuBar.normalTabColor = self.normalTabColor
            self.customMenuBar.selectedTabColor = self.selectedTabTextColor
            self.customMenuBar.height = 40.0
            self.viewControllers = [recommendViewController, searchViewController, customFoodViewController]
            self.pageCollectionView.isScrollEnabled = false
            self.selectedIndexHandler = { index in
                if index == 1 && searchViewController.isViewLoaded {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak searchViewController] in
                        searchViewController?.searchTextField.becomeFirstResponder()
                    }
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                        self?.view.endEditing(true)
                    }
                }
            }
        }
        
        //super.viewDidLoad()전에 탭 바 셋팅완료해야됨
        super.viewDidLoad()
        
        //super.viewDidLoad()후에 바 스크롤 셋팅 해야됨
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.backgroundColor = .white
    }
}

enum SearchType {
    case addFood, changeFood
}
