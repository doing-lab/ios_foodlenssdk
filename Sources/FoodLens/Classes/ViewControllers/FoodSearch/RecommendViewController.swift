//
//  RecommendViewController.swift
//  FoodLens
//
//  Created by Eunjin on 01/04/2020.
//

import UIKit

class RecommendViewController: UIViewController {
    @IBOutlet weak var recommendTableView: UITableView!
    var candidateList: [Food] = []
    var identifier = "recommendTableViewCell"
    var changeProtocol : FoodChangeProtocol?
    let bundle: Bundle = foodlensResourceBundle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.recommendTableView.isUserInteractionEnabled = true
        self.view.translatesAutoresizingMaskIntoConstraints = false
    }

}

extension RecommendViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.candidateList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // remove FoodSelectTableCell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? RecommendTableViewCell else { return UITableViewCell() }
            cell.foodNameLabel.text = candidateList[indexPath.row].foodName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let changeProc = changeProtocol, indexPath.row < candidateList.count {
            changeProc.changeFood(food: candidateList[indexPath.row], keyType: .predict)
        }
        self.parent?.navigationController?.popViewController(animated: true)
    }
}
