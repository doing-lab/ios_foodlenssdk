//
//  FoodSearchViewController.swift
//  FoodLens
//
//  Created by Eunjin on 01/04/2020.
//

import UIKit

class FoodSearchViewController: UIViewController {
    var candidateList: [Food] = []
    var changeProtocol : FoodChangeProtocol?
    var addProtocol : FoodAddProtocol?
    var manualInputProtocal : MaunalInputProtocol?
    var searchType: SearchType?
    var isDirect: Bool = false
    
    @IBOutlet weak var backButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.isDirect {
            backButton.setImage(FoodLens.navigationBarTheme.closeImage, for: .normal)
        } else {
            backButton.setImage(FoodLens.navigationBarTheme.backImage, for: .normal)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let target = segue.destination as? SearchTabViewController else { return }
        target.changeProtocol = self.changeProtocol
        target.addProtocol = self.addProtocol
        target.manualInputProtocal = self.manualInputProtocal
        target.candidateList = self.candidateList
        target.searchType = self.searchType
        self.navigationController?.navigationBar.tintColor = UIColor.hexStringToUIColor(hex: "#444444")
    }
    
    @IBAction func didBackPressed(_ sender: Any) {
        if self.isDirect {
            dismiss(animated: true) { }
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
}
