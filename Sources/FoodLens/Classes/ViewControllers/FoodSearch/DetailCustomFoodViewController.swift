//
//  DetailCustomFoodViewController.swift
//  FoodLens
//
//  Created by Eunjin on 01/04/2020.
//

import UIKit
import Alamofire

class DetailCustomFoodViewController: UIViewController {
    @IBOutlet weak var customFoodTableView: UITableView!
    @IBOutlet weak var completeButton: DesignableButton!
    var cellIdentifierInputMenu: String = "InputMenuCell"
    var cellIdentifierTypeLong: String = "longInputCustomFoodCell"
    var cellIdentifierTypeShort: String = "shortInputCustomFoodCell"
    var buttonCellIdentifier: String = "buttonCell"
    var nameOfJSONDataAsset: String = "JSONCustomFormatter"
    var countOfRow: Int = 7
    var isAddNutrientButtonClicked: Bool = false
    var nutrientFormatList: [String:CustomFood] = [:]
    var nutrientDataList: [String:Any] = [:] // 서버로부터 받아온 데이터
    var selectedCustomFood: FoodInfo? = nil
    var mode: DetailViewMode = .add
    var changeProtocol: FoodChangeProtocol? = nil
    var pointStarMark: String = "<font color = \"#cf3235\"> *</font>"
    var originY: CGFloat?
    var editingIndex: Int? = nil
    var heightOfPreviouskeyboard: CGFloat = 0
    var customFoodRequest: Request?
    let bundle: Bundle = foodlensResourceBundle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let title = NSLocalizedString("foodSearch_customFood_addFoodTitle", bundle:bundle, comment: "")
        self.navigationItem.title = title
        
        makeJsonToFormatData()
        makeNutrientDataList()
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(endEditing)))
        let completeTitle = NSLocalizedString("foodSearch_customFood_inputComplete", bundle:bundle,comment: "")
        completeButton.setTitle(completeTitle, for: .normal)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
      registerForKeyboardNotifications()
    }

    override func viewWillDisappear(_ animated: Bool) {
      unregisterForKeyboardNotifications()
    }
    
    func makeNutrientDataList() {
        guard let data = selectedCustomFood?.toJsonData() else { return }
        do {
            guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any] else { return }
            self.nutrientDataList = json
        }catch {
            print(error.localizedDescription)
        }
    }
    
    func makeJsonToFormatData() {
        let jsonDecoder: JSONDecoder = JSONDecoder()
        guard let dataAsset: NSDataAsset = NSDataAsset(name: nameOfJSONDataAsset, bundle: bundle) else { return }
        do {
            let list: [CustomFood] = try jsonDecoder.decode([CustomFood].self, from: dataAsset.data)
            self.makeNutrientFormatList(list: list)
        }catch {
            print(error.localizedDescription)
        }
    }
    
    func makeNutrientFormatList(list: [CustomFood]) {
        for item in list {
            self.nutrientFormatList.updateValue(item, forKey: item.key)
        }
    }
    
    @objc func endEditing() {
        self.view.endEditing(true)
    }
    
    @IBAction func AddNutrientButtonClicked(_ sender: Any) {
        self.countOfRow = NutrientsTag.order.count
        self.isAddNutrientButtonClicked = true
        makeNutrientDataList()
        customFoodTableView.reloadData()
    }
    
    @IBAction func CompleteButtonClicked(_ sender: Any) {
        nutrientDataList.updateValue(true, forKey: "customFoodInfo")
        //nutrientDataList
        if (nutrientDataList["foodname"] as? String) == nil || (nutrientDataList["foodname"] as? String) == "" {
            let message = NSLocalizedString("foodSearch_customFood_noFoodNameMessage", bundle:bundle, comment: "")
            ToastView.shared.short(self.view, txt_msg: message)
            return
        }
        if ( (nutrientDataList["totalgram"] as? Int) == nil && nutrientDataList["totalgram"] as? String == nil)
            || ((nutrientDataList["totalgram"] as? Int) == 0 && (nutrientDataList["totalgram"] as? String) == "") {
            let message = NSLocalizedString("foodSearch_customFood_noUnitMessage", bundle:bundle, comment: "")
            ToastView.shared.short(self.view, txt_msg: message)
            return
        }
        if mode == .add {
            CustomFoodManager.getInstance().addCustomFood(foodInfo: nutrientDataList) { result in
                self.navigationController?.popViewController(animated: true)
            }
        }else if mode == .edit {
            CustomFoodManager.getInstance().updateCustomFood(foodInfo: nutrientDataList){ result in
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
          self.view.endEditing(true)
    }
    
}

extension DetailCustomFoodViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !isAddNutrientButtonClicked {
            return countOfRow + 1
        }else {
            return countOfRow
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cellOfInputMenu = customFoodTableView.dequeueReusableCell(withIdentifier: cellIdentifierInputMenu) as? InputMenuTableViewCell else { return UITableViewCell() }
        guard let cellOfLongInput = customFoodTableView.dequeueReusableCell(withIdentifier: cellIdentifierTypeLong) as? LongInputCustomFoodTableViewCell else { return UITableViewCell() }
        guard let cellOfShortInput = customFoodTableView.dequeueReusableCell(withIdentifier: cellIdentifierTypeShort) as? InputCustomFoodTableViewCell else { return UITableViewCell() }
        guard let buttonCell = customFoodTableView.dequeueReusableCell(withIdentifier: buttonCellIdentifier) as? AddNutrientTableViewCell else { return UITableViewCell() }
        let buttonTitle = NSLocalizedString("foodSearch_customFood_addNutrient", bundle:bundle, comment: "")
        buttonCell.addNutrientButton.setTitle(buttonTitle, for: .normal)
        
        if !isAddNutrientButtonClicked, indexPath.row == countOfRow { return buttonCell } // add nutrient ButtonCell
        
        if indexPath.row == 0 { // food name textview
            cellOfInputMenu.inputMenuView.makeShadow()
            cellOfInputMenu.inputMenuTextField.delegate = self
            cellOfInputMenu.inputMenuTextField.tag = 0 // 음식명 텍스트필드
            cellOfInputMenu.inputMenuTextField.placeholder = NSLocalizedString("foodSearch_myfood_textfield", bundle:bundle, comment: "")
            if let foodName = self.selectedCustomFood?.foodname {
                cellOfInputMenu.inputMenuTextField.text = foodName
            }
            return cellOfInputMenu
        }
        
        guard let tag: NutrientsTag = NutrientsTag.init(rawValue: indexPath.row) else { return cellOfShortInput } // cell 1 ~ 17
        guard let customFoodFormatData: CustomFood = nutrientFormatList[tag.description] else { return cellOfShortInput }
        
        
        if isCellOfShortInput(row: indexPath.row) {
            cellOfShortInput.titleLabel.text = self.makeCustomFoodTitle(by: customFoodFormatData)
            if indexPath.row == 3 { // 분량 옆에 별 표시
                let htmlText = (cellOfShortInput.titleLabel.text ?? "") + pointStarMark
                cellOfShortInput.titleLabel.attributedText = htmlText.htmlToAttributed
            }
            cellOfShortInput.unitLabel.text = customFoodFormatData.unit
            cellOfShortInput.amountTextField.tag = indexPath.row
            cellOfShortInput.amountTextField.delegate = self
            cellOfShortInput.amountTextField.placeholder = "0"
            cellOfShortInput.amountTextField.keyboardType = .decimalPad
            cellOfShortInput.amountTextField.returnKeyType = .done
            
            // 실시간으로 입력받아 배열에 저장한 데이터를 출력함
            guard let key = NutrientsTag.init(rawValue: indexPath.row)?.description else { return cellOfShortInput }
            if let userInputData = nutrientDataList[key] {
                cellOfShortInput.amountTextField.text = userInputData as? String
            }
            // 수정 시 기존에 입력했던 값들을 출력함
            guard let value = nutrientDataList[key] else { return cellOfShortInput }
            cellOfShortInput.amountTextField.text = String(describing: value)
            return cellOfShortInput
            
        }else {
            cellOfLongInput.titleLabel.text = self.makeCustomFoodTitle(by: customFoodFormatData)
            cellOfLongInput.amountTextField.tag = indexPath.row
            cellOfLongInput.amountTextField.delegate = self
            cellOfLongInput.amountTextField.keyboardType = .default
            cellOfLongInput.amountTextField.returnKeyType = .done
     
            let placeholderText = NSLocalizedString("foodSearch_customFood_placeholder_\(tag.description)", bundle:bundle, comment: "")
            cellOfLongInput.amountTextField.placeholder = placeholderText
            
            // 실시간으로 입력받아 배열에 저장한 데이터를 출력함
            guard let key = NutrientsTag.init(rawValue: indexPath.row)?.description else { return cellOfLongInput }
            if let userInputData = nutrientDataList[key] {
                cellOfLongInput.amountTextField.text = userInputData as? String
            }
            
            // 수정 시 기존에 입력했던 값들을 출력함
            guard let value = nutrientDataList[key] else { return cellOfLongInput }
            cellOfLongInput.amountTextField.text = String(describing: value)
            return cellOfLongInput
        }
    }
    
    func makeCustomFoodTitle(by data: CustomFood) -> String { // 언어에 따른 출력
        if NSLocalizedString("app_language",bundle:bundle, comment: "") == "ko" {
            return data.ko
        }else {
            return data.en
        }
    }
    
    func isCellOfShortInput(row: Int) -> Bool {
        return row > 2
    }
    
}

extension DetailCustomFoodViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        let index = textField.tag
        if index == 0 {
            nutrientDataList.updateValue(textField.text ?? "", forKey: "foodname")
        }else {
            nutrientDataList.updateValue(textField.text ?? 0.0, forKey: NutrientsTag.init(rawValue: index)?.description ?? "")
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let indexPath = IndexPath(row: textField.tag, section: 0)
        let backgroundColor = UIColor.hexStringToUIColor(hex: "095d6a")
        self.makeUnderLineView(indexPath: indexPath, color: backgroundColor)
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        let indexPath = IndexPath(row: textField.tag, section: 0)
        let backgroundColor = UIColor.hexStringToUIColor(hex: "dddddd")
        self.makeUnderLineView(indexPath: indexPath, color: backgroundColor)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard var text = textField.text else { return false }
        text = text + string
        if string == "", range.location == 0 { text = "" }
        let index = textField.tag
        nutrientDataList.updateValue(text, forKey: NutrientsTag.init(rawValue: index)?.description ?? "")
        return true
    }
    
    func makeUnderLineView(indexPath: IndexPath, color: UIColor) {
        if let cell = customFoodTableView.cellForRow(at: indexPath) as? LongInputCustomFoodTableViewCell {
            cell.underLineView.backgroundColor = color
        }
        if let cell = customFoodTableView.cellForRow(at: indexPath) as? InputCustomFoodTableViewCell {
            cell.underLineView.backgroundColor = color
        }
    }
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillShow), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }

    func unregisterForKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    @objc func keyboardWillShow(note: NSNotification) {
        if let keyboardSize = (note.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size {
            if keyboardSize.height == 0.0 { return }
            
            if keyboardSize.height == self.heightOfPreviouskeyboard { return }
            let size = keyboardSize.height - self.heightOfPreviouskeyboard
            self.heightOfPreviouskeyboard = keyboardSize.height
            
            self.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width:  self.view.frame.width, height:  self.view.frame.height - size)
        }
    }

    @objc func keyboardWillHide(note: NSNotification) {
        if let keyboardSize = (note.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size {
            //self.view.frame.origin.y += self.heightOfPreviouskeyboard
            self.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height + self.heightOfPreviouskeyboard)
            self.heightOfPreviouskeyboard = 0
        }
    }
}

enum NutrientsTag: Int {
    case foodname = 0, manufacturer, unit, foodtype, totalgram, calories, carbonhydrate, protein, fat, saturatedfat, transfat, cholesterol, dietrayfiber, calcium, sodium, sugar, vitamina, vitaminb, vitaminc, vitamind, vitamine, non
    var description: String {
        switch self {
            case .foodname: return "foodname"
            case .manufacturer: return "manufacturer"
            case .unit: return "unit"
            case .foodtype: return "foodtype"
            case .totalgram: return "totalgram"
            case .calories: return "calories"
            case .carbonhydrate: return "carbonhydrate"
            case .protein: return "protein"
            case .fat: return "fat"
            case .saturatedfat: return "saturatedfat"
            case .transfat: return "transfat"
            case .cholesterol: return "cholesterol"
            case .dietrayfiber: return "dietrayfiber"
            case .calcium: return "calcium"
            case .sodium: return "sodium"
            case .sugar: return "sugar"
            case .vitamina: return "vitamina"
            case .vitaminb: return "vitaminb"
            case .vitaminc: return "vitaminc"
            case .vitamind: return "vitamind"
            case .vitamine: return "vitamine"
            case .non: return ""
        }
    }
    static let order = [foodname, manufacturer, unit, foodtype, totalgram, calories, carbonhydrate, protein, fat, saturatedfat, transfat, cholesterol, dietrayfiber, calcium, sodium, sugar, vitamina, vitaminb, vitaminc, vitamind, vitamine]
    //carbonhydrate, dietrayFiber
}

enum DetailViewMode {
    case add, edit
}
