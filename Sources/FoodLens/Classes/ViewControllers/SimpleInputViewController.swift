//
//  SimpleInputViewController.swift
//  DietCameraAI
//
//  Created by HwangChun Lee on 02/01/2019.
//  Copyright © 2019 leeint. All rights reserved.
//

import UIKit

@available(iOS 10.0, *)
class SimpleInputViewController: UIViewController {
    weak var superViewController : AmountViewController? = nil
    
    @IBOutlet weak var quarterCalorie: UILabel!
    @IBOutlet weak var halfCalorie: UILabel!
    @IBOutlet weak var threeFourthCalorie: UILabel!
    @IBOutlet weak var normalCalorie: UILabel!
    
    @IBOutlet weak var quarterAmount: UILabel!
    @IBOutlet weak var halfAmount: UILabel!
    @IBOutlet weak var threeFourceAmount: UILabel!
    @IBOutlet weak var normalAmount: UILabel!
    
    @IBOutlet weak var quarterAmountView: UIView!
    @IBOutlet weak var halfAmountView: UIView!
    @IBOutlet weak var threeThirdAmountView: UIView!
    @IBOutlet weak var normalAmountView: UIView!
    
    @IBOutlet weak var servingUnitTextView: UILabel!
    
    var amountLogSended : Bool = false
    
    let selectedColor : UIColor = UIColor(red: 0xcf/0xff, green: 0x32/0xff, blue: 0x35/0xff, alpha: 1)
    let deselectedColor : UIColor = UIColor(red: 0x33/0xff, green: 0x33/0xff, blue: 0x33/0xff, alpha: 1)
    let deselectedCalorieColor : UIColor = UIColor(red: 0x6f/0xff, green: 0x71/0xff, blue: 0x79/0xff, alpha: 1)
    
    var selectedFoodPosition : FoodPosition? = nil
    var selectedIndex : Int = 0
    
    let bundle: Bundle = foodlensResourceBundle
    
    func addShadow(view : UIView) {
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 0.0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        addShadow(view: quarterAmountView)
        addShadow(view: halfAmountView)
        addShadow(view: threeThirdAmountView)
        addShadow(view: normalAmountView)
        self.view.translatesAutoresizingMaskIntoConstraints = false
        
        //light 모드로 강제진행
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
    }
    
    func setServingUnitView() {
        guard let nutrition = selectedFoodPosition?.userSelectedFood?.nutrition else {
            servingUnitTextView.text = ""
            return
        }
        servingUnitTextView.text = "\(nutrition.unit) (\(Int(nutrition.totalgram))g)"
    }
    
    func setUnitView() {
        guard let nutrition = selectedFoodPosition?.userSelectedFood?.nutrition else {
            return
        }
        
        let unitList = Util.parseStandardUnit(standardUnit: nutrition.unit)
        var unit = "인분"
        var value = 0
        if unitList.count > 0 && unitList[0].1.count > 0 {
            unit = unitList[0].1
            value = Int(unitList[0].0) ?? 0
        }
        
        if unit.lowercased() == "g" || unit.lowercased() == "ml" {
            quarterAmount.text = String(format: "\(value/4) \(unit)")
            halfAmount.text = String(format: "\(value/2) \(unit)")
            threeFourceAmount.text = String(format: "\(value*3/4) \(unit)")
            normalAmount.text = String(format: "\(value) \(unit)")
        }
        else {
            quarterAmount.text = String(format: "1/4 \(unit)")
            halfAmount.text = String(format: "1/2 \(unit)")
            threeFourceAmount.text = String(format: "3/4 \(unit)")
            normalAmount.text = String(format: "1 \(unit)")
        }
        
        let calorie = nutrition.calories
        quarterCalorie.text = String(format: "%.0f Kcal", calorie / 4)
        halfCalorie.text = String(format: "%.0f Kcal", calorie / 2)
        threeFourthCalorie.text = String(format: "%.0f Kcal", calorie * 3 / 4)
        normalCalorie.text = String(format: "%.0f Kcal", calorie)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let selectedIndex: Int = superViewController?.selectedIndex {
            selectedFoodPosition = superViewController?.predictResult?.foodPositionList[safe: selectedIndex] ?? .init()
        }
        
        setUnitView()
        setAmountSelected()
        setServingUnitView()
        
        setAmountSelected()
    }
    
    func setAmountSelected() {
        guard let amountValue = selectedFoodPosition?.eatAmount else {
            normalAmount.textColor = deselectedColor
            quarterAmount.textColor = deselectedColor
            halfAmount.textColor = deselectedColor
            threeFourceAmount.textColor = deselectedColor
            
            quarterCalorie.textColor = deselectedColor
            halfCalorie.textColor = deselectedColor
            threeFourthCalorie.textColor = deselectedColor
            normalCalorie.textColor = deselectedColor
            
            return
        }
        
        switch amountValue {
        case 0.25:
            normalAmount.textColor = deselectedColor
            quarterAmount.textColor = selectedColor
            halfAmount.textColor = deselectedColor
            threeFourceAmount.textColor = deselectedColor
            
            quarterCalorie.textColor = selectedColor
            halfCalorie.textColor = deselectedColor
            threeFourthCalorie.textColor = deselectedColor
            normalCalorie.textColor = deselectedColor
        case 0.5:
            normalAmount.textColor = deselectedColor
            quarterAmount.textColor = deselectedColor
            halfAmount.textColor = selectedColor
            threeFourceAmount.textColor = deselectedColor
            
            quarterCalorie.textColor = deselectedColor
            halfCalorie.textColor = selectedColor
            threeFourthCalorie.textColor = deselectedColor
            normalCalorie.textColor = deselectedColor
        case 0.75:
            normalAmount.textColor = deselectedColor
            quarterAmount.textColor = deselectedColor
            halfAmount.textColor = deselectedColor
            threeFourceAmount.textColor = selectedColor
            
            quarterCalorie.textColor = deselectedColor
            halfCalorie.textColor = deselectedColor
            threeFourthCalorie.textColor = selectedColor
            normalCalorie.textColor = deselectedColor
        case 1.0:
            normalAmount.textColor = selectedColor
            quarterAmount.textColor = deselectedColor
            halfAmount.textColor = deselectedColor
            threeFourceAmount.textColor = deselectedColor
            
            quarterCalorie.textColor = deselectedColor
            halfCalorie.textColor = deselectedColor
            threeFourthCalorie.textColor = deselectedColor
            normalCalorie.textColor = selectedColor
        default:
            normalAmount.textColor = deselectedColor
            quarterAmount.textColor = deselectedColor
            halfAmount.textColor = deselectedColor
            threeFourceAmount.textColor = deselectedColor
            
            quarterCalorie.textColor = deselectedColor
            halfCalorie.textColor = deselectedColor
            threeFourthCalorie.textColor = deselectedColor
            normalCalorie.textColor = deselectedColor
        }
    }
    
    func updateAmount() {
        guard let nutrition = selectedFoodPosition?.userSelectedFood?.nutrition else {
            
            return
        }
        
        let unitList = Util.parseStandardUnit(standardUnit: nutrition.unit)
        var unit = "인분"
        if unitList[0].1.count > 0 {
            unit = unitList[0].1
        }
        
        quarterAmount.text = String(format: "1/4 \(unit)")
        halfAmount.text = String(format: "1/2 \(unit)")
        threeFourceAmount.text = String(format: "3/4 \(unit)")
        normalAmount.text = String(format: "1 \(unit)")
        
        let calorie = nutrition.calories
        quarterCalorie.text = String(format: "%.0f Kcal", calorie / 4)
        halfCalorie.text = String(format: "%.0f Kcal", calorie / 2)
        threeFourthCalorie.text = String(format: "%.0f Kcal", calorie * 3 / 4)
        normalCalorie.text = String(format: "%.0f Kcal", calorie)
    }
    
    func updateFoodInfo(selected : FoodPosition) {
        selectedFoodPosition = selected
        setUnitView()
    }
    
    func calcNewCalorie(oldAmount : Float, oldCalorie : Float, newAmount : Float) -> Float {
        if oldAmount == 0 {
            return 0
        }
        let factor = oldCalorie / oldAmount
        return factor * newAmount
    }
    
    func calcNewGram(oldAmount : Float, oldGram : Float, newAmount : Float) -> Float {
        if oldAmount == 0 {
            return 0
        }
        let factor = oldGram / oldAmount
        return factor * newAmount
    }
    
    @IBAction func didQuarterPressed(_ sender: Any) {
        quarterCalorie.textColor = selectedColor
        quarterAmount.textColor = selectedColor
        
        halfCalorie.textColor = deselectedCalorieColor
        threeFourthCalorie.textColor = deselectedCalorieColor
        normalCalorie.textColor = deselectedCalorieColor
        
        halfAmount.textColor = deselectedColor
        threeFourceAmount.textColor = deselectedColor
        normalAmount.textColor = deselectedColor
        
          selectedFoodPosition?.eatAmount = 0.25

        if let foodID: Int = selectedFoodPosition?.userSelectedFood?.foodId,
           let eatAmount: Float = selectedFoodPosition?.eatAmount {
            _ = superViewController?.calculateCalories(foodID: foodID, eatAmount: eatAmount)
        }
    }
    
    @IBAction func didHalfPressed(_ sender: Any) {
        halfCalorie.textColor = selectedColor
        halfAmount.textColor = selectedColor
        
        quarterCalorie.textColor = deselectedCalorieColor
        threeFourthCalorie.textColor = deselectedCalorieColor
        normalCalorie.textColor = deselectedCalorieColor
        
        quarterAmount.textColor = deselectedColor
        threeFourceAmount.textColor = deselectedColor
        normalAmount.textColor = deselectedColor
        
        selectedFoodPosition?.eatAmount = 0.5

        if let foodID: Int = selectedFoodPosition?.userSelectedFood?.foodId,
           let eatAmount: Float = selectedFoodPosition?.eatAmount {
            _ = superViewController?.calculateCalories(foodID: foodID, eatAmount: eatAmount)
        }
    }
    
    @IBAction func didThreeFourthPressed(_ sender: Any) {
        threeFourthCalorie.textColor = selectedColor
        threeFourceAmount.textColor = selectedColor
        
        halfCalorie.textColor = deselectedCalorieColor
        quarterCalorie.textColor = deselectedCalorieColor
        normalCalorie.textColor = deselectedCalorieColor
        
        halfAmount.textColor = deselectedColor
        quarterAmount.textColor = deselectedColor
        normalAmount.textColor = deselectedColor
        
        selectedFoodPosition?.eatAmount = 0.75

        if let foodID: Int = selectedFoodPosition?.userSelectedFood?.foodId,
           let eatAmount: Float = selectedFoodPosition?.eatAmount {
            _ = superViewController?.calculateCalories(foodID: foodID, eatAmount: eatAmount)
        }
    }
    
    @IBAction func didNormalPressed(_ sender: Any) {
        normalCalorie.textColor = selectedColor
        normalAmount.textColor = selectedColor
        
        halfCalorie.textColor = deselectedCalorieColor
        quarterCalorie.textColor = deselectedCalorieColor
        threeFourthCalorie.textColor = deselectedCalorieColor
        
        halfAmount.textColor = deselectedColor
        quarterAmount.textColor = deselectedColor
        threeFourceAmount.textColor = deselectedColor
        
        selectedFoodPosition?.eatAmount = 1.0

        if let foodID: Int = selectedFoodPosition?.userSelectedFood?.foodId,
           let eatAmount: Float = selectedFoodPosition?.eatAmount {
            _ = superViewController?.calculateCalories(foodID: foodID, eatAmount: eatAmount)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
