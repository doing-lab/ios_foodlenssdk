//
//  ChoosedDateAlertViewController.swift
//  FoodLens
//
//  Created by Eunjin on 2020/12/16.
//

import UIKit

class ChoosedDateAlertViewController: UIViewController {

    @IBOutlet weak var chooseAlertTextView: UITextView!
    @IBOutlet weak var captureTimeButton: UIButton!
    @IBOutlet weak var currentTimeButton: UIButton!
    var dateOfCaptureImage: Date?
    var userSelectedDate: String?
    var cameraView: CameraViewController?
    let bundle: Bundle = foodlensResourceBundle
    
//    var userSelecteddate: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        chooseAlertInitialized()
    }
    
    func chooseAlertInitialized() {
        let popupTitleString = NSLocalizedString("camera_chooseDate_popup", bundle: bundle, comment: "")
        let captureTimeString = NSLocalizedString("camera_chooseDate_captureTime", bundle: bundle, comment: "")
        let currentTimeString = NSLocalizedString("camera_chooseDate_currentTime", bundle: bundle, comment: "")
        
        guard let date = addImageCaptureDate(title: popupTitleString) else { return }
        chooseAlertTextView.text = date
        
        captureTimeButton.setTitle(captureTimeString, for: .normal)
        currentTimeButton.setTitle(currentTimeString, for: .normal)
        chooseAlertTextView.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func addImageCaptureDate(title: String) -> String? {
        guard let date = dateOfCaptureImage else { return nil }
        let dateString = makeDateByLocalized(date: date)
        return title.replace(target: "[]", withString: dateString)
    }
    
    func makeDateByLocalized(date: Date) -> String {
        var dateString = ""
        dateString.append(date.month)
        dateString.append(" ")
        dateString.append(date.date)
        dateString.append(" ")
        dateString.append(date.time)
        return dateString
    }
    
    func makeDateToAddSelectedTime() -> Date {
        let onlyDateFormatter = DateFormatter()
        onlyDateFormatter.dateFormat = "yyyy.MM.dd"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat="yyyy.MM.dd HH:mm:ss"
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat="HH:mm:ss"
        
        let userSelectedDate = CameraViewController.predictResult.eatDate.toString
        guard let currentDate = onlyDateFormatter.date(from: userSelectedDate) else { return Date() }
        let today = Date()
        let dateString = dateFormatter.string(from: today)
        let currentTime = String(dateString.split(separator: " ").last ?? "")
        let time = currentTime.split(separator: ":").map({ Int(String($0)) })
        
        let hour = time[0] ?? 0
        let minute = time[1] ?? 0
        let second = time[2] ?? 0
        guard let selectedDateAndCurrentTime = Calendar.current.date(bySettingHour: hour, minute: minute, second: second, of: currentDate) else { return Date() }
        return selectedDateAndCurrentTime
    }
    
    @IBAction func captureTimeButtonClicked(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat="yyyy.MM.dd HH:mm:ss"
        guard let date = dateOfCaptureImage else { return }
        FoodLens.eatDate = date
        self.dismiss(animated: false, completion: {
            self.cameraView?.runAnalisysImage()
        })
    }
    
    @IBAction func currentTimeButtonClicked(_ sender: Any) {
        self.dismiss(animated: false, completion: {
            FoodLens.eatDate = self.makeDateToAddSelectedTime()
            self.cameraView?.runAnalisysImage()
        })
    }
}

extension String {
    func replace(target: String, withString: String) -> String {
        return self.replacingOccurrences(of: target, with: withString, options: NSString.CompareOptions.literal, range: nil)
    }
}

extension Date {
    var month: String {
        let dateFormatter = DateFormatter()
        dateFormatter.setLocalizedDateFormatFromTemplate("MMM")
        return dateFormatter.string(from: self)
    }
    var date: String {
        let dateFormatter = DateFormatter()
        dateFormatter.setLocalizedDateFormatFromTemplate("dd")
        return dateFormatter.string(from: self)
    }
    var time: String {
        let dateFormatter = DateFormatter()
        dateFormatter.setLocalizedDateFormatFromTemplate("HH:mm")
        return dateFormatter.string(from: self)
    }

}
