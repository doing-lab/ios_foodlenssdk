//
//  AmountViewController_New.swift
//  DietCameraAI
//
//  Created by HwangChun Lee on 29/05/2019.
//  Copyright © 2019 leeint. All rights reserved.
//

import UIKit


class FoodAmountNameCell : UICollectionViewCell {
    @IBOutlet weak var foodImageView: DesignableImageView!
    @IBOutlet weak var foodNameView: UILabel!
    @IBOutlet weak var deleteButton: DesignableImageView!
    
    let selectedTextColor : UIColor = UIColor(red:0.80, green:0.17, blue:0.18, alpha:1.0)
    let unselectedTextColor : UIColor = UIColor(red:0.13, green:0.13, blue:0.13, alpha:1.0)
    
    var isNutritionCell : Bool = false
    var index : Int? = nil
    weak var collectionView : UICollectionView?
    var foodDeleteControl : FoodDeleteProtocol?
    
    let bundle: Bundle = foodlensResourceBundle
    
    override func prepareForReuse() {
        if isNutritionCell == false {
            foodImageView.image = nil
            foodNameView.text = ""
        }
    }
    
    func setDeleteButtonHandler() {
        deleteButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didDeletePressed)))
    }
    
    @objc func didDeletePressed() {
        guard let foodDeleteControl = foodDeleteControl, let index = index else {
            return
        }
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            let alertController = UIAlertController(title: NSLocalizedString("deleteafter", tableName: nil, bundle: bundle, value: "", comment: ""),
                                                    message: NSLocalizedString("deleteconfirm", tableName: nil, bundle: bundle, value: "", comment: ""),
                                                    preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: NSLocalizedString("ok", tableName: nil, bundle: bundle, value: "", comment: ""), style: .default) { (action) in
                foodDeleteControl.foodDeleted(index: index)
            }
            alertController.addAction(okAction)
            
            let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", tableName: nil, bundle: bundle, value: "", comment: ""), style: .default) { action in
            }
            alertController.addAction(cancelAction)
            
            topController.present(alertController, animated: true, completion: nil)
        }
    }
    
    func set(selected : Bool) {
        if selected == true {
            if deleteButton != nil {
                deleteButton.isHidden = false
            }
            foodImageView.borderWidth = 2.0
            foodNameView.textColor = selectedTextColor
        }
        else {
            if deleteButton != nil {
                deleteButton.isHidden = true
            }
            foodImageView.borderWidth = 0.0
            foodNameView.textColor = unselectedTextColor
        }
    }
}

protocol FoodChangeProtocol {
    func changeFood(food : Food, keyType: ChangeKeyType)
}

protocol FoodAddProtocol {
    //func addFood(foodPosition : FoodPosition, keyType: ChangeKeyType)
    func addFood(foodInfo: FoodInfo, searchedFoodName: String, keyType: ChangeKeyType)
}

protocol FoodDeleteProtocol {
    func foodDeleted(index : Int)
}

class GradientView: UIView {
    let colors = [
        UIColor.white.withAlphaComponent(0.0),
        UIColor.white.withAlphaComponent(0.3),
        UIColor.white.withAlphaComponent(0.6),
        UIColor.white.withAlphaComponent(0.9),
        UIColor.white.withAlphaComponent(1)
    ]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateView()
    }
    
    override class var layerClass: AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
    func updateView() {
        let layer = self.layer as! CAGradientLayer
        layer.colors = colors.map{$0.cgColor}
        layer.shadowOpacity = 0.0;
    }
}

/**
 음식정보 보여줌
 */
class AmountViewController : UITableViewController, UICollectionViewDelegate, UICollectionViewDataSource, FoodAddProtocol, FoodChangeProtocol, FoodDeleteProtocol {
    
    //MARK: - define value
    
    private let IMG_STORE_PATH:String = "foodlensStore"

    //MARK: - View
    @IBOutlet weak var foodImageView: UIImageView!
    @IBOutlet weak var gradationView: GradientView!
    @IBOutlet weak var seeOtherImageView: UIImageView!
    @IBOutlet weak var foodAddButtonImage: UIImageView!
    @IBOutlet weak var foodCollectionView: UICollectionView!
    
    //MARK: - Button
    @IBOutlet weak var dateButton: DesignableButton!
    @IBOutlet weak var timeButton: DesignableButton!
    @IBOutlet weak var mealTypeButton: DesignableButton!
    @IBOutlet weak var seeOtherButton: UIButton!
    
    //MARK: - Label
    @IBOutlet weak var totalCalorieView: UILabel!
    @IBOutlet weak var seeOtherTextView: UILabel!
    @IBOutlet weak var foodAddButtonText: UILabel!
    
    
    weak var frameView : AmountFrameViewController?
    weak var inputAmountView : AmountInputViewController?
    
    static var noPicImage : UIImage? = nil
    
    var dateSelectPopup : DateSelectPopup? = nil
    var timeSelectPopup : TimeSelectPopup? = nil
    var mealTypeSelectPopup : MealTypeSelectPopup? = nil
    var foodAddPopup : FoodAddPopup? = nil
    var foodImageSize : CGSize? = nil
    
    
    weak var predictResult : PredictionResult? = CameraViewController.predictResult
    
    var selectedIndex : Int = 0
    
    public static var addFoodImage : UIImage? = nil
    public static var nutritionImage : UIImage? = nil
    public static var foodDeleteImage : UIImage? = nil
    public static var seeOtherResultImage : UIImage? = nil
    
    let bundle: Bundle = foodlensResourceBundle
    
    var backgroundColorList : [UIColor] = [
        UIColor(red:0.00, green:0.63, blue:0.58, alpha:0.8), //#00a093
        UIColor(red:1.00, green:0.00, blue:0.45, alpha:0.8), //#ff0072
        UIColor(red:0.45, green:0.00, blue:1.00, alpha:0.8), //#7300ff
        UIColor(red:0.83, green:0.45, blue:0.00, alpha:0.8), //#d37200
        UIColor(red:1.00, green:0.34, blue:0.08, alpha:0.8), //#ff5615
        UIColor(red:0.86, green:0.00, blue:0.81, alpha:0.8), //#dc00cf
        UIColor(red:0.00, green:0.40, blue:1.00, alpha:0.8), //#0066ff
        UIColor(red:0.07, green:0.67, blue:0.10, alpha:0.8), //#12ac19
        UIColor(red:1.00, green:0.14, blue:0.00, alpha:0.8), //#ff2400
        UIColor(red:0.00, green:0.07, blue:1.00, alpha:0.8), //#0012ff
        UIColor(red:1.00, green:0.00, blue:0.19, alpha:0.8)  //#ff0030
    ]
    
    var isScrolled : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let imagePath = CameraViewController.predictResult.predictedImagePath?.split(separator: "/").last,
            let fullImage = Util.load(path: IMG_STORE_PATH, fileName: String(imagePath)) {
            
            if CameraViewController.predictResult.version == 0 {
                //Old version of prediction result only use 1024x1024 size of box coordication, so to support backward compatability keep this value.
                foodImageSize = CGSize(width: 1024, height: 1024)
            } else {
                foodImageSize = CGSize(width: fullImage.size.width, height: fullImage.size.height)
            }
            
            self.foodImageView.image = fullImage
            self.foodImageView.isHidden = false
            self.gradationView.isHidden = false
        }
        else {
            self.foodImageView.isHidden = true
            self.gradationView.isHidden = true
        }
        
        setDateButton()
        setTimeButton()
        setEatTypeButton()
        
        addFoodAreaButton()
        _ = calculateCalories()
        
        setSeeOthersView()
        
        foodAddButtonImage.tintColor = FoodLens.buttonTheme.backgroundColor
        foodAddButtonText.textColor = FoodLens.buttonTheme.textColor
        
        NotificationCenter.default.addObserver(self, selector: #selector(onPopupClosed), name: NSNotification.Name(rawValue: "notificationClosedPopup"), object: nil)

        //light 모드로 강제진행
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.frameView?.navigationController?.navigationBar.isTranslucent = true
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = .white
            self.navigationController?.navigationBar.standardAppearance = appearance;
            self.navigationController?.navigationBar.scrollEdgeAppearance = self.navigationController?.navigationBar.standardAppearance
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isScrolled == false {
            let navHeight = navigationController?.navigationBar.bounds.height ?? 0
            let bottomHeight = frameView?.bottomView.bounds.height ?? 0
            let statusHeight = UIApplication.shared.statusBarView?.bounds.height ?? 0
            
            let height = UIScreen.main.bounds.height - navHeight - bottomHeight - statusHeight
            
            UIView.animate(withDuration: 0.5, delay: 0.8, options: .curveEaseOut, animations: {
                self.tableView.scrollRectToVisible(CGRect(x: 0, y: UIScreen.main.bounds.width, width: UIScreen.main.bounds.width, height: height), animated: false)
            }, completion: { result in
                self.frameView?.setNavigationBarColor(color:  FoodLens.navigationBarTheme.backgroundColor.withAlphaComponent(1))
                self.isScrolled = true
            })
        }
        else {
            let offset = tableView.contentOffset.y / (foodImageView.bounds.height)
            if offset > 0.7 {
                frameView?.setNavigationBarColor(color: FoodLens.navigationBarTheme.backgroundColor.withAlphaComponent(1))
            }
            else {
                frameView?.setNavigationBarColor(color: FoodLens.navigationBarTheme.backgroundColor.withAlphaComponent(offset))
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.frameView?.setNavigationBarColor(color:  FoodLens.navigationBarTheme.backgroundColor.withAlphaComponent(1))
        self.frameView?.navigationController?.navigationBar.isTranslucent = false
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let frameView = frameView, scrollView == tableView else {
            return
        }
        
        if (scrollView.isDragging) || (scrollView.isDecelerating) {
            // Do what you want when user is dragging
            // Do what you want when table is decelerating after finished dragging
            let offset = scrollView.contentOffset.y / (foodImageView.bounds.height)
            if offset > 0.7 {
                frameView.setNavigationBarColor(color: FoodLens.navigationBarTheme.backgroundColor.withAlphaComponent(1))
            }
            else {
                frameView.setNavigationBarColor(color: FoodLens.navigationBarTheme.backgroundColor.withAlphaComponent(offset))
            }
        } else {
            // Do what you want when table is scrolling programmatically.
        }
    }
    
    func foodDeleted(index: Int) {
        guard let frameView = frameView else {
            return
        }
        
        let remainCount = CameraViewController.predictResult.foodPositionList.count
        if remainCount <= 1 {
            frameView.showDeleteWarning()
            return
        }
        
        let resultList = CameraViewController.predictResult.foodPositionList.enumerated()
            .filter{ $0.offset != index }
            .map{ $0.element }
        CameraViewController.predictResult.foodPositionList = resultList
        
        selectedIndex -= 1
        if selectedIndex < 0 {
            selectedIndex = 0
        }
        
        if selectedIndex >= CameraViewController.predictResult.foodPositionList.count {
            selectedIndex = CameraViewController.predictResult.foodPositionList.count - 1
        }
        
        addFoodAreaButton()
        _ = calculateCalories()
        foodCollectionView.reloadData()
        inputAmountView?.changeFoodPosition(index : selectedIndex)
    }
    
    
    func postPositionText(_ name: String) -> String {
        guard let lastText = name.last else { // 글자의 마지막 부분을 가져옴
            return ""
        }
        
        let unicodeVal = UnicodeScalar(String(lastText))?.value // 유니코드 전환
        guard let value = unicodeVal else {
            return ""
        }
        
        if (value < 0xAC00 || value > 0xD7A3) {
            return ""
        }
        
        let last = (value - 0xAC00) % 28                        // 종성인지 확인
        let str = last > 0 ? "이" : "가"      // 받침있으면 을 없으면 를 반환
        return str
    }
    
    func setSeeOthersView() {
        if selectedIndex < 0 || CameraViewController.predictResult.foodPositionList.count <= selectedIndex  {
            return
        }
        
        seeOtherImageView.tintColor = FoodLens.buttonTheme.backgroundColor
        
        let food = CameraViewController.predictResult.foodPositionList[selectedIndex]
        guard let foodname = food.userSelectedFood?.foodName else {
            return
        }
        
        let font = seeOtherTextView.font
        let formattedString = NSMutableAttributedString()
        
        if food.userSelectedFood?.keyName == "nonefood" {
            let locale = Locale.current.languageCode ?? "en"
            if locale == "ko" {
                formattedString.normal("음식을 선택해 주세요")
            }
            else {
                formattedString.normal("Please choose food")
            }
        }
        else {
            let locale = Locale.current.languageCode ?? "en"
            if locale == "ko" {
                formattedString
                    .bold(foodname, size: font?.pointSize ?? 14.0)
                    .normal("\(postPositionText(foodname)) 아닌가요?")
                
            }
            else {
                formattedString
                    .normal("Isn't it ")
                    .bold(foodname, size: font?.pointSize ?? 14.0)
                    .normal("?")
            }
        }
        
        seeOtherTextView.attributedText = formattedString
    }
    
    func setSeeOthersButton() {
        let font = seeOtherButton.titleLabel?.font
        let buttonTitle = NSLocalizedString("see_more", tableName: nil, bundle: bundle, value: "", comment: "")
        let textRange = NSMakeRange(0, buttonTitle.count)
        let attributedText = NSMutableAttributedString(string: buttonTitle)
        attributedText.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: textRange)
        
        if let font = font {
            attributedText.addAttribute(.font, value: font, range: textRange)
        }
        
        attributedText.addAttribute(.foregroundColor, value: FoodLens.buttonTheme.textColor, range: textRange)
        seeOtherButton.setAttributedTitle(attributedText, for: .normal)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let foodList = CameraViewController.predictResult.foodPositionList
        if indexPath.row == foodList.count {
            //go to nutrition information
            frameView?.performSegue(withIdentifier: "NutritionFacts", sender: nil)
            return
        }
        
        collectionView.visibleCells.forEach { cell in
            guard let cell = cell as? FoodAmountNameCell else {
                return
            }
            if cell.isNutritionCell == false {
                cell.set(selected: false)
            }
        }
        
        guard let newCell = collectionView.cellForItem(at: indexPath) as? FoodAmountNameCell else {
            return
        }
        
        newCell.set(selected: true)
        selectedIndex = indexPath.row
        setSeeOthersView()
        
        inputAmountView?.changeFoodPosition(index : newCell.index ?? 0)
    }
    
    @objc func onPopupClosed() {
        guard let _:MealType? = CameraViewController.predictResult.mealType else { return }
        UIView.setAnimationsEnabled(false)
        predictResult?.mealType = CameraViewController.predictResult.mealType
        setEatTypeButton()
        self.mealTypeButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        if let popup = mealTypeSelectPopup {
            popup.removeAnimate()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let foodList = CameraViewController.predictResult.foodPositionList
        return (foodList.count) + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? FoodAmountNameCell else {
            return
        }
        
        if indexPath.row == selectedIndex {
            cell.set(selected: true)
        }
        else if cell.isNutritionCell != true {
            cell.set(selected: false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let foodList = CameraViewController.predictResult.foodPositionList
        if indexPath.row == foodList.count {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NutritionCell", for: indexPath) as! FoodAmountNameCell
            cell.isNutritionCell = true
            cell.index = nil
            cell.foodDeleteControl = self
            return cell
        }
        else if indexPath.row < foodList.count {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FoodNameCell", for: indexPath) as! FoodAmountNameCell
            
            if let imagePath = foodList[indexPath.row].foodImagepath?.split(separator: "/").last,
                let image = Util.load(path: IMG_STORE_PATH, fileName: String(imagePath)) {
                cell.foodImageView.image = image
                cell.foodImageView.contentMode = .scaleToFill
            }
            else {
                cell.foodImageView.backgroundColor = UIColor(red:0.87, green:0.87, blue:0.87, alpha:1.0)
                cell.foodImageView.image = UIImage(named: "pdf_no_pic", in: bundle, compatibleWith: nil)
                cell.foodImageView.contentMode = .scaleAspectFit
            }
            
            cell.index = indexPath.row
            cell.foodNameView.text = foodList[indexPath.row].userSelectedFood?.foodName ?? ""
            cell.setDeleteButtonHandler()
            
            if indexPath.row == selectedIndex {
                cell.set(selected: true)
            }
            else {
                cell.set(selected: false)
            }
            
            setSeeOthersView()
            
            cell.foodDeleteControl = self
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FoodNameCell", for: indexPath) as! FoodAmountNameCell
            cell.index = nil
            //cell.collectionView = collectionView
            cell.foodDeleteControl = self
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = super.tableView(tableView, heightForRowAt: indexPath)
        if indexPath.row == 0 {
            return UIScreen.main.bounds.width
        }
        
        return height
    }
    
    //MARK: - setting UI/UX
    
    // button setting
    func setDateButton() {
        
        //먹은 날 정의
        guard let eatDate = predictResult?.eatDate else {
            return
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat="yyyy.MM.dd"
        
        //커스텀 버튼 property 설정
         //dateButton.borderColor = FoodLens.toolbarButtonTheme.buttonTheme.borderColor
        
        //TODO: navTheme toolbarTheme buttonTheme widgetButtonTheme가 어디에 사용되야 하는지.
        //ios에서 제공되는 버튼 property 설정
        //잘리는 문자없이 택스트가 보이게
        dateButton.titleLabel?.adjustsFontSizeToFitWidth = true
        dateButton.titleLabel?.minimumScaleFactor = 0.5
        dateButton.setTitleColor(FoodLens.buttonTheme.textColor, for: .normal)
        dateButton.setTitle(dateFormatter.string(from: eatDate), for: .normal)
        //dateButton.backgroundColor = FoodLens.toolbarButtonTheme.buttonTheme.backgroundColor
    }
    
    func setTimeButton() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat="a hh:mm"
        let date = CameraViewController.predictResult.eatDate
        timeButton.setTitle(dateFormatter.string(from: date), for: .normal)
    }
    
    // button setting
    func setEatTypeButton() {
        guard let mealType : MealType = predictResult?.mealType else {
            return
        }
        
        mealTypeButton.setTitleColor(FoodLens.buttonTheme.textColor, for: .normal)
        
        //글씨 최소 축소 포인트 설정
        mealTypeButton.titleLabel?.minimumScaleFactor = 0.5
        switch(mealType) {
        case .breakfast:
            mealTypeButton.setTitle(NSLocalizedString("breakfast", tableName: nil, bundle: bundle, value: "", comment: ""), for: .normal)
        case .lunch:
            mealTypeButton.setTitle(NSLocalizedString("lunch", tableName: nil, bundle: bundle, value: "", comment: ""), for: .normal)
        case .dinner:
            mealTypeButton.setTitle(NSLocalizedString("dinner", tableName: nil, bundle: bundle, value: "", comment: ""), for: .normal)
        case .snack, .morning_snack, .afternoon_snack, .late_night_snack:
            self.mealTypeButton.setTitle(NSLocalizedString("snack", bundle: bundle, comment: ""), for: .normal)

        }
        
        
    }
    
    //MARK: -
    
    func calculateCalories(foodID: Int, eatAmount: Float) -> Float {
        for i in 0..<(predictResult?.foodPositionList.count ?? 0) {
            if predictResult?.foodPositionList[i].userSelectedFood?.foodId == foodID {
                predictResult?.foodPositionList[i].eatAmount = eatAmount
            }
        }
        return calculateCalories()
    }
    
    func calculateCalories() -> Float {
        guard let positionList = predictResult?.foodPositionList else {
            return 0.0
        }
        
        let totalCalorie = positionList
            .filter { $0.isDelete != true }
            .map { $0.eatAmount * ($0.userSelectedFood?.nutrition?.calories ?? 0) } // [(amount*calorie), (amount*calorie), (amount*calorie) ...]
            .reduce(0) { (result : Float, calories) -> Float in result + calories } // 배열 요소를 순회하며 값을 다 더함
        
        self.totalCalorieView.text = String(format: "(%.0f Kcal)", totalCalorie)
        return totalCalorie
    }
    
    func addFoodAreaButton() {
        guard let positionList = predictResult?.foodPositionList else {
            return
        }
        
        foodImageView.subviews.forEach({ elem in
            if let button = elem as? UIButton {
                button.removeFromSuperview()
            }
        })
        
        positionList.enumerated().forEach { (offset, foodPosition) in
            if foodPosition.isDelete == true {
                return
            }
            
            if let foodName = foodPosition.userSelectedFood?.foodName,
                let imagePosition = foodPosition.imagePosition,
                let position = transformPosition(box: imagePosition, imageSize: foodImageSize) {
                let areaButton = ImageAreaButton(frame: position)
                
                areaButton.setData(title: foodName, backgroundColor: self.backgroundColorList[offset % self.backgroundColorList.count], position: position, tag: offset)
                foodImageView.addSubview(areaButton)
            }
        }
    }
    
    func transformPosition(box : Box? , imageSize : CGSize?) -> CGRect? {
        guard let orgSize = imageSize, let box = box else {
            return nil
        }
        
        //let imageSize : CGSize = CGSize(width: imageWidth, height: imageHeight)
        
        let xRatio = foodImageView.frame.width / orgSize.width
        let yRatio = foodImageView.frame.height / orgSize.height
        let xMin = box.xmin
        let xMax = box.xmax
        let yMin = box.ymin
        let yMax = box.ymax
        
        let xPos = CGFloat(xMin) * xRatio
        let yPos = CGFloat(yMin) * yRatio
        let width = CGFloat(xMax - xMin) * xRatio
        let height = CGFloat(yMax - yMin) * yRatio

        if width < 1 || height < 1 {
            return nil
        }
        
        return CGRect(x: xPos, y: yPos, width: width, height: height)
    }

    
    @IBAction func didDatePressed(_ sender: Any) {
        guard let predictResult = predictResult else {
            return
        }
        
        let popup : DateSelectPopup = UINib(nibName: "DateSelectPopup", bundle: bundle).instantiate(withOwner: self, options: nil)[0] as! DateSelectPopup

        popup.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        popup.datePicker.backgroundColor = UIColor.white
        popup.baseView.layer.cornerRadius = 9.0
        popup.datePicker.datePickerMode = .date
        popup.datePicker.maximumDate = Date()
        if #available(iOS 14.0, *) {
            popup.datePicker.preferredDatePickerStyle = .inline
        }
        
        popup.datePicker.setDate(predictResult.eatDate, animated: false)
        popup.okButton.addTarget(self, action: #selector(dateSelectOK), for: .touchUpInside)
        dateSelectPopup = popup
        
        if let parentVC = self.parent {
            popup.frame = parentVC.view.frame
            parentVC.view.addSubview(popup)
        } else {
            popup.frame = self.view.frame
            self.view.addSubview(popup)
        }
    }
    
    @objc func dateSelectOK() {
        if let popup : DateSelectPopup = dateSelectPopup {
            //RealmPredictHelper.updateDate(date: popup.datePicker.date)
            UIView.setAnimationsEnabled(false)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat="yyyy.MM.dd"
            dateButton.setTitle(dateFormatter.string(from: popup.datePicker.date), for: .normal)
            predictResult?.eatDate = popup.datePicker.date
            self.dateButton.layoutIfNeeded()
            UIView.setAnimationsEnabled(true)
            popup.removeFromSuperview()
        }
    }
    
    @IBAction func timeButtonClicked(_ sender: Any) {
        guard let predictResult = predictResult else { return }
        
        let popup : TimeSelectPopup = UINib(nibName: "TimeSelectPopup", bundle: bundle).instantiate(withOwner: self, options: nil)[0] as! TimeSelectPopup
        popup.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        popup.timePicker.backgroundColor = UIColor.white
        popup.baseView.layer.cornerRadius = 9.0
        popup.timePicker.datePickerMode = .time
        if #available(iOS 14.0, *) {
            popup.timePicker.preferredDatePickerStyle = .wheels
        }

        popup.timePicker.setDate(predictResult.eatDate ?? Date(), animated: false)
        popup.okButton.addTarget(self, action: #selector(timeSelectOK), for: .touchUpInside)
        timeSelectPopup = popup
        
        if let parentVC = self.parent {
            popup.frame = parentVC.view.frame
            parentVC.view.addSubview(popup)
        } else {
            popup.frame = self.view.frame
            self.view.addSubview(popup)
        }
    }
    
    @objc func timeSelectOK() {
        if let popup : TimeSelectPopup = timeSelectPopup {
            guard let predictResult = predictResult else {
                self.timeButton.layoutIfNeeded()
                UIView.setAnimationsEnabled(true)
                popup.removeFromSuperview()
                return
            }
            
            UIView.setAnimationsEnabled(false)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat="a hh:mm"
            timeButton.setTitle(dateFormatter.string(from: popup.timePicker.date), for: .normal)
            predictResult.eatDate = popup.timePicker.date
//            SystemInfo.setChangedFoodDate(date: predictResult.eatDate ?? Date())
            self.timeButton.layoutIfNeeded()
            UIView.setAnimationsEnabled(true)
            popup.removeFromSuperview()
        }
    }
    
    @IBAction func didMealTypePressed(_ sender: Any) {
        guard let predictResult = predictResult else {
            return
        }
                    
        let popup : MealTypeSelectPopup = UINib(nibName: "MealTypeSelectPopup", bundle: bundle).instantiate(withOwner: self, options: nil)[0] as! MealTypeSelectPopup
        popup.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        popup.baseView.layer.cornerRadius = 10.0
        let mealType = CameraViewController.predictResult.mealType ?? MealType.breakfast
        
        popup.selectedMealType = mealType
        mealTypeSelectPopup = popup
        
        if let parentVC = self.parent {
            popup.frame = parentVC.view.frame
            parentVC.view.addSubview(popup)
        } else {
            popup.frame = self.view.frame
            self.view.addSubview(popup)
        }
    }
    
    @objc func breakfastButtonClick() {
        //RealmPredictHelper.updateMealType(mealType: .breakfast)
        UIView.setAnimationsEnabled(false)
        self.mealTypeButton.setTitle(NSLocalizedString("breakfast", tableName: nil, bundle: bundle, value: "", comment: ""), for: .normal)
        self.mealTypeButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        //baseInfo!.mealType = MealType.breakfast.rawValue
        predictResult?.mealType = .breakfast
        
        if let popup = mealTypeSelectPopup {
            popup.removeAnimate()
        }
    }
    
    @objc func lunchButtonClick() {
        //RealmPredictHelper.updateMealType(mealType: .lunch)
        UIView.setAnimationsEnabled(false)
        self.mealTypeButton.setTitle(NSLocalizedString("lunch", tableName: nil, bundle: bundle, value: "", comment: ""), for: .normal)
        self.mealTypeButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        predictResult?.mealType = .lunch
        if let popup = mealTypeSelectPopup {
            popup.removeAnimate()
        }
    }
    
    @objc func dinnerButtonClick() {
        //RealmPredictHelper.updateMealType(mealType: .dinner)
        UIView.setAnimationsEnabled(false)
        self.mealTypeButton.setTitle(NSLocalizedString("dinner", tableName: nil, bundle: bundle, value: "", comment: ""), for: .normal)
        self.mealTypeButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        predictResult?.mealType = .dinner
        if let popup = mealTypeSelectPopup {
            popup.removeAnimate()
        }
    }
    
    @objc func snackButtonClick() {
        UIView.setAnimationsEnabled(false)
        self.mealTypeButton.setTitle(NSLocalizedString("snack", tableName: nil, bundle: bundle, value: "", comment: ""), for: .normal)
        self.mealTypeButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        //baseInfo!.mealType = MealType.snack.rawValue
        predictResult?.mealType = .snack
        if let popup = mealTypeSelectPopup {
            popup.removeAnimate()
        }
    }
    
    @IBAction func didAddFoodPressed(_ sender: Any) {
        self.navigationController?.navigationBar.isTranslucent = true
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        guard let foodSearchViewController = storyboard.instantiateViewController(withIdentifier: "FoodSearchViewController") as? FoodSearchViewController else { return }
        guard let candidateList: [Food] = predictResult?.foodPositionList[selectedIndex].foodCandidates else {
            return
        }
        foodSearchViewController.candidateList = candidateList
        foodSearchViewController.addProtocol = self
        foodSearchViewController.searchType = .addFood
        self.navigationController?.pushViewController(foodSearchViewController, animated: true)
        
    }
    
    /*func addFood(foodPosition : FoodPosition, keyType: ChangeKeyType) {
        if let selectedCell = foodCollectionView.cellForItem(at: IndexPath(row: selectedIndex, section: 0)) as? FoodAmountNameCell {
            selectedCell.set(selected: false)
        }
        
        CameraViewController.predictResult.foodPositionList.append(foodPosition)
        let foodList = CameraViewController.predictResult.foodPositionList
        selectedIndex = foodList.count - 1
        foodCollectionView.reloadData()
        
        _ = calculateCalories()
        inputAmountView?.changeFoodPosition(index: selectedIndex)
        
        //if let popup = foodSearchPopup {
        //    popup.removeFromSuperview()
        //    foodSearchPopup = nil
        //}
    }*/
    
    func addFood(foodInfo: FoodInfo, searchedFoodName : String, keyType: ChangeKeyType) {
        var position = FoodPosition()
        var food = Food()
        food.foodId = foodInfo.id
        food.foodName = searchedFoodName
        food.manufacturer = ""
        food.source = keyType
        food.nutrition = Nutrition(foodInfo: foodInfo)
        position.foodCandidates.append(food)
        position.userSelectedFood = position.foodCandidates[0]
        
        //position. .originName = foodInfo.foodname
//        position.type = "db_add"
//        position.changedKey = 1
        
        if let selectedCell = foodCollectionView.cellForItem(at: IndexPath(row: selectedIndex, section: 0)) as? FoodAmountNameCell {
            selectedCell.set(selected: false)
        }
        
        if var insertIndex = predictResult?.foodPositionList.count {
            //CameraViewController.predictResult.foodPositionList.insert(position, at: insertIndex)
            predictResult?.foodPositionList.insert(position, at: insertIndex)
            insertIndex = getUndeletedFoodList().last?.0 ?? 0
            selectedIndex = insertIndex
            foodCollectionView.reloadData()
            //foodCollectionView.selectItem(at: IndexPath(row: insertIndex, section: 0), animated: true, scrollPosition: .right)
            
            _ = calculateCalories()
            inputAmountView?.changeFoodPosition(index: selectedIndex)
            
            //reloadDatas()
            
            //if let popup = foodSearchPopup {
            //    popup.removeFromSuperview()
            //    foodSearchPopup = nil
            //}
        }
    }
    
    func getUndeletedFoodList() -> [(Int, FoodPosition)]{
        return CameraViewController.predictResult.foodPositionList
            .enumerated()
            .map{ ($0, $1) }   // [(0, FoodPosition), (1, FoodPosition), (2, FoodPosition) ... ]
            .filter { $1.isDelete == false }    // delete if isDelete is true
    }
    
            
    @IBAction func foodSearchButtonClicked(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        guard let foodSearchViewController = storyboard.instantiateViewController(withIdentifier: "FoodSearchViewController") as? FoodSearchViewController else { return }
        guard let candidateList: [Food] = predictResult?.foodPositionList[selectedIndex].foodCandidates else {
            return
        }
        foodSearchViewController.candidateList = candidateList
        foodSearchViewController.changeProtocol = self
        foodSearchViewController.searchType = .changeFood
        self.navigationController?.pushViewController(foodSearchViewController, animated: true)

    }
    
    func changeFood(food : Food, keyType: ChangeKeyType) {
        guard var selectedFoodPosition = predictResult?.foodPositionList[selectedIndex] else {
            return
        }
        
        if predictResult?.foodPositionList[selectedIndex].foodCandidates.first(where: {
            return $0.foodId == food.foodId && $0.foodName == food.foodName
        }) == nil {
            predictResult?.foodPositionList[selectedIndex].foodCandidates.append(food)
        }
        var nutritionFood: Food = food
        
        if let nutrition = food.nutrition {
            nutritionFood.source = keyType
            predictResult?.foodPositionList[selectedIndex].userSelectedFood = nutritionFood
            setSeeOthersView()
            inputAmountView?.changeFoodPosition(index: selectedIndex)
            
            foodCollectionView.reloadData()
            _ = calculateCalories()
            addFoodAreaButton()
        }
        else {
            let networkService = FoodLens.createNetworkService(accessToken: nil)
            networkService.getNutritionInfo(foodId: food.foodId) { [weak self] (nutrition, status) in
                guard
                    let `self` = self,
                    let nutrition = nutrition else {
                    return
                }
                
                nutritionFood.source = keyType
                nutritionFood.nutrition = nutrition
                self.predictResult?.foodPositionList[self.selectedIndex].userSelectedFood = nutritionFood
                self.setSeeOthersView()
                self.inputAmountView?.changeFoodPosition(index: self.selectedIndex)
                
                self.foodCollectionView.reloadData()
                _ = self.calculateCalories()
                self.addFoodAreaButton()
            }
        }
        
        
        //if let popup = foodSearchPopup {
        //    popup.removeFromSuperview()
        //    foodSearchPopup = nil
        //}
    }
    //InputView
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "InputView" {
            if let target = segue.destination as? AmountInputViewController {
                self.inputAmountView = target
                target.superViewController = self
            }
        }
    }
    
    
}

