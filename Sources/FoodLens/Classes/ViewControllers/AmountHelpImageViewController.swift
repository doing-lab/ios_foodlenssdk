//
//  AmountHelpImageViewController.swift
//  DietCameraAI
//
//  Created by HwangChun Lee on 18/06/2019.
//  Copyright © 2019 leeint. All rights reserved.
//

import UIKit

class AmountHelpImageViewController: UIViewController {
    let bundle: Bundle = foodlensResourceBundle
    
    @IBOutlet weak var imageView: UIImageView!
    var index : Int = 0
    var image : UIImage? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //light 모드로 강제진행
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        
        if let image = image {
            imageView.image = image
        }
        // Do any additional setup after loading the view.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
