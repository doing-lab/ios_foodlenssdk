//
//  CameraViewController.swift
//  Alamofire
//
//  Created by HwangChun Lee on 07/03/2019.
//

import UIKit
import UIKit
import AVFoundation
import Photos
import PhotosUI
//import CoreData
import CoreMotion

@available(iOS 10.0, *)
class CameraViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, AVCapturePhotoCaptureDelegate, MaunalInputProtocol {
    @IBOutlet weak var previewView: UIImageView!
    @IBOutlet weak var takenImageView: UIImageView!
    @IBOutlet weak var flashOptionButton: UIButton!
    
    var flashOption:AVCaptureDevice.FlashMode!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var captureButton: UIButton!
    @IBOutlet weak var galleryButton: UIButton!
    @IBOutlet weak var manualButton: UIButton!
    
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var helpImageView: UIImageView!
    
    @IBOutlet weak var galleryHelpTextEnglish: UIImageView!
    @IBOutlet weak var galleryHelpText: UIImageView!
    @IBOutlet weak var galleryHelpCircle: UIImageView!
    
    @IBOutlet weak var manualInputTextEnglish: UIImageView!
    @IBOutlet weak var manualInputText: UIImageView!
    @IBOutlet weak var manualInputCircle: UIImageView!
    
    @IBOutlet weak var arrowHelp: UIImageView!
    @IBOutlet weak var cameraHelp: UIImageView!
    @IBOutlet weak var cameraHelpEnglish: UIImageView!
    
    @IBOutlet weak var helpBackground: UIImageView!
    
    //weak var searchFoodPopup : SearchFoodPopup? = nil
    weak var foodAddPopup : FoodAddPopup? = nil
    weak var loadingPopup : PredictLoadingPopup? = nil

    static var internalUseMode : Bool?
    
    var captureSession: AVCaptureSession?
    var capturePhotoOutput: AVCapturePhotoOutput?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var nowOrientation : UIImage.Orientation = UIImage.Orientation.right
    var candidateImage:UIImage!
    var transmitImageSize : CGSize?
    var requestTime : CFAbsoluteTime?
    var motionManager: CMMotionManager?
    var selectedImage: UIImage!
    
    var isDirect: Bool = false

    let dispatchGroup : DispatchGroup = DispatchGroup()
    
    private static var _predictResult : PredictionResult? = nil
    
    static var predictResult:PredictionResult {
        get {
            return _predictResult ?? PredictionResult()
        }
        
        set {
            _predictResult = newValue
        }
    }
    
    let bundle: Bundle = foodlensResourceBundle
    
    var backgroundColorList : [UIColor] = [
        UIColor(red:0.00, green:0.63, blue:0.58, alpha:1.0), //#00a093
        UIColor(red:1.00, green:0.00, blue:0.45, alpha:1.0), //#ff0072
        UIColor(red:0.45, green:0.00, blue:1.00, alpha:1.0), //#7300ff
        UIColor(red:0.83, green:0.45, blue:0.00, alpha:1.0), //#d37200
        UIColor(red:1.00, green:0.34, blue:0.08, alpha:1.0), //#ff5615
        UIColor(red:0.86, green:0.00, blue:0.81, alpha:1.0), //#dc00cf
        UIColor(red:0.00, green:0.40, blue:1.00, alpha:1.0), //#0066ff
        UIColor(red:0.07, green:0.67, blue:0.10, alpha:1.0), //#12ac19
        UIColor(red:1.00, green:0.14, blue:0.00, alpha:1.0), //#ff2400
        UIColor(red:0.00, green:0.07, blue:1.00, alpha:1.0), //#0012ff
        UIColor(red:1.00, green:0.00, blue:0.19, alpha:1.0)  //#ff0030
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.isDirect {
            self.navigationController?.isNavigationBarHidden = true
            self.presentGallery()
            return
        }
        
        touchHelp()
        
        if FoodLens.isEnableCameraOrientation == true {
            initializeMotionManager()
        }
        
        if FoodLens.isEnableManualInput == false {
            manualButton.isHidden = true
        }
        
        cancelButton.tintColor = FoodLens.navigationBarTheme.foregroundColor
        flashButton.tintColor = FoodLens.navigationBarTheme.foregroundColor
        
        //light 모드로 강제진행
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = .white
            self.navigationController?.navigationBar.standardAppearance = appearance;
            self.navigationController?.navigationBar.scrollEdgeAppearance = self.navigationController?.navigationBar.standardAppearance
            
        }
    }
    
    func addCoverImageView() {
        let image = UIImage(named: "camera_loading", in: foodlensResourceBundle, compatibleWith : nil) ?? UIImage()
        let coverImageView = UIImageView(image: image)
        coverImageView.translatesAutoresizingMaskIntoConstraints = false
        coverImageView.backgroundColor = .white
        self.view.addSubview(coverImageView)
        
        NSLayoutConstraint.activate([
            coverImageView.topAnchor.constraint(equalTo: self.view.topAnchor),
            coverImageView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            coverImageView.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            coverImageView.rightAnchor.constraint(equalTo: self.view.rightAnchor)
        ])
        
        self.view.bringSubviewToFront(coverImageView)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        if captureSession != nil {
            captureSession?.stopRunning()
            captureSession = nil
        }

        self.takenImageView.image = nil
        bringAllButtonToFront()
        clearFoodAreaButton()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    func initializeMotionManager() {
        motionManager = CMMotionManager()
        motionManager?.accelerometerUpdateInterval = 0.2
        motionManager?.gyroUpdateInterval = 0.2
        motionManager?.startAccelerometerUpdates(to: (OperationQueue.current)!, withHandler: {
            (accelerometerData, error) -> Void in
            if error == nil {
                self.outputAccelertionData((accelerometerData?.acceleration)!)
            }
            else {
                print("\(error!)")
            }
        })
    }
    
    func outputAccelertionData(_ acceleration: CMAcceleration) {
        let angle : CGFloat;
        var orientationNew: UIInterfaceOrientation
        if acceleration.x >= 0.75 {
            angle = CGFloat(-1*Double.pi/2)
            nowOrientation = UIImage.Orientation.down
        }
        else if acceleration.x <= -0.75 {
            angle = CGFloat(Double.pi/2)
            nowOrientation = UIImage.Orientation.up
        }
        else if acceleration.y <= -0.75 {
            angle = CGFloat(0.0)
            nowOrientation = UIImage.Orientation.right
            
        }
        else if acceleration.y >= 0.75 {
            angle = CGFloat(Double.pi)
            nowOrientation = UIImage.Orientation.left
        }
        else {
            // Consider same as last time
            return
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            self.flashButton.transform = CGAffineTransform(rotationAngle: angle)
            //self.cancelButton.transform = CGAffineTransform(rotationAngle: angle)
            self.galleryButton.transform = CGAffineTransform(rotationAngle: angle)
            self.manualButton.transform = CGAffineTransform(rotationAngle: angle)
        })
    }

    
    func checkCamera() {
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authStatus {
        case .authorized: callCamera() // Do your stuff here i.e. callCameraMethod()
        case .denied:
            alertToEncourageCameraAccessInitially()
            break
        case .notDetermined:
            alertPromptToAllowCameraAccessViaSetting()
            break
        default:
            alertToEncourageCameraAccessInitially()
            break
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.isDirect {
            self.addCoverImageView()
            return
        }
        //checkCamera()
        makeGalleryButton()
        if captureSession == nil {
          checkCamera()
        }
        else {
            DispatchQueue.global().async { [weak self] in
                self?.captureSession?.startRunning()
            }
        }
        
        var option:Int? = SystemInfo.getUserFlashOption()
        if option == nil {
            option = AVCaptureDevice.FlashMode.auto.rawValue
        }
        setFlashButton(flash : AVCaptureDevice.FlashMode(rawValue: option!)!)
        bringAllButtonToFront()
        setAllButtonEnabled(isEnabled: true)
    }
    
    func setFlashButton(flash : AVCaptureDevice.FlashMode) {
        flashOption = flash;
        switch flashOption! {
        case AVCaptureDevice.FlashMode.auto:
            flashOptionButton.setImage(FoodLens.navigationBarTheme.flashAutoImage, for: .normal)
        case AVCaptureDevice.FlashMode.on:
            flashOptionButton.setImage(FoodLens.navigationBarTheme.flashOnImage, for: .normal)
        case AVCaptureDevice.FlashMode.off:
            flashOptionButton.setImage(FoodLens.navigationBarTheme.flashOffImage, for: .normal)
        default:
            flashOption = .auto
            flashOptionButton.setImage(FoodLens.navigationBarTheme.flashAutoImage, for: .normal)
        }
        
        SystemInfo.setUserFlashOption(flashOption: flashOption.rawValue)
    }
    
    func alertToEncourageCameraAccessInitially() {
        //NSLocalizedString("allowcameramsg", tableName: nil, bundle: bundle, value: "", comment: "")
        let alert:UIAlertController = UIAlertController(
            title: NSLocalizedString("important", tableName: nil, bundle: bundle, value: "", comment: ""),
            message: NSLocalizedString("allowcameramsg", tableName: nil, bundle: bundle, value: "", comment: ""),
            preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(UIAlertAction(title: NSLocalizedString("cancel", tableName: nil, bundle: bundle, value: "", comment: ""), style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: NSLocalizedString("allowcamera", tableName: nil, bundle: bundle, value: "", comment: ""), style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: [:], completionHandler: nil)
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func alertPromptToAllowCameraAccessViaSetting() {
        if AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .back) != nil {
            AVCaptureDevice.requestAccess(for: AVMediaType.video) { granted in
                DispatchQueue.main.async() {
                    self.checkCamera()
                    self.makeGalleryButton()
                }
            }
        }
    }
    
    func callCamera() {
        let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice!)
            captureSession = AVCaptureSession()
            captureSession?.addInput(input)
            
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
            videoPreviewLayer!.videoGravity = AVLayerVideoGravity.resizeAspectFill
            let width = UIScreen.main.bounds.width
            let height = previewView.frame.height
            videoPreviewLayer!.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: width, height: height)
            previewView.layer.addSublayer(videoPreviewLayer!)
            
            capturePhotoOutput = AVCapturePhotoOutput()
            capturePhotoOutput?.isHighResolutionCaptureEnabled = true
            // Set the output on the capture session
            captureSession?.addOutput(capturePhotoOutput!)
            DispatchQueue.global().async { [weak self] in
                self?.captureSession?.startRunning()
            }
        } catch {
            print(error)
        }
    }
    
    func makeGalleryButton() {
        
        if FoodLens.isEnablePhtoGallery {
            
            self.galleryButton.layer.borderWidth = CGFloat(1.0)
            self.galleryButton.layer.borderColor = UIColor.clear.cgColor
            self.galleryButton.layer.cornerRadius = self.galleryButton.frame.size.width / 2
            self.galleryButton.clipsToBounds = true
            
            let fetchOptions = PHFetchOptions()
            let manager = PHImageManager.default()
            let targetSize = CGSize(width: 30, height: 30)
            fetchOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
            let assetList = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions)
            
            if let image = assetList.lastObject {
                let requestOptions = PHImageRequestOptions()
                requestOptions.isSynchronous = false
                requestOptions.deliveryMode = .highQualityFormat
                manager.requestImage(for: image, targetSize: targetSize, contentMode: .aspectFill, options: requestOptions) { (uiImage, info) in
                    self.galleryButton.setImage(uiImage, for: UIControl.State.normal)
                }
            }
        } else {
            self.galleryButton.isHidden = true
            self.galleryButton.isEnabled = false
        }
        
    }
    
    func bringAllButtonToFront() {
        view.bringSubviewToFront(previewView)
        view.bringSubviewToFront(cancelButton)
        view.bringSubviewToFront(captureButton)
        view.bringSubviewToFront(galleryButton)
        view.bringSubviewToFront(manualButton)
        view.bringSubviewToFront(flashButton)
    }
    
    func clearFoodAreaButton() {
        takenImageView.subviews.forEach({ elem in
            if let button = elem as? UIButton {
                button.removeFromSuperview()
            }
        })
    }
    
    func touchHelp() {
        galleryHelpCircle.isHidden = true
        manualInputCircle.isHidden = true
        arrowHelp.isHidden = true
        
        cameraHelpEnglish.isHidden = true
        galleryHelpTextEnglish.isHidden = true
        manualInputTextEnglish.isHidden = true
        
        galleryHelpText.isHidden = true
        manualInputText.isHidden = true
        cameraHelp.isHidden = true
        
        helpBackground.isHidden = true
    }
    
    @IBAction func didCancelPressed(_ sender: Any) {
        dismiss(animated: true) {
            if let callback = UIService.getCompletion() {
                callback.onCancel()
            }
        }
    }
    
    @IBAction func loadFromGallery(_ sender: Any) {
        self.presentGallery()
    }
    
    private func presentGallery() {
        if #available(iOS 14.0, *) {
            self.presentGalleryAvaiableiOS14()
        } else {
            self.presentGalleryEarlieriOS14()
        }
    }
    
    private func presentGalleryEarlieriOS14() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        imagePicker.modalPresentationStyle = .fullScreen
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    @available(iOS 14, *)
    private func presentGalleryAvaiableiOS14() {
        DispatchQueue.main.async {
            let requiredAccessLevel: PHAccessLevel = .readWrite
            PHPhotoLibrary.requestAuthorization(for: requiredAccessLevel) { authorizationStatus in
                switch authorizationStatus {
                case .authorized,
                        .limited:
                    DispatchQueue.main.async {
                        var configuration = PHPickerConfiguration(photoLibrary: PHPhotoLibrary.shared())
                        configuration.selectionLimit = 1
                        configuration.filter = .any(of: [.images])
                        
                        let picker = PHPickerViewController(configuration: configuration)
                        picker.delegate = self
                        picker.modalPresentationStyle = .fullScreen
                        
                        self.present(picker, animated: true, completion: nil)
                    }
                default:
                    //FIXME: Implement handling for all authorizationStatus
                    print("Unimplemented")
                }
            }
        }
    }
    
    @IBAction func flashOptionPressed(_ sender: Any) {
        var option : AVCaptureDevice.FlashMode = .auto
        if flashOption == .auto {
            option = .off
        } else if flashOption == .off {
            option = .on
        } else if flashOption == .on {
            option = .auto
        }
        self.setFlashButton(flash: option)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        dismiss(animated: true, completion: nil)
        if  let pickedImage = info[.originalImage] as? UIImage,
            let orientedImage = pickedImage.removeRotationForImage(orientation: pickedImage.imageOrientation) {
            DispatchQueue.main.async { [weak self] in
                if self != nil {
                    var width = orientedImage.size.width
                    var height = orientedImage.size.height
                    
                    if width < height {
                        height = width
                    }
                    else if height < width {
                        width = height
                    }
                    
                    let posX = orientedImage.size.width / 2 - width / 2
                    let posY = orientedImage.size.height / 2 - height / 2
                    
                    let croppedImage = orientedImage.cropToBounds(posX: posX, posY: posY, width: width, height: height)
                    self?.selectedImage = croppedImage
                    
                    if FoodLens.isUseImageRecordDate {
                        guard let dateOfCaptureImage = self?.makeDateAndSelectedTimeFrom(phAsset: info) else { return }
                        self?.showChoosedDateAlertPopup(dateOfCaptureImage: dateOfCaptureImage)
                    } else {
                        self?.runAnalisysImage()
                    }
                    
                }
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true)
        if self.isDirect {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
                self.dismiss(animated: true)
            }
        }
    }
    
    func runAnalisysImage() {
        analisysImage(image: selectedImage)
    }
    
    @IBAction func takePhoto(_ sender: Any) {
        // Make sure capturePhotoOutput is valid
        guard let capturePhotoOutput = self.capturePhotoOutput else { return }
        // Get an instance of AVCapturePhotoSettings class
        let photoSettings = AVCapturePhotoSettings()
        // Set photo settings for our need
        photoSettings.isAutoStillImageStabilizationEnabled = false
        photoSettings.isHighResolutionPhotoEnabled = true
        photoSettings.flashMode = flashOption
        
        //photoSettings.accessibilityFrame = self.guideView.frame
        // Call capturePhoto method by passing our photo settings and a
        // delegate implementing AVCapturePhotoCaptureDelegate
        
        captureButton.isEnabled = false
        
        capturePhotoOutput.capturePhoto(with: photoSettings, delegate: self)
    }
    
    func photoOutput(_ captureOutput: AVCapturePhotoOutput,
                 didFinishProcessingPhotoSampleBuffer photoSampleBuffer: CMSampleBuffer?,
                 previewPhotoSampleBuffer: CMSampleBuffer?,
                 resolvedSettings: AVCaptureResolvedPhotoSettings,
                 bracketSettings: AVCaptureBracketedStillImageSettings?,
                 error: Error?) {
        guard error == nil, let photoSampleBuffer = photoSampleBuffer else {
            print("Error capturing photo: \(String(describing: error))")
            dismiss(animated: true) {
                if let callback = UIService.getCompletion() {
                    callback.onError(BaseError(message: "Camera Error \(String(describing: error))"))
                }
            }
            return
        }
        
        guard let imageData =
            AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: photoSampleBuffer, previewPhotoSampleBuffer: previewPhotoSampleBuffer) else {
                dismiss(animated: true) {
                    if let callback = UIService.getCompletion() {
                        callback.onError(BaseError(message: "Image Processing Error on AVCapturePhotoOutput.jpegPhotoDataRepresentation"))
                    }
                }
                return
        }
        
        guard let image = postCaptureProcess(imageData: imageData) else {
            dismiss(animated: true) {
                if let callback = UIService.getCompletion() {
                    callback.onError(BaseError(message: "Image Processing Error on postCaptureProcess"))
                }
            }
            return
        }
        analisysImage(image: image)
    }
    
    @available(iOS 11.0, *)
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto: AVCapturePhoto, error: Error?) {
        guard error == nil else {
            print("Error capturing photo: \(String(describing: error))")
            dismiss(animated: true) {
                if let callback = UIService.getCompletion() {
                    callback.onError(BaseError(message: "Error capturing photo: \(String(describing: error))"))
                }
            }
            return
        }
        
        guard let imageData = didFinishProcessingPhoto.fileDataRepresentation() else {
            dismiss(animated: true) {
                if let callback = UIService.getCompletion() {
                    callback.onError(BaseError(message: "Error On fileDataRepresentation"))
                }
            }
            return
        }
        
        guard let image = postCaptureProcess(imageData: imageData) else {
            dismiss(animated: true) {
                if let callback = UIService.getCompletion() {
                    callback.onError(BaseError(message: "Error On postCaptureProcess iOS ver 11.0"))
                }
            }
            return
        }
        
        analisysImage(image: image)
    }
    
    func postCaptureProcess(imageData : Data) -> UIImage? {
        guard let tempImage = UIImage.init(data: imageData , scale: 1.0),
            let previewLayer = self.videoPreviewLayer,
            let croppedImage = cropCameraImage(original: tempImage, previewLayer: previewLayer),
            let image = croppedImage.removeRotationForImage(orientation: nowOrientation) else {
            return nil
        }
        
        if captureSession != nil {
            captureSession?.stopRunning()
        }
        
        return image
    }
    
    func analisysImage(image : UIImage) {
        SystemInfo.setPredictStartTime(startTime: CFAbsoluteTimeGetCurrent())
        
        if !self.isDirect {
            takenImageView.image = image
            view.bringSubviewToFront(takenImageView)
        }
        
        showLoadingImage()
        setAllButtonEnabled(isEnabled: false)
        candidateImage = image
        
        if captureSession != nil {
            captureSession?.stopRunning()
        }
        
        requestTime = CFAbsoluteTimeGetCurrent()
        let network = FoodLens.createNetworkService(accessToken : nil)
        //FIXME temp code
        var tempImg = image
        if let _ = CameraViewController.internalUseMode {
            tempImg = image.resizeImage(targetSize: CGSize(width: NetworkService.MULTI_PREDICT_IMG_SIZE, height: NetworkService.MULTI_PREDICT_IMG_SIZE))
        }

        network.predictMultipleFood(image: tempImg, completion: predictResult)
    }
    
    func predictResult(result : PredictionResult?, status : ProcessStatus) {
        guard status.state == .success else {
            if status.reasion != .userCancel {
                let checkResult = checkMultiPredictResult(status.reasion)
                dismiss(animated: true) {
                    if let callback = UIService.getCompletion() {
                        callback.onError(checkResult.1)
                    }
                }
            }
            return
        }
        
        guard let result = result else {
            dismiss(animated: true) {
                if let callback = UIService.getCompletion() {
                    let error = BaseError(message: "Recognition Result is nil")
                    callback.onError(error)
                }
            }
            return
        }
        
        for i in 0..<result.foodPositionList.count {
            result.foodPositionList[i].userSelectedFood = result.foodPositionList[i].foodCandidates[0]
        }
        
        CameraViewController.predictResult = result
        self.closeLoadingImage(false)
        
        self.performSegue(withIdentifier: "ToAmountView", sender: nil)
    }
    
    func addFoodAreaButton(foodName : String, position : Box, imageSize : CGSize, index : Int) {
        let foodName = foodName
        let buttonPos = transformPosition(box: position, imageSize: imageSize)
        let areaButton = ImageAreaButton(frame: buttonPos)
        areaButton.setData(title: foodName, backgroundColor: self.backgroundColorList[index % self.backgroundColorList.count], position: buttonPos, tag: index)
        takenImageView.addSubview(areaButton)
    }
    
    func transformPosition(box : Box, imageSize : CGSize) -> CGRect {
        let xRatio = previewView.frame.width / imageSize.width
        let yRatio = previewView.frame.height / imageSize.height
        let xMin = box.xmin
        let xMax = box.xmax
        let yMin = box.ymin
        let yMax = box.ymax
        
        let xPos = CGFloat(xMin) * xRatio
        let yPos = CGFloat(yMin) * yRatio
        let width = CGFloat(xMax - xMin) * xRatio
        let height = CGFloat(yMax - yMin) * yRatio
        
        return CGRect(x: xPos, y: yPos, width: width, height: height)
    }
    
    func checkMultiPredictResult(_ reasion : ProcessReason) -> (Bool, BaseError) {
        switch reasion {
        case .success:
            return (true, BaseError(message: "Success"))
        case .decodingError:
            return (false, BaseError(message: "Data Decoding Error"))
        case .encodingError:
            return (false, BaseError(message: "Data Encoding Error"))
        case .imageProcessingError:
            return (false, BaseError(message: "Data Decoding Error"))
        case .invalidAccessTokenError:
            return (false, BaseError(message: "Authorization Token is invalid"))
        case .invalidServerResponseError:
            return (false, BaseError(message: "Invalid Server Response"))
        case .noPredictionInfoError:
            return (false, BaseError(message: "No Prediction Info"))
        case .quotaLimitExceedError:
            return (false, BaseError(message: "Quota exceeded"))
        case .serverNotWorkError:
            return (false, BaseError(message: "Server Not Work"))
        case .unauthorizedError:
            return (false, BaseError(message: "Unauthorized"))
        case .userCancel:
            return (false, BaseError(message: "User Cancel"))
        }
    }
    
    func cropCameraImage(original: UIImage, previewLayer: AVCaptureVideoPreviewLayer) -> UIImage? {
        var image = UIImage()
        
        let previewImageLayerBounds = previewLayer.bounds
        
        let originalWidth = original.size.width
        let originalHeight = original.size.height
        
        let A = previewImageLayerBounds.origin
        let B = CGPoint(x: previewImageLayerBounds.size.width, y: previewImageLayerBounds.origin.y)
        let D = CGPoint(x: previewImageLayerBounds.size.width, y: previewImageLayerBounds.size.height)
        
        let a = previewLayer.captureDevicePointConverted(fromLayerPoint: A)
        let b = previewLayer.captureDevicePointConverted(fromLayerPoint: B)
        let d = previewLayer.captureDevicePointConverted(fromLayerPoint: D)
        
        let posX = floor(b.x * originalHeight)
        let posY = floor(b.y * originalWidth)
        
        let width: CGFloat = d.x * originalHeight - b.x * originalHeight
        let height: CGFloat = a.y * originalWidth - b.y * originalWidth
        
        let cropRect = CGRect(x: posX, y: posY, width: width, height: height)
        
        if let imageRef = original.cgImage?.cropping(to: cropRect) {
            image = UIImage(cgImage: imageRef, scale: 2.5, orientation: .right)
        }
        
        return image
    }
    
    func getExifDate(by info: [UIImagePickerController.InfoKey : Any]) -> Date? {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd hh:mm:ss"

        guard let url = info[.imageURL] as? NSURL else { return nil }
        
        guard let imageSource = CGImageSourceCreateWithURL(url, nil) else { return nil }
        
        let imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as Dictionary?
        let exifDict = imageProperties?[kCGImagePropertyExifDictionary]
        
        guard let dateTimeOriginal = exifDict?[kCGImagePropertyExifDateTimeOriginal] as? String else {
            return Date()
  
        }
        
        return formatter.date(from: dateTimeOriginal)
        
    }

    
    
    func makeDateAndSelectedTimeFrom(phAsset: [UIImagePickerController.InfoKey : Any]) -> Date? {
        
        guard let asset = phAsset[.phAsset] as? PHAsset else {
            return getExifDate(by: phAsset)
            
        }
        return asset.creationDate
        
    }
    
    func showChoosedDateAlertPopup(dateOfCaptureImage: Date) {
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        guard let choosedDateAlertController = storyboard.instantiateViewController(withIdentifier: "ChoosedDateAlertPopup") as? ChoosedDateAlertViewController else { return }
        choosedDateAlertController.dateOfCaptureImage = dateOfCaptureImage
        print(dateOfCaptureImage)
        choosedDateAlertController.cameraView = self
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        alertController.setValue(choosedDateAlertController, forKey: "contentViewController")
//        self.present(alertController, animated: true, completion: {
//            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissAlertController))
//            alertController.view.superview?.subviews[0].addGestureRecognizer(tapGesture)
//        })
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func dismissAlertController(){
        self.dismiss(animated: true, completion: {
            self.runAnalisysImage()
        })
    }
    
    func showLoadingImage() {
        if let topController = navigationController {
            let popup : PredictLoadingPopup = UINib(nibName: "PredictLoadingPopup", bundle: bundle).instantiate(withOwner: self, options: nil)[0] as! PredictLoadingPopup
            popup.superViewController = self
            popup.frame = topController.view.frame
            loadingPopup = popup
            topController.view.addSubview(popup)
        }

    }
    
    func setAllButtonEnabled(isEnabled: Bool) {
        flashOptionButton.isEnabled = isEnabled
        cancelButton.isEnabled = isEnabled
        captureButton.isEnabled = isEnabled
        galleryButton.isEnabled = isEnabled
        manualButton.isEnabled = isEnabled
    }
    
    func closeLoadingImage(_ isCancel : Bool) {
        if let popup = loadingPopup {
            popup.loadingImageView.stopAnimating()
            popup.removeFromSuperview()
        }
        loadingPopup = nil
        
        if isCancel == true {
            clearFoodAreaButton()
            if captureSession != nil {
                DispatchQueue.global().async { [weak self] in
                    self?.captureSession?.startRunning()
                }
                captureButton.isEnabled = true
            }
            self.takenImageView.image = nil
            bringAllButtonToFront()
        }
    }
    
    @IBAction func didManualInputPressed(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        guard let foodSearchViewController = storyboard.instantiateViewController(withIdentifier: "FoodSearchViewController") as? FoodSearchViewController else { return }
        
        foodSearchViewController.manualInputProtocal = self
        foodSearchViewController.searchType = .addFood
        self.navigationController?.pushViewController(foodSearchViewController, animated: true)

    }
    
    func manualInputComplete(foodInfo : FoodInfo, searchedFoodName:String, keyType : ChangeKeyType) {
        SystemInfo.setFoodInfo(foodInfo: foodInfo)
        
        let predictResult : PredictionResult = PredictionResult()
        predictResult.setCurrentVersion()
        predictResult.eatDate = FoodLens.eatDate ?? Date()
        predictResult.setEatTypeByDate()
        //predictResult.mealType = CameraViewController.predictResult.mealType
        
        predictResult.fullImageS3Path = ""
        
        var food : Food = Food()
        food.foodName = searchedFoodName
        food.foodId = foodInfo.id
        food.manufacturer = ""
        food.source = keyType
        food.keyName = foodInfo.predict_key
        food.nutrition = Nutrition(foodInfo: foodInfo)
        
        var foodPosition : FoodPosition = FoodPosition()
        foodPosition.foodCandidates = [food]
        foodPosition.userSelectedFood = foodPosition.foodCandidates[0]
        //foodPosition.originName = foodInfo.foodname
        //foodPosition.type = "db_add"
        //foodPosition.changedKey = 1
        
        predictResult.foodPositionList = [foodPosition]
        //predictResult.predictionSource = "manual"
        
        CameraViewController.predictResult = predictResult
        
        //if let popup = self.searchFoodPopup {
        //    popup.removeFromSuperview()
        //}
        
        performSegue(withIdentifier: "ToAmountView", sender: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let amountFrameViewController = segue.destination as? AmountFrameViewController else {
            return
        }
        
        amountFrameViewController.isDirect = self.isDirect
    }
    
    func copyNutritionData(userNut: inout Nutrition, rawNut:FoodInfo) {
        userNut.calcium = rawNut.calcium
        userNut.calories = rawNut.calories
        userNut.carbonhydrate = rawNut.carbonhydrate
        userNut.cholesterol = rawNut.cholesterol
        userNut.dietrayfiber = rawNut.dietrayfiber
        userNut.fat = rawNut.fat
        userNut.protein = rawNut.protein
        userNut.saturatedfat = rawNut.saturatedfat
        userNut.sodium = rawNut.sodium
        userNut.sugar = rawNut.sugar
        userNut.totalgram = rawNut.totalgram
        userNut.transfat = rawNut.transfat
        userNut.vitamina = rawNut.vitamina
        userNut.vitaminb = rawNut.vitaminb
        userNut.vitaminc = rawNut.vitaminc
        userNut.vitamind = rawNut.vitamind
        userNut.vitamine = rawNut.vitamine
        userNut.totalgram = rawNut.totalgram
        userNut.unit = rawNut.unit
        userNut.foodtype = rawNut.foodtype
    }
}

@available(iOS 14.0, *)
extension CameraViewController: PHPickerViewControllerDelegate {
    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        picker.dismiss(animated: true, completion: nil)
        
        guard let result = results.first else {
            if self.isDirect {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
                    self.dismiss(animated: true)
                }
            }
            return
        }

        if result.itemProvider.hasItemConformingToTypeIdentifier(UTType.image.identifier) {
            result.itemProvider.loadDataRepresentation(forTypeIdentifier: UTType.image.identifier) { data, error in
                result.itemProvider.loadObject(ofClass: UIImage.self) { (selectedImage: NSItemProviderReading?, error: Error?) in
                    // 선택한 Image를 Load해 수행할 명령
                    if let pickedImage = selectedImage as? UIImage,
                       let orientedImage = pickedImage.removeRotationForImage(orientation: pickedImage.imageOrientation) {
                        DispatchQueue.main.async { [weak self] in
                            guard let `self` = self else { return }
                            var width = orientedImage.size.width
                            var height = orientedImage.size.height
                            
                            if width < height {
                                height = width
                            }
                            else if height < width {
                                width = height
                            }
                            
                            let posX = orientedImage.size.width / 2 - width / 2
                            let posY = orientedImage.size.height / 2 - height / 2
                            
                            let croppedImage = orientedImage.cropToBounds(posX: posX, posY: posY, width: width, height: height)
                            self.selectedImage = croppedImage
                            
                            if FoodLens.isUseImageRecordDate {
                                guard let assetId = result.assetIdentifier else  {
                                    return
                                }
                                let assetResults = PHAsset.fetchAssets(withLocalIdentifiers: [assetId], options: nil)
                                let dateOfCaptureImage = assetResults.firstObject?.creationDate ?? Date()
                                DispatchQueue.main.async {
                                    self.showChoosedDateAlertPopup(dateOfCaptureImage: dateOfCaptureImage)
                                }
                            } else {
                                self.runAnalisysImage()
                            }
                        }
                    }
                }
            }
        }
    }
}
