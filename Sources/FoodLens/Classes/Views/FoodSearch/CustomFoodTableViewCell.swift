//
//  CustomFoodTableViewCell.swift
//  FoodLens
//
//  Created by Eunjin on 01/04/2020.
//

import UIKit

class CustomFoodTableViewCell: UITableViewCell {
    @IBOutlet weak var foodTitleLabel: UILabel!
    @IBOutlet weak var controlButton: UIButton!
}
