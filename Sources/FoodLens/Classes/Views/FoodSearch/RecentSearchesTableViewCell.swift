//
//  RecentSearchesTableViewCell.swift
//  FoodLens
//
//  Created by Eunjin on 01/04/2020.
//

import UIKit

class RecentSearchesTableViewCell: UITableViewCell {
    @IBOutlet weak var foodNameLabel: UILabel!
    @IBOutlet weak var foodDeleteButton: UIButton!
}
