//
//  InputCustomFoodTableViewCell.swift
//  FoodLens
//
//  Created by Eunjin on 01/04/2020.
//

import UIKit

class InputCustomFoodTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var unitLabel: UILabel!
    @IBOutlet weak var underLineView: UIView!
    
    override func prepareForReuse() {
        self.amountTextField.text = nil
        self.underLineView.backgroundColor = UIColor.hexStringToUIColor(hex: "dddddd")
    }
}
