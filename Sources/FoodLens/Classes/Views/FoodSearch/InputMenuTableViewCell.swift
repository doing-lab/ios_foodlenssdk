//
//  InputMenuTableViewCell.swift
//  FoodLens
//
//  Created by Eunjin on 01/04/2020.
//

import UIKit

class InputMenuTableViewCell: UITableViewCell {
    @IBOutlet weak var inputMenuView: UIView!
    @IBOutlet weak var inputMenuTextField: UITextField!
}
