//
//  AddNutrientTableViewCell.swift
//  FoodLens
//
//  Created by Eunjin on 01/04/2020.
//

import UIKit

class AddNutrientTableViewCell: UITableViewCell {
    @IBOutlet weak var addNutrientButton: DesignableButton!
}
