//
//  RecentTitleTableViewCell.swift
//  FoodLens
//
//  Created by Eunjin on 01/04/2020.
//

import UIKit

class RecentTitleTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
}
