//
//  RealTimeSearchesTableViewCell.swift
//  FoodLens
//
//  Created by Eunjin on 01/04/2020.
//

import UIKit

class RealTimeSearchesTableViewCell: UITableViewCell {
    @IBOutlet weak var foodNameLabel: UILabel!
        
    override func prepareForReuse() {
        foodNameLabel.text = ""
    }
    
    func makeEmphaSizeString(of inputText: String) {
        guard let foodName = foodNameLabel.text else { return }
        var htmlText = ""
        var isfirst = true
        for item in foodName {
            if String(item) == inputText, isfirst {
                htmlText = htmlText + "<font color = \"#095d6a\">\(item)</font>"
                isfirst = false
            }else {
                htmlText = htmlText + "<font color = \"black\">\(item)</font>"
            }
        }
        foodNameLabel.attributedText = htmlText.htmlToAttributed
    }
}
