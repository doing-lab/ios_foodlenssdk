//
//  SystemInfo.swift
//  FoodLens
//
//  Created by HwangChun Lee on 14/03/2019.
//

import UIKit



class SystemInfo {
    private static let PID_USER_CAMEA_FLASH_OPTION:String = "pid_user_camera_flash_option"
    private static let PID_USER_FOOD_IMAGE_STORE:String = "pid_user_food_imag_store"
    private static let PID_PREDICT_START_TIME:String = "pid_predict_start_time"
    private static let PID_PREDICT_BASE_INFO:String = "pid_predict_base_info"
    private static let PID_PREDICT_POSITION:String = "pid_predict_position"
    private static let PID_PREDICT_CANDIDATE:String = "pid_predict_candidate"
    private static let PID_NOW_SELECTED_POSITION:String = "pid_now_selected_position"
    private static let PID_FOODINFO:String = "pid_foodinfo"
    private static let PID_RECENT_KEYWORD:String = "pid_recent_keyword"
    private static let PID_USER_TARGET_CALORIE:String = "pid_user_target_calorie"
    private static let PID_USER_UUID:String = "pid_user_uuid"
    
    private static var isFoodChange:Bool = false
    private static var premiumType:Int = 0
    
    public static func getUserFlashOption() -> Int? {
        let defaults = UserDefaults.standard
        let value:Int? = defaults.integer(forKey: SystemInfo.PID_USER_CAMEA_FLASH_OPTION)
        return value
    }
    
    public static func setUserFlashOption(flashOption:Int) {
        let defaults = UserDefaults.standard
        defaults.set(flashOption, forKey: SystemInfo.PID_USER_CAMEA_FLASH_OPTION)
    }
    
    public static func setFoodImageStore(option:Bool) {
        let defaults = UserDefaults.standard
        defaults.set(option, forKey: SystemInfo.PID_USER_FOOD_IMAGE_STORE)
    }
    
    public static func getFoodImageStore() -> Bool {
        let defaults = UserDefaults.standard
        let value:Bool? = defaults.bool(forKey: SystemInfo.PID_USER_FOOD_IMAGE_STORE)
        if value == nil {
            return false
        }
        return value!
    }
    
    public static func setPredictStartTime(startTime : CFAbsoluteTime) {
        let defaults = UserDefaults.standard
        defaults.set(startTime, forKey: PID_PREDICT_START_TIME)
    }
    
    public static func getPredictStartTime() -> CFAbsoluteTime? {
        let defaults = UserDefaults.standard
        return defaults.double(forKey: PID_PREDICT_START_TIME)
    }
    
    public static func setUserPreminum(isPremium:Int) {
        premiumType = isPremium
    }
    
    public static func getUserPremiumType() -> Int {
        return premiumType
    }
    
    public static func isUserPremium() -> Bool {
        #if DEBUG
        return true
        #else
        if premiumType == 0 {
            return false
        }
        return true
        #endif
    }
    
    public static func foodChanged(isChanged:Bool) {
        isFoodChange = isChanged
    }
    
    public static func isFoodChanged() -> Bool {
        return isFoodChange
    }
    
    public static func setRecentKeyword(keywordList : [FoodSearchList]) {
        let defaults = UserDefaults.standard
        do {
            let jsonData: Data = try JSONEncoder().encode(keywordList)
            defaults.set(String(data: jsonData, encoding: .utf8), forKey: PID_RECENT_KEYWORD)
        } catch {
            
        }
    }
    
    public static func getRecentKeyword() -> [FoodSearchList]? {
        let defaults = UserDefaults.standard
        if let value = defaults.string(forKey: PID_RECENT_KEYWORD) {
            
            let foodSearchList: [FoodSearchList]? = try? JSONDecoder().decode([FoodSearchList].self, from: value.data)
            return foodSearchList
        } else {
            return nil
        }
    }
    
    public static func setNowSelectedPosition(position : LocalDataPredictPosition) {
        let defaults = UserDefaults.standard
        let jsonString : String = position.toJsonString() ?? ""
        defaults.set(jsonString, forKey: PID_NOW_SELECTED_POSITION)
    }
    
    public static func getNowSelectedPosition() -> LocalDataPredictPosition? {
        let defaults = UserDefaults.standard
        let value = defaults.string(forKey: PID_NOW_SELECTED_POSITION) ?? ""
        return LocalDataPredictPosition(json: value)
    }
    
    public static func setUserTargetCalorie(colorie:Float) {
        let defaults = UserDefaults.standard
        defaults.set(colorie, forKey: SystemInfo.PID_USER_TARGET_CALORIE)
    }
    
    public static func getUserTargetCalorie() -> Float? {
        let defaults = UserDefaults.standard
        let value:Float? = defaults.float(forKey: SystemInfo.PID_USER_TARGET_CALORIE)
        return value
    }
    
    public static func getUserUUID() -> String? {
        let defaults = UserDefaults.standard
        let uuid = defaults.string(forKey: SystemInfo.PID_USER_UUID)
        return uuid
    }
    
    public static func setUserUUID(uuid : String) {
        let defaults = UserDefaults.standard
        defaults.set(uuid, forKey: SystemInfo.PID_USER_UUID)
    }
    
    public static func getFoodInfo(id : Int) -> FoodInfo? {
        let defaults = UserDefaults.standard
        let dict = defaults.dictionary(forKey: PID_FOODINFO)
        if let value = dict?[String(id)] as? String {
            return FoodInfo.genreated(json: value)
        }
        else {
            return nil
        }
    }
    
    public static func setFoodInfo(foodInfo : FoodInfo) {
        let defaults = UserDefaults.standard
        let jsonString = foodInfo.toJsonString()
        var dict = defaults.dictionary(forKey: PID_FOODINFO)
        
        if dict != nil {
            dict![String(foodInfo.id)] = jsonString
            defaults.set(dict, forKey: PID_FOODINFO)
        }
        else {
            var newDict:[String : Any] = [String : Any]()
            newDict[String(foodInfo.id)] = jsonString
            defaults.set(newDict, forKey: PID_FOODINFO)
        }
    }
}
