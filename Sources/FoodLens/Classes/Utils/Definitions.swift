//
//  Definitions.swift
//  FoodLens
//
//  Created by HwangChun Lee on 15/03/2019.
//

import UIKit

/**
 Enums that indicates types of meal
 */
public enum MealType: String, Codable {
    case breakfast
    case lunch
    case dinner
    case snack
    case morning_snack
    case afternoon_snack
    case late_night_snack
    
    static let order = ["breakfast", "lunch", "dinner", "snack", "morning snack", "afternoon snack", "late night snack"]
    
    static func getMealType(by index: Int) -> MealType {
        switch index {
        case 0: return MealType.breakfast
        case 1: return MealType.lunch
        case 2: return MealType.dinner
        case 3: return MealType.snack
        case 4: return MealType.morning_snack
        case 5: return MealType.afternoon_snack
        case 6: return MealType.late_night_snack
        default:
            return MealType.breakfast
        }
    }
}

enum WeightUnit:Int {
    case kg = 0
    case lb
}

enum AnalysisPeriodUnit:Int {
    case day = 0
    case week
    case month
    case hour
    case dayOfWeek
    case year
}
