//
//  ColorTable.swift
//  FoodLens
//
//  Created by HwangChun Lee on 15/03/2019.
//

import UIKit

class ColorTable {
    public static let mainRed:UIColor = UIColor(red: 205.0/255.0, green: 43.0/255.0, blue: 46.0/255.0, alpha: 1)
    public static let mainNavigationRed:UIColor = UIColor(red: 221/255.0, green: 19/255.0, blue: 19/255.0, alpha: 1)
    public static let mainBlue:UIColor = UIColor(red: 0.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    public static let mainWhite:UIColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    public static let mainGrayBG:UIColor = UIColor(red: 246.0/255.0, green: 246.0/255.0, blue: 246.0/255.0, alpha: 1.0)
    public static let mainGrayLine:UIColor = UIColor(red: 200.0/255.0, green: 199.0/255.0, blue: 204.0/255.0, alpha: 1.0)
    public static let mainGray:UIColor = UIColor(red: 75.0/255.0, green: 75.0/255.0, blue: 75.0/255.0, alpha: 1.0)
    public static let mainBlack:UIColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1)
    public static let analysisSkyBlue:UIColor = UIColor(red: 99.0/255.0, green: 202.0/255.0, blue: 250.0/255.0, alpha: 1)
    public static let analysisBlue:UIColor = UIColor(red: 0.0/255.0, green: 125.0/255.0, blue: 255.0/255.0, alpha: 1)
    public static let analysisLightBlue:UIColor = UIColor(red: 21.0/255.0, green: 126.0/255.0, blue: 256.0/255.0, alpha: 0.7)
    public static let analysisRed:UIColor = UIColor(red: 253.0/255.0, green: 112.0/255.0, blue: 113.0/255.0, alpha: 1)
    public static let mainLightGray:UIColor = UIColor(red: 240/255.0, green: 240/255.0, blue: 240/255.0, alpha: 1)
}
