//
//  Util.swift
//  DietDiaryAI
//
//  Created by leeint on 2017. 10. 18..
//  Copyright © 2017년 leeint. All rights reserved.
//

import UIKit

class Util {
    static func getFileNameFromURL(url:URL) -> String {
        let firstpart = url.path.components(separatedBy: "?")
        let range = (firstpart[0].index(after: (firstpart[0].startIndex)))..<(firstpart[0].endIndex)
        let key = firstpart[0][range]
        //print(key)
        return String(key)
    }
    
    static func getDocumentsPath() -> URL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
    
    static func save(image: UIImage, path:String?, fileName:String) -> String? {
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        var targetUrl:URL! = documentsUrl
        if path != nil {
            targetUrl = documentsUrl.appendingPathComponent(path!)
        }
        
        let fileURL = targetUrl.appendingPathComponent(fileName)

        
        if let imageData = image.jpegData(compressionQuality: 1.0) {
            do {
                try FileManager.default.createDirectory(atPath: targetUrl.path, withIntermediateDirectories: true, attributes: nil)
                try imageData.write(to: fileURL, options: .atomic)
                return fileURL.absoluteString // ----> Save fileName
            }
            catch {
                print("Error saving image : \(error)")
            }
        }
        //print("Error saving image")
        return nil
    }
    
    static func load(path:String?, fileName:String) -> UIImage? {
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        var targetUrl:URL! = documentsUrl
        if path != nil {
            targetUrl = documentsUrl.appendingPathComponent(path!)
        }
        
        let fileURL = targetUrl.appendingPathComponent(fileName)
        do {
            let imageData = try Data(contentsOf: fileURL)
            return UIImage(data: imageData)
        } catch {
            print("Error loading image : \(error)")
        }
        return nil
    }
    
    static func deleteDirectory(path : String?) {
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        var dataPath:URL
        if path != nil {
            dataPath = documentsUrl.appendingPathComponent(path!)
        } else {
            dataPath = documentsUrl
        }
        
        do {
            try FileManager.default.removeItem(atPath: dataPath.path)
        } catch {
            print("Could not clear temp folder: \(error)")
        }
    }
    
    static func deleteFiles(path:String?) {
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        var dataPath:URL
        if path != nil {
            dataPath = documentsUrl.appendingPathComponent(path!)
        } else {
            dataPath = documentsUrl
        }
        
        if let urlArray = try? FileManager.default.contentsOfDirectory(at: dataPath,
                                                                   includingPropertiesForKeys: [.contentModificationDateKey],
                                                                   options:.skipsHiddenFiles) {
            for fileName in urlArray {
                do {
                    try FileManager.default.removeItem(atPath: fileName.path)
                } catch {
                    print("Could not clear temp folder: \(error)")
                }
            }
        }
    }
    
    static func deleteFile(path:String?, fileName:String) {
        
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        var dataPath:URL
        if path != nil {
            dataPath = documentsUrl.appendingPathComponent(path!)
        } else {
            dataPath = documentsUrl
        }

        let filePath:String = String(format:"%@/%@", dataPath.path, fileName)
        do {
            try FileManager.default.removeItem(atPath: filePath)
        } catch {
            print("Could not clear temp folder: \(error)")
        }
        
    
    }
    static func getFileList() -> [String]? {
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let dataPath = documentsUrl.appendingPathComponent("after")
        
        if let urlArray = try? FileManager.default.contentsOfDirectory(at: dataPath,
                                                                       includingPropertiesForKeys: [.contentModificationDateKey],
                                                                       options:.skipsHiddenFiles) {
            
            let result = urlArray.map { url in
                (url.lastPathComponent, (try? url.resourceValues(forKeys: [.contentModificationDateKey]))?.contentModificationDate ?? Date.distantPast)
                }
                .sorted(by: { $0.1 > $1.1 }) // sort descending modification dates
                .map { $0.0 } // extract file names
 
            return result
        } else {
            return nil
        }
    }
    
//    static func storeImageFileWithFilename(filename : String, image : UIImage) {
//        ImageCache.default.store(image, forKey: filename)
//    }
//    
//    static func removeImageFileWithFilename(filename : String) {
//        ImageCache.default.removeImage(forKey: filename)
//    }
    
    static func parseStandardUnit(standardUnit : String) -> [(String, String)] {
        let amountList = standardUnit.getArrayAfterRegex(regex: "([0-9]+/[0-9]+|[0-9]+)")
        let unitList = standardUnit.getArrayAfterRegex(regex: "[A-Za-z가-힣]+")
        
        let maxCount = max(amountList.count, unitList.count)
        
        var result : [(String, String)] = []
        for i in 0 ..< maxCount {
            var temp : (String, String) = ("", "")
            if i < amountList.count {
                temp.0 = amountList[i]
            }
            
            if i < unitList.count {
                temp.1 = unitList[i]
            }
            result.append(temp)
        }
        return result
    }
    
}
