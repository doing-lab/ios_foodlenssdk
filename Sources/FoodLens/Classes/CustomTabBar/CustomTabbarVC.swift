//
//  MainVC.swift
//  CustomTabBar
//
//  Created by 이동건 on 2018. 4. 18..
//  Copyright © 2018년 이동건. All rights reserved.
//

import UIKit

class CustomTabbarVC: UIViewController, CustomMenuBarDelegate {
    
    //MARK: Outltes
    var pageCollectionView: UICollectionView = {
        let collectionViewLayout = UICollectionViewFlowLayout()
        collectionViewLayout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: 0, height: 0), collectionViewLayout: collectionViewLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        return collectionView
    }()
    
    var customMenuBar = CustomMenuBar(titles: ["A", "B"])
    var viewControllers = [UIViewController]()
    
    var selectedIndexHandler: ((Int) -> (Void)) = { _ in }
    
    private var itemCount: Int {
        max(1, self.viewControllers.count)
    }
    
    convenience init(tabbarTitles: [String], viewControllers: [UIViewController]) {
        self.init()

        self.customMenuBar = .init(titles: tabbarTitles)
        self.viewControllers = viewControllers
    }
    
    init() {    
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    //MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        setupCustomTabBar()
        setupPageCollectionView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    //MARK: Setup view
    func setupCustomTabBar(){
        self.view.addSubview(customMenuBar)
        customMenuBar.delegate = self
        customMenuBar.translatesAutoresizingMaskIntoConstraints = false
        if customMenuBar.isFillWidth {
            customMenuBar.indicatorViewWidthConstraint.constant = self.view.frame.width / CGFloat(self.itemCount)
        } else {
            customMenuBar.indicatorViewWidthConstraint.constant = self.view.frame.width / 4
        }
        customMenuBar.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        customMenuBar.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        customMenuBar.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        customMenuBar.heightAnchor.constraint(equalToConstant: self.customMenuBar.height).isActive = true
    }
    
    func customMenuBar(scrollTo index: Int) {
        let indexPath = IndexPath(row: index, section: 0)
        self.pageCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        self.selectedIndexHandler(index)
    }
    
    func setupPageCollectionView(){
        let bundle: Bundle = foodlensResourceBundle
        pageCollectionView.delegate = self
        pageCollectionView.dataSource = self
        pageCollectionView.backgroundColor = .white
        pageCollectionView.showsHorizontalScrollIndicator = false
        pageCollectionView.isPagingEnabled = true
        pageCollectionView.register(UINib(nibName: PageCell.reusableIdentifier, bundle: bundle), forCellWithReuseIdentifier: PageCell.reusableIdentifier)
        self.view.addSubview(pageCollectionView)
        pageCollectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        pageCollectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        pageCollectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        pageCollectionView.topAnchor.constraint(equalTo: self.customMenuBar.bottomAnchor).isActive = true
    }
}
//MARK:- UICollectionViewDelegate, UICollectionViewDataSource
extension CustomTabbarVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PageCell.reusableIdentifier, for: indexPath) as! PageCell
        cell.viewController = self.viewControllers[indexPath.row]
        cell.parentVC = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewControllers.count
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if customMenuBar.isFillWidth {
            customMenuBar.indicatorViewLeadingConstraint.constant = (scrollView.contentOffset.x / CGFloat(self.itemCount)) + (customMenuBar.leftMargin / CGFloat(self.itemCount))
        } else {
            customMenuBar.indicatorViewLeadingConstraint.constant = (scrollView.contentOffset.x / 4) + (customMenuBar.leftMargin / 4)
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let itemAt = Int(targetContentOffset.pointee.x / self.view.frame.width)
        let indexPath = IndexPath(item: itemAt, section: 0)
        customMenuBar.customTabBarCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
        self.selectedIndexHandler(indexPath.row)
    }
}
//MARK:- UICollectionViewDelegateFlowLayout
extension CustomTabbarVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: pageCollectionView.frame.width, height: pageCollectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
