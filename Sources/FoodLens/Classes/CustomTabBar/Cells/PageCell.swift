//
//  PageCell.swift
//  CustomTabBar
//
//  Created by 이동건 on 2018. 4. 20..
//  Copyright © 2018년 이동건. All rights reserved.
//

import UIKit

class PageCell: UICollectionViewCell {
    var parentVC: CustomTabbarVC? {
        didSet {
            parentVC?.addChild(self.viewController)
        }
    }
    
    var viewController: UIViewController = .init() {
        didSet {
            self.updateLayout()
            self.layoutIfNeeded()
        }
    }
    
    init(viewController: UIViewController) {
        self.viewController = viewController
        
        super.init(frame: .zero)
        self.updateLayout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func updateLayout() {
        self.subviews.forEach {
            $0.removeFromSuperview()
        }
        self.addSubview(viewController.view)
        viewController.view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        viewController.view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        viewController.view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        viewController.view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
//        viewController.view.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor).isActive = true
//        viewController.view.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor).isActive = true
    }
}
