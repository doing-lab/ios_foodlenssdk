//
//  CustomCell.swift
//  CustomTabBar
//
//  Created by 이동건 on 2018. 4. 18..
//  Copyright © 2018년 이동건. All rights reserved.
//

import UIKit

class TabTitleCell: UICollectionViewCell {
    
    var label: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textAlignment = .center
        label.font = .boldSystemFont(ofSize: 15)
        label.textColor = .lightGray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var title: String = "" {
        didSet {
            self.label.text = self.title
        }
    }
    
    var font: UIFont = .boldSystemFont(ofSize: 15) {
        didSet {
            label.font = self.font
        }
    }
    
    var normalTabColor: UIColor = UIColor(red:0.53, green:0.53, blue:0.53, alpha:1.0)
    var selectedTabColor: UIColor = UIColor(red:0.20, green:0.20, blue:0.20, alpha:1.0)
    
    override var isSelected: Bool {
        didSet{
            self.label.textColor = isSelected ? selectedTabColor : normalTabColor
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addSubview(label)
        
        label.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        label.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        label.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor).isActive = true
        label.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor).isActive = true
//        label.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
//        label.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
}
