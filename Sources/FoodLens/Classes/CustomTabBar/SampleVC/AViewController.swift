//
//  AViewController.swift
//  CustomTabBar
//
//  Created by Eddy Lee on 2023/01/09.
//  Copyright © 2023 이동건. All rights reserved.
//

import UIKit

class AViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let titleView: UITextView = .init()
        titleView.text = "Hello A !!"
        titleView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(titleView)
        self.view.translatesAutoresizingMaskIntoConstraints = false
        self.view.backgroundColor = .blue
        titleView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        titleView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        titleView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        titleView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
