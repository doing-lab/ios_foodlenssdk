//
//  LocalDataModel.swift
//  FoodLens
//
//  Created by HwangChun Lee on 15/03/2019.
//
import UIKit


struct LocalDataPredictBaseInfo : Codable {
    var filename : String = ""
    var eatDate : Date = Date()
    var mealType : Int = 0
    var eatHistoryId : Int = 0
    var foodInfoId : Int = 0
}

struct LocalDataPredictPosition : Codable {
    var id : Int = 0
    var score : Float = 0.0
    var xmin : Int = 0
    var xmax : Int = 0
    var ymin : Int = 0
    var ymax : Int = 0
    
    var amount : Float = 1
    var calorie : Float = 0
    var totalgram:Float = 0.0
    var foodname : String = ""
    var isDeleted : Bool = false
    var isManual : Bool = false
    
    init (id : Int, score : Float, xmin : Int, xmax : Int, ymin : Int, ymax : Int) {
        self.id = id
        self.score = score
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax
    }
    
    func toJsonString() -> String? {
        do {
            let jsonData: Data = try JSONEncoder().encode(self)
            return .init(data: jsonData, encoding: .utf8)
        } catch {
            return ""
        }
    }
    
    init?(json: String) {
        do {
            self = try JSONDecoder().decode(LocalDataPredictPosition.self, from: json.data)
        } catch {

        }
    }
}

struct LocalDataFoodCandidate : Codable {
    var positionId : Int = 0
    var isSelected : Bool = false
    var foodInfoId: Int = 0
    var foodname: String=""
    var manufacturer: String=""
    var predictKey: String = ""
    var keyname: String=""
    var possibility:Float = 0.0
    
    init(positionId : Int, isSelected : Bool, foodInfoId : Int, foodName : String, manufacturer : String, predictKey : String, keyname: String, possibility:Float) {
        self.positionId = positionId
        self.isSelected = isSelected
        self.foodInfoId = foodInfoId
        self.foodname = foodName
        self.manufacturer = manufacturer
        self.predictKey = predictKey
        self.keyname = keyname
        self.possibility = possibility
    }
    
    init(positionId: Int, isSelected: Bool, foodInfoId: Int, foodName: String) {
        self.positionId = positionId
        self.isSelected = isSelected
        self.foodInfoId = foodInfoId
        self.foodname = foodName
    }
}

struct LocalDataFoodInfo : Codable {
    var id:Int = 0
    var predict_key:String = ""
    var foodcategory:String = ""
    var maincountry:String = ""
    var foodname:String = ""
    var unit:String = ""
    var foodtype: String? = ""
    var calories:Float = 0
    var cholesterol:Float = 0
    var fat:Float = 0
    var totalgram:Float = 0
    var carbonhydrate:Float = 0
    var transfat:Float = 0
    var dietrayfiber:Float = 0
    var saturatedfat:Float = 0
    var calcium:Float = 0
    var protein:Float = 0
    var sodium:Float = 0
    var sugar:Float = 0
    var vitamina:Float = 0
    var vitaminb:Float = 0
    var vitaminc:Float = 0
    var vitamind:Float = 0
    var vitamine:Float = 0
}
