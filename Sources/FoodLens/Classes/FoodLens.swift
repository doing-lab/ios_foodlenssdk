//
//  FoodLens.swift
//  FoodLens
//
//  Created by leeint on 23/01/2019.
//  Copyright © 2019 leeint. All rights reserved.
//

import UIKit

#if SWIFT_PACKAGE
    let foodlensResourceBundle = Bundle.module
#else
    let foodlensResourceBundle = Bundle(identifier: "org.cocoapods.FoodLens") ?? Bundle.main
#endif

/**
 Manager class of FoodLens library
 */
@available(iOS 10.0, *)
open class FoodLens {
    private static let bundle: Bundle = foodlensResourceBundle
    private static var accessToken:String? = nil
    private static var appToken:String? = nil
    private static var companyToken : String? = nil
    private static var networkService:NetworkService? = nil
    private static var uiService : UIService? = nil

    //configuarable option
    private static var _uiServiceMode : UIServiceMode = .userSelectedOnly
    private static var _isSaveToGallery : Bool = false
    private static var _isShowHelp : Bool = false
    private static var _helpImages : [UIImage]? = nil
    private static var _isEnableCameraOrientation : Bool = true
    internal static var _eatDate : Date? = nil
    internal static var _eatType : MealType? = nil
    internal static var _isEnableManualInput : Bool = true
    internal static var _isEnablePhtoGallery : Bool = true
    internal static var _isUseImageRecordDate : Bool = true

    internal static var _language: LanguageConfig = .device
    internal static var sdkVersion:String = ""
    internal static var bundleIdentifier:String = ""
    internal static var deviceId:String = ""

    
    internal static var navigationBarTheme : NavigationBarTheme! = nil
    internal static var toolbarButtonTheme : ToolBarButtonTheme! = nil
    internal static var buttonTheme : ButtonTheme! = nil
    internal static var widgetButtonTheme : ButtonTheme! = nil
    
    private static func initFoodLens(accessToken : String?, appToken : String?, companyToken : String?) {
        if let appVersion = bundle.infoDictionary?["CFBundleShortVersionString"] as? String {
            sdkVersion = appVersion

        }

        if let bundleName = Bundle.main.bundleIdentifier {
            bundleIdentifier = bundleName
        }
        
        if let uuid = SystemInfo.getUserUUID() {
            deviceId = uuid
        } else {
            let uuid = UUID()
            SystemInfo.setUserUUID(uuid: uuid.uuidString)
            deviceId = uuid.uuidString
        }
        
        
        
        if buttonTheme == nil {
            buttonTheme = ButtonTheme()
        }
        
        if navigationBarTheme == nil {
            navigationBarTheme = NavigationBarTheme()
        }
        
        if toolbarButtonTheme == nil {
            toolbarButtonTheme = ToolBarButtonTheme()
        }
        
        if widgetButtonTheme == nil {
            widgetButtonTheme = ButtonTheme()
        }
        
        let appDelegate = UIApplication.shared.delegate as! UIApplicationDelegate
        
        
        self.accessToken = accessToken
        self.appToken = appToken
        self.companyToken = companyToken
        HttpHelper.accessToken = accessToken
        HttpHelper.appToken = appToken
        HttpHelper.companyToken = companyToken
        HttpHelper.checkServerAddress()
        checkInteralMode()
    }
    
    private static func initCustomUI() {
    }
    
    private static func checkInteralMode() {
        let infoDic = Bundle.main.infoDictionary!
        
        if let isOnServer = infoDic["FoodLensInternalMode"] {
            if isOnServer as! Bool {
                CustomFoodManager.getInstance().setLocalMode(isLocal: false)
                PredictionResult.internalUseMode = true
                CameraViewController.internalUseMode = true
            }
        }
    }

    
    /**
     Start network services, when you use only network library, call the function below
     
     - parameter accessToken : accessToken that you receive
     
     - returns: NetworkService object that helps you request the recognition of image
     
     */
    public static func createNetworkService(accessToken : String?) -> NetworkService {
        let accessToken = accessToken ?? FoodLens.accessToken
        let appToken = FoodLens.appToken
        let companyToken = FoodLens.companyToken
        FoodLens.initFoodLens(accessToken : accessToken, appToken: appToken, companyToken: companyToken)
        if networkService == nil {
            networkService = NetworkService()
        }
        return networkService!
    }

    /**
     Start network services, when you use only network library, call the function below
     
     - parameter nutritionRetrieveMode : Nutrition retrive mode
     - parameter accessToken : accessToken that you receive
     
     - returns: NetworkService object that helps you request the recognition of image
     
     */
    public static func createNetworkService(nutritionRetrieveMode: NutritionRetrieveMode, accessToken : String?) -> NetworkService {
        let accessToken = accessToken ?? FoodLens.accessToken
        let appToken = FoodLens.appToken
        let companyToken = FoodLens.companyToken

        FoodLens.initFoodLens(accessToken : accessToken, appToken: appToken, companyToken: companyToken)
        if networkService == nil {
            networkService = NetworkService()
        }
        networkService!.nutritionRetrieveMode = nutritionRetrieveMode
        return networkService!
    }

    /**
     Start network services, when you use only network library, call the function below
     
     - parameter nutritionRetrieveMode : Nutrition retrive mode
     - parameter appToken : Application token that you receive
     - parameter companyToken : Company token that you receive
     - returns: NetworkService object that helps you request the recognition of image
     
     */
    public static func createNetworkService(nutritionRetrieveMode: NutritionRetrieveMode, appToken : String?, companyToken : String?) -> NetworkService {
        let appToken = appToken ?? FoodLens.appToken
        let companyToken = companyToken ?? FoodLens.companyToken
        
        FoodLens.initFoodLens(accessToken : nil, appToken : appToken, companyToken: companyToken)
        if networkService == nil {
            //FIXME need to remove access token and user id
            networkService = NetworkService()
        }
        networkService!.nutritionRetrieveMode = nutritionRetrieveMode
        return networkService!
    }

    /**
     Start network services, when you use only network library, call the function below
     
     - parameter appToken : Application token that you receive
     - parameter companyToken : Company token that you receive

     - returns: NetworkService object that helps you request the recognition of image
     
     */
    public static func createNetworkService(appToken : String?, companyToken : String?) -> NetworkService {
        let appToken = appToken ?? FoodLens.appToken
        let companyToken = companyToken ?? FoodLens.companyToken
        
        FoodLens.initFoodLens(accessToken : nil, appToken:appToken, companyToken:companyToken)
        if networkService == nil {
            //FIXME need to remove access token and user id
            networkService = NetworkService()
        }
        return networkService!
    }
    
    /**
     Start UI services with company access token
     
     - parameter accessToken : Company access token that you receive
     
     - returns: UIService object that helps you use our recognition system UI
     
     */
    public static func createUIService(accessToken : String?) -> UIService {
        FoodLens.initFoodLens(accessToken : accessToken, appToken : nil, companyToken: nil)
        FoodLens.initCustomUI()
        if uiService == nil {
            uiService = UIService()
        }
        return uiService!
    }

    /**
     Start UI services with user based mode
     
     - parameter appToken : Application token that you receive
     - parameter companyToken : Company token that you receive
     - returns: UIService object that helps you use our recognition system UI
     
     */
    public static func createUIService(appToken : String?, companyToken : String?) -> UIService {
        FoodLens.initFoodLens(accessToken : nil, appToken : appToken, companyToken: companyToken)
        FoodLens.initCustomUI()
        if uiService == nil {
            uiService = UIService()
        }
        return uiService!
    }

    /**
     Start UI services with custom theme
     
     - parameter accessToken : Company access token that you receive
     - parameter navigationBarTheme : theme of navigation bar
     - parameter toolbarTheme : theme of toolbar (bottom side of view)
     - parameter buttonTheme : theme of food add and delete buttons, only 'textColor' is valid
     - parameter widgetButtonTheme : theme of buttons in amount input view (see Other Results button and manual input)
     
     - returns: UIService object that helps you use our recognition system UI
     
     */
    public static func createUIService(accessToken : String?, navigationBarTheme navTheme : NavigationBarTheme, toolbarTheme toolTheme: ToolBarButtonTheme, buttonTheme btnTheme : ButtonTheme, widgetButtonTheme : ButtonTheme) -> UIService {
        FoodLens.buttonTheme = btnTheme
        FoodLens.navigationBarTheme = navTheme
        FoodLens.toolbarButtonTheme = toolTheme
        FoodLens.widgetButtonTheme = widgetButtonTheme
        
        return createUIService(accessToken : accessToken)
    }

    /**
     Start UI services with custom theme
     
     - parameter appToken : Application token that you receive
     - parameter companyToken : Company token that you receive
     - parameter navigationBarTheme : theme of navigation bar
     - parameter toolbarTheme : theme of toolbar (bottom side of view)
     - parameter buttonTheme : theme of food add and delete buttons, only 'textColor' is valid
     - parameter widgetButtonTheme : theme of buttons in amount input view (see Other Results button and manual input)
     
     - returns: UIService object that helps you use our recognition system UI
     
     */
    public static func createUIService(appToken : String?, companyToken : String?, navigationBarTheme navTheme : NavigationBarTheme, toolbarTheme toolTheme: ToolBarButtonTheme, buttonTheme btnTheme : ButtonTheme, widgetButtonTheme : ButtonTheme) -> UIService {
        FoodLens.buttonTheme = btnTheme
        FoodLens.navigationBarTheme = navTheme
        FoodLens.toolbarButtonTheme = toolTheme
        FoodLens.widgetButtonTheme = widgetButtonTheme
        
        return createUIService(appToken: appToken, companyToken: companyToken)
    }

    /**
     Set language (en, ko)
     */
    public static var language: LanguageConfig {
        get {
            return _language
        }
        
        set {
            _language = newValue
        }
    }
    
    /**
     Set service mode of UI, see UIServiceMode
     */
    public static var uiServiceMode : UIServiceMode {
        get {
            return _uiServiceMode
        }
        
        set {
            _uiServiceMode = newValue
        }
    }
    
    /**
    Set Photo saving mode, if it is true, taken photo will be saved to the gallery, otherwise, it won't be
    */
    public static var isSaveToGallery : Bool {
        get {
            return _isSaveToGallery
        }
        
        set {
            _isSaveToGallery = newValue
        }
    }
    
    /**
     Set date of eating, if it is not nil, it will be returned as RecognitionResult.getDate() which is the parameter of UserServiceResultHandler.onSuccess
     otherwise, current date will be returned
     */
    public static var eatDate : Date? {
        get {
            return _eatDate
        }
        
        set {
            _eatDate = newValue
        }
    }
    
    /**
     Set type of eating, see MealType
     if it is not nil, it will be returned as RecognitionResult.getMealType() which is the parameter of UserServiceResultHandler.onSuccess
     otherwise, self-calculated value will be returned
     */
    public static var eatType : MealType? {
        get {
            return _eatType
        }
        
        set {
            _eatType = newValue
        }
    }
    
    /**
     Set whether help page is showed or not
    */
    public static var isShowHelp : Bool {
        get {
            return _isShowHelp
        }
        set {
            _isShowHelp = newValue
        }
    }
    
    /**
     Set the images to be showed on the help page
    */
    public static var helpImages : [UIImage]? {
        get {
            return _helpImages
        }
        set {
            _helpImages = newValue
        }
    }
    
    /**
     Set Camera Orientation, default value is true
     when it is true, rotate image by orientation
     otherwise, do not rotate image.
    */
    public static var isEnableCameraOrientation : Bool {
        get {
            return _isEnableCameraOrientation
        }
        
        set {
            _isEnableCameraOrientation = newValue
        }
    }
    
    /**
        Set whether manual input button is displayed or not
    */
    public static var isEnableManualInput : Bool {
        get {
            return _isEnableManualInput
        }
        
        set {
            _isEnableManualInput = newValue
        }
    }

    /**
        Set whether photh gallery button is displayed or not
    */
    public static var isEnablePhtoGallery : Bool {
        get {
            return _isEnablePhtoGallery
        }
        
        set {
            _isEnablePhtoGallery = newValue
        }
    }

    /**
        Set whether use date which image was taken
    */
    public static var isUseImageRecordDate : Bool {
        get {
            return _isUseImageRecordDate
        }
        
        set {
            _isUseImageRecordDate = newValue
        }
    }

    

}

extension UIApplication {
    var statusBarView: UIView? {
        var statusBarView: UIView?
        if #available(iOS 13.0, *) {
            /*let tag = 38482458385
            if let statusBar = UIApplication.shared.keyWindow?.viewWithTag(tag) {
                statusBarView = statusBar
            }
            else {
                let statusBar = UIView(frame: UIApplication.shared.statusBarFrame)
                statusBar.tag = tag
                UIApplication.shared.keyWindow?.addSubview(statusBar)
                statusBarView = statusBar
            }
            statusBarView = UIApplication.shared.statusBarView*/
            statusBarView = nil
        }
        else {
            statusBarView = UIApplication.shared.value(forKey: "statusBar") as? UIView
        }
        return statusBarView
    }
}
