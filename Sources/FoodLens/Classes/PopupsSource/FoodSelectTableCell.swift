//
//  FoodSelectTableViewCell.swift
//  DietCameraAI
//
//  Created by HwangChun Lee on 03/01/2019.
//  Copyright © 2019 leeint. All rights reserved.
//

import UIKit

class FoodSelectTableCell: UITableViewCell {
    @IBOutlet weak var foodName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
