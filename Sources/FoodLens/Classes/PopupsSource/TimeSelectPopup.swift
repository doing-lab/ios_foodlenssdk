//
//  TimeSelectPopup.swift
//  FoodLens
//
//  Created by Eunjin on 03/04/2020.
//

import UIKit

class TimeSelectPopup: UIView {
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var timePicker: UIDatePicker!
    @IBOutlet weak var okButton: UIButton!
    weak var targetButton : UIButton?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
