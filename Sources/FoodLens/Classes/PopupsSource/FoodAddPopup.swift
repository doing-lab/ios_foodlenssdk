//
//  FoodAddPopup.swift
//  DietCameraAI
//
//  Created by HwangChun Lee on 02/01/2019.
//  Copyright © 2019 leeint. All rights reserved.
//

import UIKit

class FoodAddPopup: UIView {
    @IBOutlet weak var baseView: UIView!
    
    @IBOutlet weak var FoodNameView: UITextField!
    @IBOutlet weak var AmountView: UITextField!
    @IBOutlet weak var CalorieView: UITextField!
    @IBOutlet weak var GramView: UITextField!
    
    @IBOutlet weak var doneButton: DesignableButton!
    
    var foodOriginTitle : String = ""
    
    var backupValue : String? = nil
    
    var isFromAdd : Bool = false
    
    weak var activeField : UITextField? = nil
    
    var undoMgr : UndoManager = UndoManager()
    
    @IBAction func didBackgroundPressed(_ sender: Any) {
        removeAnimate()
    }
    
    @IBAction func didCloseButtonPressed(_ sender: Any) {
        removeAnimate()
    }
    
    override func awakeFromNib() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        addConfirmButtonToTextField(textField: FoodNameView, action: #selector(didFoodNameViewConfirmPressed))
        addConfirmButtonToTextField(textField: GramView, action: #selector(didGramViewConfirmPressed))
        addConfirmButtonToTextField(textField: AmountView, action: #selector(didAmountConfirmPressed))
        addConfirmButtonToTextField(textField: CalorieView, action: #selector(didCalorieConfirmPressed))
        
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromTop
        
        FoodNameView.text = foodOriginTitle
        
        self.baseView.layer.add(transition, forKey: kCATransition)
    }
    
    @objc func didGramViewConfirmPressed() {
        backupValue = nil
        activeField = nil
        GramView.endEditing(true)
    }
    
    @objc func didFoodNameViewConfirmPressed() {
        backupValue = nil
        activeField = nil
        FoodNameView.endEditing(true)
    }
    
    @objc func didAmountConfirmPressed() {
        backupValue = nil
        activeField = nil
        AmountView.endEditing(true)
    }
    
    @objc func didCalorieConfirmPressed() {
        backupValue = nil
        activeField = nil
        CalorieView.endEditing(true)
    }
    
    @objc func keyboardWillShow(_ sender:Notification){
        self.frame.origin.y = -80
    }
    
    @objc func keyboardWillHide(_ sender:Notification){
        self.frame.origin.y = 0
    }
    
    @IBAction func didBowlPressed(_ sender: Any) {
        rollbackChanges()
        AmountView.becomeFirstResponder()
    }
    
    @IBAction func didFoodNameChanged(_ sender: Any) {
        if self.FoodNameView.text!.count < 1 {
            doneButton.isEnabled = false
            doneButton.layer.borderColor = UIColor(red:0.81, green:0.81, blue:0.81, alpha:1.0).cgColor
        }
        else {
            doneButton.isEnabled = true
            doneButton.layer.borderColor = UIColor(red:0.04, green:0.36, blue:0.42, alpha:1.0).cgColor
        }
    }
    
    @IBAction func didInputEnded(_ sender: Any) {
        let foodName : String = FoodNameView.text ?? ""
        let amount : Float = Float(AmountView.text ?? "0.0") ?? 0.0
        let calorie : Float = Float(CalorieView.text ?? "-1.0") ?? -1.0
        let gram : Float = Float(GramView.text ?? "0.0") ?? 0.0
        
        if foodName.count < 1 || amount <= 0.0 || calorie < 0.0 || gram <= 0.0 {
            doneButton.isEnabled = false
            doneButton.layer.borderColor = UIColor(red:0.81, green:0.81, blue:0.81, alpha:1.0).cgColor
        }
        else {
            doneButton.isEnabled = true
            doneButton.layer.borderColor = UIColor(red:0.04, green:0.36, blue:0.42, alpha:1.0).cgColor
        }
    }
    
    func rollbackChanges() {
        if let field = activeField {
            field.endEditing(true)
            if let undoString = backupValue {
                field.text = String(undoString)
                backupValue = nil
            }
            activeField = nil
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        rollbackChanges()
        FoodNameView.endEditing(true)
        AmountView.endEditing(true)
        CalorieView.endEditing(true)
        GramView.endEditing(true)
    }
    
    func addConfirmButtonToTextField(textField : UITextField, action: Selector?) {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let confirmButton = UIBarButtonItem(title: "OK  ", style: .done, target: self, action: action)
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.items = [flexibleSpace, confirmButton]
        textField.inputAccessoryView = toolbar
    }
    
    func removeAnimate() {
        UIView.animate(withDuration: 0.5, animations: {
            self.baseView.transform = CGAffineTransform(translationX: 0.0, y: UIScreen.main.bounds.height)
            self.baseView.alpha = 1.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.removeFromSuperview()
            }
        });
    }
    
    /*@IBAction func didFoodNameEditBegin(_ sender: Any) {
        rollbackChanges()
        activeField = FoodNameView
        backupValue = String(FoodNameView.text ?? "")
    }
    
    @IBAction func didAmountEditBegin(_ sender: Any) {
        rollbackChanges()
        activeField = AmountView
        backupValue = String(AmountView.text ?? "")
    }
    
    @IBAction func didGramEditBegin(_ sender: Any) {
        rollbackChanges()
        activeField = GramView
        backupValue = String(GramView.text ?? "")
    }
    
    @IBAction func didCalorieEditBegin(_ sender: Any) {
        rollbackChanges()
        activeField = CalorieView
        backupValue = String(CalorieView.text ?? "")
    }*/
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
