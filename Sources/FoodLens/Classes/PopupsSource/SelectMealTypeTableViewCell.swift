//
//  SelectMealTypeTableViewCell.swift
//  Pods
//
//  Created by leeint on 2020/04/28.
//

import UIKit

class SelectMealTypeTableViewCell: UITableViewCell {

    //@IBOutlet weak var mealTypeLabel: UILabel!
    @IBOutlet weak var mealTypeLabel: UILabel!
    let bundle: Bundle = foodlensResourceBundle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func makeTextColor(of selected: String) {
        if selected.lowercased() == (mealTypeLabel.text)?.lowercased() {
            guard let mealType = mealTypeLabel.text else { return }
            let textColor = UIColor.hexStringToUIColor(hex: "#cf3235")
            let attributes: [NSAttributedString.Key:Any] = [NSAttributedString.Key.foregroundColor:textColor]
            let attributedString = NSAttributedString(string: mealType, attributes: attributes)
            mealTypeLabel.attributedText = attributedString
        }
    }
}
