//
//  MealTypeSelectPopup.swift
//  DietCameraAI
//
//  Created by HwangChun Lee on 02/01/2019.
//  Copyright © 2019 leeint. All rights reserved.
//

import UIKit

class MealTypeSelectPopup: UIView {
    @IBOutlet weak var selectMealTypeTableView: UITableView!
    //private let mealTypeCell: String = "SelectMealTypeCell"
    var selectedMealType: MealType = .breakfast
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var title: UILabel!
    
    let bundle: Bundle = foodlensResourceBundle
    
    override func awakeFromNib() {
        self.selectMealTypeTableView.delegate = self
        self.selectMealTypeTableView.dataSource = self
        registerXib()
        title.text = NSLocalizedString("select_meal_title", bundle: bundle, comment: "")
        addAnimate()
    }

    func registerXib() {
        let nibName = UINib(nibName: "SelectMealTypeTableViewCell", bundle: bundle)
        selectMealTypeTableView.register(nibName, forCellReuseIdentifier: "SelectMealTypeTableViewCell")
    }

    @IBAction func didBackgroundPressed(_ sender: Any) {
        removeAnimate()
    }

    @IBAction func didCloseButtonPressed(_ sender: Any) {
        removeAnimate()
    }
    
    func addAnimate() {
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromTop
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
        self.baseView.layer.add(transition, forKey: kCATransition)
    }

    func removeAnimate() {
        UIView.animate(withDuration: 0.5, animations: {
            self.baseView.transform = CGAffineTransform(translationX: 0.0, y: UIScreen.main.bounds.height)
            self.baseView.alpha = 1.0;
        }, completion:{(finished : Bool) in
            if (finished) {
                self.removeFromSuperview()
            }
        });
    }
}

extension MealTypeSelectPopup: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedMealType == MealType.snack { // snack
            return MealType.order.count
        }
        return MealType.order.count - 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //guard let cell = selectMealTypeTableView.dequeueReusableCell(withIdentifier: "SelectMealTypeCell") as? SelectMealTypeTableViewCell else { return UITableViewCell() }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SelectMealTypeTableViewCell", for: indexPath) as? SelectMealTypeTableViewCell else { return UITableViewCell() }

        let index = makeIndex(by: indexPath.row)
        cell.mealTypeLabel.text = NSLocalizedString(MealType.order[index], bundle: bundle, comment: "")
        cell.makeTextColor(of: NSLocalizedString(selectedMealType.rawValue, bundle: bundle,comment: "") )
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = makeIndex(by: indexPath.row)
        self.selectedMealType = MealType.getMealType(by: index)
        CameraViewController.predictResult.mealType = makeMealType(by: MealType.order[index])
        NotificationCenter.default.post(name: NSNotification.Name("notificationClosedPopup"), object: nil)
        self.removeAnimate()
    }
    
    func makeMealType(by name: String) -> MealType {
        var selectedIndex = 0
        for index in 0..<MealType.order.count {
            print("\(name.lowercased()), \(MealType.order[index])")
            if name.lowercased() == MealType.order[index] {
                selectedIndex = index
            }
        }
        return MealType.getMealType(by: selectedIndex)
    }
    
    func makeIndex(by row: Int) -> Int {
        let index = row
        if selectedMealType != MealType.snack, row >= 3 { // 아침, 점심, 저녁 간식일 때 인덱스 +1
            return index + 1
        }
        return index
    }
}
