//
//  SearchFoodPopup.swift
//  DietCameraAI
//
//  Created by HwangChun Lee on 02/11/2018.
//  Copyright © 2018 leeint. All rights reserved.
//

import UIKit

protocol MaunalInputProtocol {
    //func manualInputComplete(searchedFood : FoodSearchList)
    func manualInputComplete(foodInfo : FoodInfo, searchedFoodName:String, keyType : ChangeKeyType)
    //func manualInputComplete(foodname : String, amount : Float, calorie : Float, totalGram : Float)
    //func changeToManualInput()
}
