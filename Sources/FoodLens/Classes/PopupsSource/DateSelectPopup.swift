//
//  DateSelectPopup.swift
//  DietCameraAI
//
//  Created by HwangChun Lee on 2018. 8. 30..
//  Copyright © 2018년 leeint. All rights reserved.
//

import UIKit

class DateSelectPopup: UIView {
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var okButton: UIButton!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBAction func didBackgroundPressed(_ sender: Any) {
        removeFromSuperview()
    }
}
