//
//  PredictLoadingPopup.swift
//  DietCameraAI
//
//  Created by HwangChun Lee on 2018. 10. 17..
//  Copyright © 2018년 leeint. All rights reserved.
//

import UIKit

@available(iOS 10.0, *)
class PredictLoadingPopup: UIView {
    let bundle: Bundle = foodlensResourceBundle
    
    let animationImages:[UIImage] = [
        UIImage(named: "img_loading1", in: foodlensResourceBundle, compatibleWith : nil)!,
        UIImage(named: "img_loading2", in: foodlensResourceBundle, compatibleWith : nil)!,
        UIImage(named: "img_loading3", in: foodlensResourceBundle, compatibleWith : nil)!,
        UIImage(named: "img_loading4", in: foodlensResourceBundle, compatibleWith : nil)!,
        UIImage(named: "img_loading5", in: foodlensResourceBundle, compatibleWith : nil)!,
        UIImage(named: "img_loading6", in: foodlensResourceBundle, compatibleWith : nil)!
    ]
    
    var superViewController : UIViewController? = nil
    
    @IBOutlet weak var loadingImageView: UIImageView!
    
    override func awakeFromNib() {
        loadingImageView.animationImages = animationImages;
        loadingImageView.animationDuration = TimeInterval(2);
        loadingImageView.animationRepeatCount = 20
        loadingImageView.startAnimating()
    }
    
    @IBAction func didCancelPressed(_ sender: Any) {
        HttpHelper.cancelAllRequests()
        if let superView = superViewController {
            if let camera = superView as? CameraViewController {
                camera.closeLoadingImage(true)
                camera.setAllButtonEnabled(isEnabled: true)
            }
        }
        self.removeFromSuperview()
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
}
