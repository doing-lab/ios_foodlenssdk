//
//  AmountSelectPopup.swift
//  DietCameraAI
//
//  Created by HwangChun Lee on 04/01/2019.
//  Copyright © 2019 leeint. All rights reserved.
//

import UIKit

class AmountSelectPopup: UIView, UITableViewDelegate, UITableViewDataSource {
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    var changeAmountProtocol : ChangeAmountProtocol?
    
    var amountList : [String] = []
    
    let bundle: Bundle = foodlensResourceBundle
    
    @IBOutlet weak var tableView: UITableView!
    
    override func awakeFromNib() {
        tableView.register(UINib(nibName: "FoodSelectTableCell", bundle: bundle), forCellReuseIdentifier: "FoodSelectTableCell")
        amountList = [NSLocalizedString("manual_input", tableName: nil, bundle: self.bundle, value: "", comment: ""), "1/4", "1/2", "3/4", "1", "2", "3","4","5","6","7","8","9","10"]
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
    }
    
    @IBAction func didBackgroundPressed(_ sender: Any) {
        removeFromSuperview()
    }
    
    @IBAction func didTitlePressed(_ sender: Any) {
    }
    
    @IBAction func didCancelPressed(_ sender: Any) {
        removeFromSuperview()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.amountList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FoodSelectTableCell", for: indexPath) as! FoodSelectTableCell
        cell.foodName.text = amountList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if let proc = changeAmountProtocol {
                proc.manualInputSelected()
            }
            removeFromSuperview()
        }
        else {
            var selectedValue = "1"
            if indexPath.row < amountList.count {
                selectedValue = self.amountList[indexPath.row]
            }
            else {
                selectedValue = self.amountList[indexPath.row]
            }
            
            if let proc = changeAmountProtocol {
                proc.changeAmount(amount: selectedValue)
            }
            removeFromSuperview()
        }
    }
}
