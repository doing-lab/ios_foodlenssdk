//
//  SearchFoodPopupCell.swift
//  DietCameraAI
//
//  Created by HwangChun Lee on 02/11/2018.
//  Copyright © 2018 leeint. All rights reserved.
//

import UIKit

class SearchFoodPopupCell: UITableViewCell {
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func didDeletePressed(_ sender: Any) {
    }
}
