//
//  PredictionResult.swift
//  FoodLens
//
//  Created by leeint on 23/01/2019.
//  Copyright © 2019 leeint. All rights reserved.
//

import UIKit


/**
 Result of food recognition process
 */
public enum ProcessState {
    /**
     Recognition process is succeeded
     */
    case success
    
    /**
     Recognition process is failed
     */
    case fail
}

/**
 Result explaination of request
 
 **Note**: This enum is used only by network services.
 */
public enum ProcessReason {
    /**
     Success
     */
    case success
    
    /**
     An error occurred during image conversion.
     */
    case imageProcessingError
    
    /**
     Authorization failed, check your access token
     */
    case invalidAccessTokenError
    
    /**
     Invalid response value received from server.
     */
    case invalidServerResponseError
    
    /**
     The server is not working.
     */
    case serverNotWorkError
    
    /**
     Authorization failed, check your access token
     */
    case unauthorizedError
    
    /**
     No prediction results.
     */
    case noPredictionInfoError
    
    /**
     An error occurs at encoding process
     */
    case encodingError
    
    /**
     An error occurs at decoding process
     */
    case decodingError
    
    /**
     call count exceeded the limit
     */
    case quotaLimitExceedError
    
    /**
     Cancel Button is pressed during processing
    */
    case userCancel
}

/**
 Result of food recognition process
 */
public class ProcessStatus {
    /**
     Flag whether process is succeeded or not
     */
    public var state:ProcessState = .success
    
    /**
     Explaination of result
     */
    public var reasion:ProcessReason = .success
}

/**
 
 Result of prediction
 
 **Note**: This object is only used by NetworkService
 */
public class PredictionResult : Codable, RecognitionResult {
    internal static let CURRENT_VERSION:Int = 1
    internal var fullImageS3Path : String? = ""
    internal var version : Int? = 0
    internal var predictedImagePath:String?
    internal var foodPositionList:[FoodPosition] = []
    internal var mealType : MealType = .breakfast
    internal var eatDate : Date = FoodLens.eatDate ?? Date()
    static var internalUseMode : Bool?
    
    public static func create(json: String) -> PredictionResult {
        
        let jsonData: Data = json.data(using: .utf8) ?? .init()
        let decoder: JSONDecoder = .init()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        decoder.dateDecodingStrategy = .formatted(formatter)
        
        let predictResult: PredictionResult? = try? decoder.decode(PredictionResult.self, from: jsonData)
        return predictResult ?? .init()
    }
    
    public required init() {
        setEatTypeByDate()
    }
    
    public func setCurrentVersion() {
        version = PredictionResult.CURRENT_VERSION
    }
    
    public func `toJSONString`() -> String? {
        let encoder: JSONEncoder = .init()
        encoder.outputFormatting = [.sortedKeys]
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        encoder.dateEncodingStrategy = .formatted(formatter)
        do {
            let jsonData: Data = try encoder.encode(self)
            return String(data: jsonData, encoding: .utf8)
        } catch {
            return nil
        }
    }
    
    internal func validateDatas() -> (String, Bool) {
        
        var invalidDatas: [(String, Bool)] = .init()
        for i in 0..<foodPositionList.count {
            invalidDatas.append(foodPositionList[i].validateData())
        }
        let invalidData: (String, Bool)? = invalidDatas.first
        
        guard let result = invalidData else {
            return ("valid", true)
        }
        
        return result
    }
    
    public func setEatTypeByDate() {
        if let type = FoodLens.eatType {
            mealType = type
            return
        }
            
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: eatDate)
        
        if components.hour! >= 5 && components.hour! < 10 {
            mealType = .breakfast
        } else if components.hour! >= 10 && components.hour! < 11 {
            mealType = .morning_snack
        } else if components.hour! >= 11 && components.hour! < 13 {
            mealType = .lunch
        } else if components.hour! >= 13 && components.hour! < 17 {
            mealType = .afternoon_snack
        } else if components.hour! >= 17 && components.hour! < 20 {
            mealType = .dinner
        } else {
            mealType = .late_night_snack
        }
    }
    
    
    /**
     Get Path of recognized image
     
     **Note**: This function returns nil when user uses 'Manual Input'
     
     */
    public func getRecognizedImage() -> String? {
        
        if let _ = PredictionResult.internalUseMode {
            return fullImageS3Path;
        }
        
        return predictedImagePath;
    }
    
    /**
     Get FoodPosition object that 'index' indicates
     
     - parameter index : index to get
     
     **Note**: This function returns nil when index is over the length of FoodPosition object list
     
     */
    public func getFoodPositionAtIndex(index : Int) -> FoodPosition? {
        if index < foodPositionList.count {
            return foodPositionList[index]
        }
        return nil
    }
    
    /**
     Get whole member of FoodPosition object list
     
     **Note**: This function returns nil when user uses 'Manual Input'
     
     */
    public func getFoodPositions() -> [FoodPosition] {
        return foodPositionList
    }
    
    /**
     Put FoodPosition object into the end of FoodPosition object list
     
     - parameter food : FoodPosition object that intends to put
     
     **Note**: This function returns nil when user uses 'Manual Input'
     
     */
    public func putFoodPosition(_ food: FoodPosition) {
        foodPositionList.append(food)
    }
    
    /**
     Set Path of recognizeed image
     
     - parameter path : Absolute path of image to store
     
     */
    public func setRecognizedImagePath(_ path: String) {
        predictedImagePath = path
    }
    
    /**
     Get type of meal
     
     - returns: MealType value (.breakfast, .lunch, .dinner. .snack, .morning_snack, .afternoon_snack, .late_night_snack)
     */
    public func getMealType() -> MealType {
        return mealType
    }

    /**
     Set type of meal
     
     - parameter type: MealType value (.breakfast, .lunch, .dinner. .snack, .morning_snack, .afternoon_snack, .late_night_snack )
     */

    public func setMealType(_ type : MealType) {
        self.mealType = type
    }

    /**
     Get date of meal
     
     - returns: Date
     */
    
    public func getDate() -> Date {
        return eatDate
    }
    
    /**
     Set date of meal
     
     - parameter date: date for display on screen
     */
    
    public func setDate(_ date : Date) {
        eatDate = date
    }

}


extension PredictionResult {
    enum CodingKeys: CodingKey {
        case fullImageS3Path
        case version, predictedImagePath, foodPositionList, mealType, eatDate
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = try encoder.container(keyedBy: CodingKeys.self)
        try container.encode(version, forKey: .version)
        try container.encode(predictedImagePath, forKey: .predictedImagePath)
        try container.encode(foodPositionList, forKey: .foodPositionList)
        try container.encode(mealType, forKey: .mealType)
        try container.encode(eatDate, forKey: .eatDate)
    }
}
