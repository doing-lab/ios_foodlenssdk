//
//  HttpHelper.swift
//  DrawShape
//
//  Created by leeint on 2017. 6. 12..
//  Copyright © 2017년 leeint. All rights reserved.
//

import UIKit
import Alamofire


struct PredictionLogKeyItem : Codable {
    var key : String = ""
    
    init(key : String) {
        self.key = key
    }
}

struct PredictionLogUnit : Codable {
    var filename : String = ""
    var userSelect : String = ""
    var keylist : [PredictionLogKeyItem] = []
    var seqLog: String = ""
    
    init(filename : String, select : String, keyList : [PredictionLogKeyItem], seq: String) {
        self.filename = filename
        self.userSelect = select
        self.keylist = keyList
        self.seqLog = seq
    }
    
    init(filename : String, select : String, keyStringList : [String], seq: String) {
        self.filename = filename
        self.userSelect = select
        self.keylist = keyStringList.map{ PredictionLogKeyItem(key: $0) }
        self.seqLog = seq
    }
    
    init(foodPosition : FoodPosition) {
        self.filename = foodPosition.s3ImagePath ?? ""
        self.userSelect = foodPosition.userSelectedFood?.foodName ?? ""
        self.keylist = foodPosition.foodCandidates.map{ PredictionLogKeyItem(key : $0.keyName ?? "") }
        self.seqLog = foodPosition.seqLog ?? ""
    }
}

struct PostPredictionLog : Codable {
    var full_image_path : String = ""
    var source : String = "sdk"
    var device : String = "ios"
    var device_id : String = "";
    var sdk_version : String = "";
    var loglist : [PredictionLogUnit] = []
}

struct UserLogUnit : Codable {
    var date : String?
    var eventId : String?
    var param1 : Int = 0
    var param2 : Int = 0
    var comment : String?
    var source : Int = 1
}

struct FoodItemInfo:Codable {
    var calorie: Int = 0
    var comment: String=""
    var date: String=""
    var eatamount:Float=0.0
    var eattype: Int=0
    var foodinfoid:Int = 0
    var foodname:String=""
    var id:Int = 0
    var imageurl: String=""
    var predictkey: String=""
    var saltlevel = 0
    var sweetlevel = 0
    var multi_predict : [MealMultiInfo]?
    var feedbackid : Int = 0
    var gram: Int = 0
}

struct FoodAdd : Codable {
    var mealId:Int = 0
    var comment:String = ""
    var eatdate:String = ""
    var eatamount:Float = 0
    var eattype:Int = 0
    var predictedkeyname:String = ""
    var foodid:Int = 0
    var imagefile:String = ""
    var saltlevel:Int = 0
    var sweetlevel:Int = 0
    var calorie:Float=0.0
    var foodname:String = ""
    //var eatdatereal:Date=Date()
    var multi_predict : [MealMultiInfo]?
    var feedbackId : Int = 0
}

struct AddResult : Codable {
    var result:Bool = false
}

struct MealMultiInfo : Codable {
    var eatamount:Float = 0.0
    var eathistory_id : Int = 0
    var foodinfo_id : Int = 0
    var foodname : String?
    var id : Int = 0
    var score : Float = 0.0;
    var xmin : Int = 0;
    var xmax : Int = 0;
    var ymin : Int = 0;
    var ymax : Int = 0;
    var filename : String = "";
    var predictKey : String = "";
    var gram : Float = 0;
    var calorie : Float = 0;
}

extension String {
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        
        return String(data: data, encoding: .utf8)
    }
    
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
}

struct MultiPredictResult : Codable {
    var filename:String = ""
    var boxes:[MultiPredictBoxes] = []
}

struct MultiPredictBoxesJSONData: Codable {
    var score : Double = 0.0
    var xmin : Int = 0
    var xmax : Int = 0
    var ymin : Int = 0
    var ymax : Int = 0
}

struct MultiPredictBoxes : Codable, Equatable {
    var score : Double = 0.0;
    var xmin : Int = 0;
    var xmax : Int = 0;
    var ymin : Int = 0;
    var ymax : Int = 0;
    internal var isCropedAsEllipse : Bool = false
    internal var overlappedArea : [MultiPredictBoxes] = []
    
    init(score : Double, xmin : Int, xmax : Int, ymin : Int, ymax : Int) {
        self.score = score
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax
    }
    
    init(jsonData: MultiPredictBoxesJSONData) {
        self.score = jsonData.score
        self.xmin = jsonData.xmin
        self.xmax = jsonData.xmax
        self.ymin = jsonData.ymin
        self.ymax = jsonData.ymax
    }
    
    internal var width : Int {
        get {
            return xmax - xmin
        }
    }
    
    internal var height : Int {
        get {
            return ymax - ymin
        }
    }
    
    internal var area : Int {
        if xmax - xmin < 0 {
            return -1
        }
        
        if ymax - ymin < 0 {
            return -1
        }
        return (xmax - xmin) * (ymax - ymin)
    }
    
    init() {}
}

//struct MultiPredictElement : Codable {
//    var image : UIImage?
//    var selected : NameList?
//    var area : MultiPredictBoxes?
//    var predictList:[PredictList]?
//    var amount : Float = 1.0
//    var filename : String?
//}

struct PredictResult : Codable {
    var filename: String = ""
    var predictlist:[PredictList]?
    var seqLog: String = ""
}

struct PredictList: Codable {
    var foodlist: [NameList]?
    var keyname: String = ""
    var possibility:Float = 0.0
    
    init (foodlist : [NameList]?, keyname : String, possibility : Float) {
        self.foodlist = foodlist
        self.keyname = keyname
        self.possibility = possibility
    }
    
    init (foodlist : [NameList]?) {
        self.foodlist = foodlist
    }
}

struct NameList: Codable {
    var id: Int = 0
    var foodname: String = ""
    var manufacturer: String = ""
    var predict_key: String = ""
    
    init (id : Int, foodname : String) {
        self.id = id
        self.foodname = foodname
    }
    
    init (id : Int, foodname : String, predictkey : String) {
        self.id = id
        self.foodname = foodname
        self.predict_key = predictkey
    }
}

public struct FoodSearchResult : Codable {
    var foodnamelist:[FoodSearchList]?
}
public struct FoodSearchList: Codable, Equatable {
    public var id: Int = 0
    public var foodname: String=""
    public var manufacturer: String=""
    public var predict_key:String=""
    
    func toJsonString() -> String? {
        do {
            let jsonData: Data = try JSONEncoder().encode(self)
            return .init(data: jsonData, encoding: .utf8)
        } catch {
            return nil
        }
    }
}

struct FoodInfo : Codable {
    var id:Int = 0
    var predict_key:String = ""
    var foodcategory:String = ""
    var foodtype: String? = ""
    var maincountry:String = ""
    var foodname:String = ""
    var unit:String = ""
    var calories:Float = 0
    var cholesterol:Float = 0
    var fat:Float = 0
    var totalgram:Float = 0
    var carbonhydrate:Float = 0
    var transfat:Float = 0
    var dietrayfiber:Float = 0
    var saturatedfat:Float = 0
    var calcium:Float = 0
    var protein:Float = 0
    var sodium:Float = 0
    var sugar:Float = 0
    var vitamina:Float = 0
    var vitaminb:Float = 0
    var vitaminc:Float = 0
    var vitamind:Float = 0
    var vitamine:Float = 0
    var manufacturer:String = ""
    var customFoodInfo:Bool = false

    
    init() {}
    
    init(dictionary:
         [String: Any]) {
        self.id = (dictionary["id"] as? Int) ?? 0
        self.predict_key = (dictionary["predict_key"] as? String) ?? ""
        self.foodcategory = (dictionary["foodcategory"] as? String) ?? ""
        self.foodtype = (dictionary["foodtype"] as? String) ?? ""
        self.maincountry = (dictionary["maincountry"] as? String) ?? ""
        self.foodname = (dictionary["foodname"] as? String) ?? ""
        self.unit = (dictionary["unit"] as? String) ?? ""
        self.calories = dictionary.getFloatItem("calories")
        self.cholesterol = dictionary.getFloatItem("cholesterol")
        self.fat = dictionary.getFloatItem("fat")
        self.totalgram = dictionary.getFloatItem("totalgram")
        self.carbonhydrate = dictionary.getFloatItem("carbonhydrate")
        self.transfat = dictionary.getFloatItem("transfat")
        self.dietrayfiber = dictionary.getFloatItem("dietrayfiber")
        self.saturatedfat = dictionary.getFloatItem("saturatedfat")
        self.calcium = dictionary.getFloatItem("calcium")
        self.protein = dictionary.getFloatItem("protein")
        self.sodium = dictionary.getFloatItem("sodium")
        self.sugar = dictionary.getFloatItem("sugar")
        self.vitamina = dictionary.getFloatItem("vitamina")
        self.vitaminb = dictionary.getFloatItem("vitaminb")
        self.vitaminc = dictionary.getFloatItem("vitaminc")
        self.vitamind = dictionary.getFloatItem("vitamind")
        self.vitamine = dictionary.getFloatItem("vitamine")
        self.manufacturer = (dictionary["manufacturer"] as? String) ?? ""
        self.customFoodInfo = (dictionary["customFoodInfo"] as? Bool) ?? false
    }
    
    static func genreated(json: String) -> FoodInfo {
        (try? JSONDecoder().decode(FoodInfo.self, from: json.data)) ?? .init()
    }
    
    func toJsonData() -> Data? {
        try? JSONEncoder().encode(self)
    }
    
    func toJsonString() -> String? {
        do {
            let jsonData: Data = try JSONEncoder().encode(self)
            return .init(data: jsonData, encoding: .utf8)
        } catch {
            return nil
        }
    }
}

private extension [String: Any] {
    func getFloatItem(_ key: String) -> Float {
        switch self[key] {
        case is Float:
            return self[key] as? Float ?? 0.0
        case is String:
            return (self[key] as? String).toFloat()
        default:
            return 0.0
        }
    }
}

private extension String? {
    func toFloat() -> Float {
        (self as? NSString)?.floatValue ?? 0.0
    }
}

extension Decodable {
    static func decode<T: Decodable>(array: [Any]) throws -> T {
        let data = try JSONSerialization.data(withJSONObject: array, options: [.sortedKeys])
        return try JSONDecoder().decode(T.self, from: data)
    }
    
    static func decode<T: Decodable>(dictionary: [String: Any]) throws -> T {
        let data = try JSONSerialization.data(withJSONObject: dictionary, options: [.sortedKeys])
        return try JSONDecoder().decode(T.self, from: data)
    }
}

 class HttpHelper {
    private static let address = "api.foodlens.ai"
    private static let port = "8443"

    private static var server_address = ""
    
    private static let user = "foodlens"
    private static let password = "c4259610493611e9b4750800200c9a66"
    private static var credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
    private static var base64Credentials = credentialData.base64EncodedString(options: [])
    static var accessToken:String? = nil
    static var appToken:String? = nil
    static var companyToken:String? = nil
    
    static func checkServerAddress() {
        let infoDic = Bundle.main.infoDictionary!
        
        if let userServer = infoDic["FoodLensServerAddr"] {
            server_address = String(format: "https://%@:%@", userServer as! String, HttpHelper.port)
        } else {
            server_address = String(format: "https://%@", HttpHelper.address)
        }

        if let authId = infoDic["FoodLensBasicAuthId"] {
            if let authPw = infoDic["FoodLensBasicAuthPw"] {
                credentialData = "\(authId as! String):\(authPw as! String)".data(using: String.Encoding.utf8)!
                base64Credentials = credentialData.base64EncodedString(options: [])
            }
        }
    }
     
     static func createHttpHeader() -> HTTPHeaders {

         let headers:HTTPHeaders?
         
         if accessToken != nil {
             headers = [
                "Authorization": "Basic \(base64Credentials)",
                "Accept": "application/json",
                "CompanyToken": accessToken!,
                "AppToken": "",
                "ApiVersion":"v2",
                "AppVersion":FoodLens.sdkVersion,
                "AppDeviceId": FoodLens.deviceId,
                "AppDevice":"ios",
                "AppPackage": FoodLens.bundleIdentifier,
                "User-agent": "FoodLensSDK \(FoodLens.sdkVersion), Doinglab HTTP Version v2"
             ]

         } else {
             //FIXME need to add error control
             headers = [
                "Authorization": "Basic \(base64Credentials)",
                "Accept": "application/json",
                "CompanyToken": companyToken!,
                "AppToken": appToken!,
                "ApiVersion":"v2",
                "AppVersion":FoodLens.sdkVersion,
                "AppDeviceId":FoodLens.deviceId,
                "AppDevice":"ios",
                "AppPackage": FoodLens.bundleIdentifier,
                "User-agent": "FoodLensSDK \(FoodLens.sdkVersion), Doinglab HTTP Version v2",
             ]
        }
         
         return headers!
     }
    
    static func insertPredictionLog(predictionLog : PostPredictionLog) {
        let subUrl:String = String.init(format: "/logs/prediction")
        
        let headers:HTTPHeaders = createHttpHeader()
        
        if !HttpHelper.server_address.contains(address) {
            return
        }

        let fullAddress = HttpHelper.server_address + subUrl
        AF.request(fullAddress, method: .post, parameters: predictionLog, encoder: JSONParameterEncoder.default, headers: headers)
            .validate()
            .responseDecodable(of: String.self) { response in
                switch response.result {
                case .success:
                    break
                case .failure:
                    debugPrint(response)
                }
            }
    }
    
    static func multiPredictImage(imageData: Data?, locale:String, completion: @escaping (_ user: MultiPredictResult?, _ reasion : ProcessReason) -> ()) {
        if (HttpHelper.accessToken == nil) && (HttpHelper.appToken == nil || HttpHelper.companyToken == nil) {
            completion(nil, .invalidAccessTokenError)
            return
        }

        let subUrl:String = "/predict/multi"
        let headers:HTTPHeaders =  createHttpHeader()
        let fullAddress = HttpHelper.server_address + subUrl
        #if DEBUG
            let parameters: Parameters = ["locale": "\(locale)", "isStore": "true"]
        #else
            let parameters: Parameters = ["locale": "\(locale)", "isStore": "true"]
        #endif
        
       AF.upload(multipartFormData: { (multipartFormData) in
            if let data = imageData{
                multipartFormData.append(data, withName: "file", fileName: "file.jpg", mimeType: "image/jpg")
            }
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: .utf8)!, withName: key)
            }
            
       }, to: fullAddress, method: .post, headers: headers)
       .responseJSON { response in
           switch response.result {
           case .success(let result):
               if response.response?.statusCode == 200 {
                   guard let result = result as? [String: Any] else {
                       completion(nil, .encodingError)
                       return
                   }
                   
                   if let filename: String = result["filename"] as? String,
                      let boxArray: [Any] = result["boxes"] as? [Any] {
                       let data: Data = (try? JSONSerialization.data(withJSONObject: boxArray, options: [.sortedKeys])) ?? Data()
                       let boxes: [MultiPredictBoxesJSONData] = (try? JSONDecoder().decode([MultiPredictBoxesJSONData].self, from: data)) ?? []
                       let predictResult: MultiPredictResult = .init(filename: filename, boxes: boxes.map { .init(jsonData: $0) })
                       completion(predictResult, .success)
                   } else {
                       completion(nil, .encodingError)
                   }
                   
               } else {
                   if (response.error as NSError?)?.code == NSURLErrorCancelled {
                       // Do Your stuff
                       completion(nil, .userCancel)
                   }
                   else {
                       //TODO need to control server response
                       completion(nil, .invalidServerResponseError)
                   }
               }
               break
           case .failure(let error):
               debugPrint(response)
               if (response.response?.statusCode == 401 || response.response?.statusCode == 403) {
                   completion(nil, .invalidAccessTokenError)
               } else {
                   completion(nil, .encodingError)
               }
           }
       }
    }
    
     static func predictImage(imageData: Data?, locale:String, filename: String?, completion: @escaping (_ user: PredictResult?, _ reasion : ProcessReason) -> ()) {
        let subUrl :String = "/predict"
        let headers: HTTPHeaders = createHttpHeader()
        let fullAddress = HttpHelper.server_address + subUrl
        #if DEBUG
         let parameters: Parameters = ["locale": "\(locale)",
                                       "isStore": "false",
                                       "filename": "\(filename ?? "")"]
        #else
        let parameters: Parameters = ["locale": "\(locale)",
                                      "isStore": "true",
                                      "filename": "\(filename ?? "")"]
        #endif
        
        AF.upload(multipartFormData: { (multipartFormData) in
            if let data = imageData{
                multipartFormData.append(data, withName: "file", fileName: "file.jpg", mimeType: "image/jpg")
            }
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: .utf8)!, withName: key)
            }
            
        }, to: fullAddress, method: .post, headers: headers)
        .responseJSON(completionHandler: { response in
            switch response.result {
            case .success(let result):
                if response.response?.statusCode == 200 {
                    guard let result = result as? [String: Any] else {
                        completion(nil, .encodingError)
                        return
                    }
                    
                    if let predictResult: PredictResult = try? PredictResult.decode(dictionary: result) {
                        completion(predictResult, .success)
                    } else {
                        completion(nil, .encodingError)
                    }
                } else {
                    if (response.error as NSError?)?.code == NSURLErrorCancelled {
                        // Do Your stuff
                        completion(nil, .userCancel)
                    } else {
                        //TODO need to control server error case
                        completion(nil, .invalidServerResponseError)
                    }
                }
                break
            case .failure:
                debugPrint(response)
                if (response.response?.statusCode == 401 || response.response?.statusCode == 403) {
                    completion(nil, .invalidAccessTokenError)
                } else {
                    completion(nil, .encodingError)
                }

                break
            }
        })
    }
    
    
    static func getFoodInfo(foodId:Int, completion: @escaping (_ user: FoodInfo?, _ reasion : ProcessReason) -> ()) {
        
        let subUrl:String = String.init(format: "/foodinfo/%d", foodId)
        
        let headers:HTTPHeaders = createHttpHeader()
        let fullAddress = HttpHelper.server_address+subUrl
        AF.request(fullAddress,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .validate()
        .responseDecodable(of: FoodInfo.self, completionHandler: { response in
            switch response.result {
            case .success(let result):
                if response.response?.statusCode == 200 {
                    completion(result, .success)
                }
                else {
                    if (response.error as NSError?)?.code == NSURLErrorCancelled {
                        // Do Your stuff
                        completion(nil, .userCancel)
                    }
                    else {
                        //TODO need to control server error case
                        completion(nil, .invalidServerResponseError)
                    }
                }
            case.failure:
                debugPrint(response)
                if (response.response?.statusCode == 401 || response.response?.statusCode == 403) {
                    completion(nil, .invalidAccessTokenError)
                } else {
                    completion(nil, .encodingError)
                }
            }
        })
    }
    
   
    static func getFoodSearch(foodName:String?, completion: @escaping (_ list: FoodSearchResult?) -> ()) {
        
        let subUrl:String = String(format: "/foodinfo/autocomplement")
        let headers:HTTPHeaders =  createHttpHeader()
        var parameters: Parameters? = nil
            parameters = ["key": "\(foodName!)"]
        
        let fullAddress = HttpHelper.server_address+subUrl
        AF.request(fullAddress,
                                   parameters: parameters,
                                   encoding: URLEncoding.default,
                                   headers: headers).validate()
            .responseDecodable(of: FoodSearchResult.self, completionHandler: { response in
                switch response.result {
                case .success(let result):
                    if response.response?.statusCode == 200 {
                        completion(result)
                    }
                case.failure:
                    debugPrint(response)
                    completion(nil)
                }
        })
    }
    
    static func cancelAllRequests() {
        AF.session.getTasksWithCompletionHandler { (urlSessions, uploadSessions, downloadSessions) in
            urlSessions.forEach({ $0.cancel() })
            uploadSessions.forEach({ $0.cancel() })
            downloadSessions.forEach({ $0.cancel() })
        }
    }
    
     //deprecated
    static func getCustomFoodSearch(completion: @escaping (_ list: [FoodInfo]?) -> ()) {
        if HttpHelper.accessToken == "" { return }
        let subUrl:String = String(format: "/user/%d/foodInfo/", 0)
        let headers:HTTPHeaders =  createHttpHeader()
        let parameters: Parameters? = nil
        let fullAddress = HttpHelper.server_address+subUrl
        AF.request(fullAddress,
                                   parameters: parameters,
                                   encoding: URLEncoding.default,
                                   headers: headers).validate()
            .responseDecodable(of: [FoodInfo].self, completionHandler: { response in
                switch response.result {
                case .success(let list):
                    if response.response?.statusCode == 200 {
                        completion(list)
                    }
                case.failure:
                    debugPrint(response)
                    completion(nil)
                }
        })
    }
    
     //deprecated
    static func deleteCustomFoodHistory(foodId:Int, completion: @escaping (_ result: Bool) -> ()) {
        if HttpHelper.accessToken == "" {
            return
        }
        
        let subUrl: String = String(format: "/user/%d/foodInfo/%d", 0, foodId)
        let headers:HTTPHeaders =  createHttpHeader()
        let fullAddress = HttpHelper.server_address+subUrl
        AF.request(fullAddress,
                                   method: .delete,
                                   encoding: URLEncoding.default,
                                   headers: headers).validate()
            .response { response in
                if response.response?.statusCode == 200 {
                    completion(true)
                } else {
                    completion(false)
                }
        }
    }
    
     //deprecated
    static func getCustomFoodInfo(foodId:Int, completion: @escaping (_ user: FoodInfo?) -> ()) {
        if HttpHelper.accessToken == "" {
            return
        }
        
        let subUrl:String = String.init(format: "/user/%d/foodInfo/%d", 0, foodId)
        
        let headers:HTTPHeaders =  createHttpHeader()
        
        let fullAddress = HttpHelper.server_address+subUrl
        AF.request(fullAddress,
                                   encoding: JSONEncoding.default,
                                   headers: headers).validate()
            .responseDecodable(of: [FoodInfo].self, completionHandler: { response in
                //print(response)
                switch response.result {
                case .success(let list):
                    if response.response?.statusCode == 200 {
                        completion(list.first)
                    }
                case.failure:
                    completion(nil)
                }
        })
    }
    
     //deprecated
    static func addCustomFoodHistory(foodInfo: [String:Any], completion: @escaping (_ result: Bool) -> ()) -> Request? {
        if HttpHelper.accessToken == "" { return nil }
        let subUrl:String = String.init(format: "/user/%d/foodInfo/", 0)
        
        let headers:HTTPHeaders =  createHttpHeader()
        let fullAddress = HttpHelper.server_address+subUrl
        let parameters: Parameters = foodInfo
        
        return AF.request(fullAddress,
                                   method: .post,
                                   parameters: parameters,
                                   encoding: JSONEncoding.default,
                                   headers: headers
            ).validate()
            .response { response in
                if response.response?.statusCode == 201 || response.response?.statusCode == 200 {
                    completion(true)
                } else {
                    completion(false)
                }
        }
    }
    
     //deprecated
    static func updateCustomFoodHistory(foodId: Int, foodInfo: [String:Any], completion: @escaping (_ result: Bool) -> ()) -> Request? {
        
        if HttpHelper.accessToken == "" { return nil }
        let subUrl:String = String.init(format: "/user/%d/foodInfo/%d", 0, foodId)
        
        let headers:HTTPHeaders =  createHttpHeader()
        let fullAddress = HttpHelper.server_address+subUrl
        
        let parameters: Parameters = foodInfo
                
        return AF.request(fullAddress,
                                   method: .put,
                                   parameters: parameters,
                                   encoding: JSONEncoding.default,
                                   headers: headers
            ).validate()
            .response { response in
                if response.response?.statusCode == 200 {
                    completion(true)
                } else {
                    completion(false)
                }
            }
        
    }
}
