//
//  Box.swift
//  FoodLens
//
//  Created by leeint on 24/01/2019.
//  Copyright © 2019 leeint. All rights reserved.
//

import UIKit


/**
 Position of partition image in full image
 */
public struct Box: Codable {
    /**
     Minimum X coordinate of position
     */
    public var xmin:Int = 0
    
    /**
     Maximum X coordinate of position
     */
    public var xmax:Int = 0
    
    /**
     Minimum X coordinate of position
     */
    public var ymin:Int = 0
    
    /**
     Maximum X coordinate of position
     */
    public var ymax:Int = 0
    
    init(origin : Box) {
        
        xmin = origin.xmin
        xmax = origin.xmax
        ymin = origin.ymin
        ymax = origin.ymax
    }
    
    public init() {}
    
    init(xmin: Int, xmax: Int, ymin: Int, ymax: Int) {
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax
    }
    
    func resize(scale: CGFloat) -> Self {
        return .init(
            xmin: Int(Double(self.xmin) * scale),
            xmax: Int(Double(self.xmax) * scale),
            ymin: Int(Double(self.ymin) * scale),
            ymax: Int(Double(self.ymax) * scale)
        )
    }
}
