//
//  FoodPosition.swift
//  FoodLens
//
//  Created by leeint on 24/01/2019.
//  Copyright © 2019 leeint. All rights reserved.
//

import UIKit


/**
 Information unit of each food in the photo
 */
public struct FoodPosition : Codable {
    internal var isDelete : Bool? = false
    internal var s3ImagePath : String? = ""
    internal var seqLog: String? = ""
    
    /**
     absolute path of parted image
     */
    public var foodImagepath:String? = nil
    
    /**
     position of partition image in full image
     */
    public var imagePosition:Box?  = nil
    
    /**
     amount to eat, default value is 1.0
     */
    public var eatAmount : Float = 1.0;
    
    /**
     Infomation of Food of being selected by user, default value is foodCandidates[0]
     */
    public var userSelectedFood : Food? = nil;
    
    /**
     Other results of being recognised
     */
    public var foodCandidates:[Food] = []
    
    
    public init(origin : FoodPosition) {
        
        foodImagepath = origin.foodImagepath
        eatAmount = origin.eatAmount
        
        if let box = origin.imagePosition {
            imagePosition = Box(origin: box)
        }
        
        origin.foodCandidates.forEach { (food) in
            let tempFood = Food(origin: food)
            self.foodCandidates.append(tempFood)
            if origin.userSelectedFood == food {  //check if they are the same
                self.userSelectedFood = tempFood
            }
        }
    }
    
    internal mutating func validateData() -> (String, Bool) {
        if foodCandidates.count < 1 {
            return ("foodCandidates count is 0", false)
        }
        
        if userSelectedFood == nil {
            userSelectedFood = foodCandidates[0]
        }
        
        var count = foodCandidates.filter{ $0.foodName.count < 1}.count
        if count > 1 {
            return ("foodname of Food is empty", false)
        }
        
        return ("valid", true)
    }
    
    public init() {}
    
}

extension FoodPosition {
    enum CodingKeys: CodingKey {
        case isDelete, s3ImagePath
        case foodImagepath, imagePosition, eatAmount, userSelectedFood, foodCandidates
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = try encoder.container(keyedBy: CodingKeys.self)
        try container.encode(foodImagepath, forKey: .foodImagepath)
        try container.encode(imagePosition, forKey: .imagePosition)
        try container.encode(eatAmount, forKey: .eatAmount)
        try container.encode(userSelectedFood, forKey: .userSelectedFood)
        try container.encode(foodCandidates, forKey: .foodCandidates)
    }
}
