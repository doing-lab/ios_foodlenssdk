//
//  Nutrition.swift
//  FoodLens
//
//  Created by leeint on 24/01/2019.
//  Copyright © 2019 leeint. All rights reserved.
//

import UIKit


/**
 Nutritional information object of food
 */
public struct Nutrition : Codable {
    /**
     unit of food (1공기, 1컵 등등..)
     */
    public var unit:String = ""
    
    /**
     Amount of calorie that food has
     */
    public var calories:Float = 0.0
    
    /**
     Amount of carbonhydrate that food has
     */
    public var carbonhydrate:Float = 0.0
    
    /**
     Amount of dietrayfiber that food has
     */
    public var dietrayfiber:Float = 0.0
    
    /**
     Amount of fat that food has
     */
    public var fat:Float = 0.0
    
    /**
     Amount of calcium that food has
     */
    public var calcium:Float = 0.0
    
    /**
     Amount of saturatedfat that food has
     */
    public var saturatedfat:Float = 0.0
    
    /**
     Amount of sodium that food has
     */
    public var sodium:Float = 0.0
    
    /**
     Amount of sugar that food has
     */
    public var sugar:Float = 0.0
    
    /**
     Amount of transfat that food has
     */
    public var transfat:Float = 0.0
    
    /**
     Amount of vitamin A that food has
     */
    public var vitamina:Float = 0.0
    
    /**
     Amount of vitamin B that food has
     */
    public var vitaminb:Float = 0.0
    
    /**
     Amount of vitamin C that food has
     */
    public var vitaminc:Float = 0.0
    
    /**
     Amount of vitamin D that food has
     */
    public var vitamind:Float = 0.0
    
    /**
     Amount of vitamin E that food has
     */
    public var vitamine:Float = 0.0
    
    /**
     Amount of gram that food has
     */
    public var totalgram:Float = 0.0
    
    /**
     Amount of protein that food has
     */
    public var protein:Float = 0.0
    
    /**
     Amount of cholesterol that food has
     */
    public var cholesterol:Float = 0.0
    
    /**
     Type of food
     */
    public var foodtype: String? = ""
    
    init(foodInfo : FoodInfo) {
        self.unit = foodInfo.unit
        self.calories = foodInfo.calories
        self.carbonhydrate = foodInfo.carbonhydrate
        self.dietrayfiber = foodInfo.dietrayfiber
        self.fat = foodInfo.fat
        self.calcium = foodInfo.calcium
        self.saturatedfat = foodInfo.saturatedfat
        self.sodium = foodInfo.sodium
        self.sugar = foodInfo.sugar
        self.transfat = foodInfo.transfat
        self.vitamina = foodInfo.vitamina
        self.vitaminb = foodInfo.vitaminb
        self.vitaminc = foodInfo.vitaminc
        self.vitamind = foodInfo.vitamind
        self.vitamine = foodInfo.vitamine
        self.totalgram = foodInfo.totalgram
        self.protein = foodInfo.protein
        self.cholesterol = foodInfo.cholesterol
        self.foodtype = foodInfo.foodtype
    }
    
    init(origin : Nutrition) {
        
        unit = origin.unit
        calories = origin.calories
        carbonhydrate = origin.carbonhydrate
        dietrayfiber = origin.dietrayfiber
        fat = origin.fat
        calcium = origin.calcium
        saturatedfat = origin.saturatedfat
        sodium = origin.sodium
        sugar = origin.sugar
        transfat = origin.transfat
        vitamina = origin.vitamina
        vitaminb = origin.vitaminb
        vitaminc = origin.vitaminc
        vitamind = origin.vitamind
        vitamine = origin.vitamine
        totalgram = origin.totalgram
        protein = origin.protein
        cholesterol = origin.cholesterol
        foodtype = origin.foodtype
    }
    
    public init() {}
}
