//
//  Food.swift
//  FoodLens
//
//  Created by leeint on 24/01/2019.
//  Copyright © 2019 leeint. All rights reserved.
//

import UIKit


public enum ChangeKeyType : String, Codable {
    case predict // = 0 // 인식 된 음식 그대로 쓰는 것
    case db // = // 1 // 검색하기를 눌러서 선택했을 때
    case manual // = 2 // 유저가 음식을 추가한 경우
}
/**
 Information of recognized food
 */
public struct Food : Codable {
    internal var keyName:String? = ""
    
    /**
     Name of Food
     */
    public var foodName:String = ""
    
    /**
     Unique ID of the FoodLens food database
     */
    public var foodId:Int = 0
    
    /**
     Manufacturer of food
    */
    public var manufacturer:String? = ""
    
    /**
     Nutritional information of food
     */
    public var nutrition:Nutrition? = nil
    
    public var source:ChangeKeyType? = .predict
    
    init(origin : Food) {
        
        foodName = origin.foodName
        foodId = origin.foodId
        manufacturer = origin.manufacturer
        if let nut = origin.nutrition {
            nutrition = Nutrition(origin: nut)
        }
    }
    
    public init() {}
}

extension Food: Equatable {
    public static func == (lhs: Food, rhs: Food) -> Bool {
        lhs.foodId == rhs.foodId
    }
}
