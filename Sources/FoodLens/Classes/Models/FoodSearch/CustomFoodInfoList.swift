//
//  CustomFoodInfoList.swift
//  FoodLens
//
//  Created by Eunjin on 01/04/2020.
//

import UIKit


struct CustomFoodInfoList: Codable {
    var list: [String:FoodInfo] = [:]
    
    func toJsonString() -> String? {
        do {
            let jsonData: Data = try JSONEncoder().encode(self)
            return .init(data: jsonData, encoding: .utf8)
        } catch {
            return nil
        }
    }
}

extension String {
    var data:          Data  { return Data(utf8) }
    var base64Encoded: Data  { return data.base64EncodedData() }
    var base64Decoded: Data? { return Data(base64Encoded: self) }
}

extension Data {
    var string: String? { return String(data: self, encoding: .utf8) }
}
