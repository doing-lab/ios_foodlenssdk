//
//  CustomFood.swift
//  FoodLens
//
//  Created by Eunjin on 01/04/2020.
//

import UIKit

struct CustomFood: Codable {
    let key: String
    let en: String
    let ko: String
    let unit: String
}
