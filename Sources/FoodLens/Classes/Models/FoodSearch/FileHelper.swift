//
//  FileHelper.swift
//  FoodLens
//
//  Created by leeint on 2020/05/01.
//

import UIKit

class FileHelper {
    private static let customfood_filename = "customfoodlists.dat"
    
    
    static func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    
    static func writeCustomFoods(foodsDict:[String:FoodInfo], completion: @escaping (_ result: Bool) -> Void) {
    
        var customFoodInfoList : CustomFoodInfoList = CustomFoodInfoList()
        
        let fullPath = FileHelper.getDocumentsDirectory().appendingPathComponent(customfood_filename)
        customFoodInfoList.list = foodsDict
//        for food in foods {
//            customFoodInfoList.list.updateValue(food, forKey: String(food.id))
//        }
        let jsonVal = customFoodInfoList.toJsonString()
        
        do {
            //let jsonData = try JSONSerialization.data(withJSONObject: list, options: [])
            //let jsonVal = String(data:jsonData, encoding: .utf8)
            try jsonVal?.write(to: fullPath, atomically: true, encoding:.utf8)
            completion(true)
        } catch {
            print("Error info: \(error)")
            completion(false)
        }
    }
    
    static func readCustomFoods(completion: @escaping (_ list: [FoodInfo]?) -> ()) {
        
        let fullPath = FileHelper.getDocumentsDirectory().appendingPathComponent(customfood_filename)
        
        if #available(iOS 16.0, *) {
            if FileManager.default.fileExists(atPath: fullPath.path()) == false {
                completion([])
                return
            }
        } else {
            if FileManager.default.fileExists(atPath: fullPath.path) == false {
                completion([])
                return
            }
        }
        //var data = ""
        do {
            let data = try String(contentsOf: fullPath, encoding: .utf8).data
            let dict = try! JSONSerialization.jsonObject(with: data, options:[]) as! [String:Any]

            var customfoodInfoList: [FoodInfo] = []

            let testval = dict["list"] as! NSDictionary

            let finalval = testval.sorted { (first, second) -> Bool in
                print(first, second)
                return Int(first.key as! String) ?? 0 < Int(second.key as! String) ?? 0
            }

            for item in finalval {
                //item.
                customfoodInfoList.append(FoodInfo(dictionary: (item.value as? [String: Any]) ?? [:]))
            }
            completion(customfoodInfoList)
        } catch {
            print("Error info: \(error)")
        }
        
    }
}
