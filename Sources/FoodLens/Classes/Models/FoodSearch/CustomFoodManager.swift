//
//  CustomFoodManager.swift
//  FoodLens
//
//  Created by Eunjin on 01/04/2020.
//

import UIKit

class CustomFoodManager {
    
    private static let customFoodMgr : CustomFoodManager = CustomFoodManager()
    
    private var isRequest = false
    private var isLocalMode = true
    private var cachedFood:[String:FoodInfo] = [String:FoodInfo]()
    private var latestKey : Int = 0
    static func getInstance() ->CustomFoodManager {
        return customFoodMgr
    }
    
    func setLocalMode(isLocal:Bool) {
        isLocalMode = isLocal
    }
    
    func addCustomFood(foodInfo: [String:Any], completion: @escaping (_ result: Bool) -> Void) {
        if isLocalMode {
            var food : FoodInfo = FoodInfo(dictionary: foodInfo)
            food.id = latestKey + 1
            self.updateLatestKey(key:food.id)
            cachedFood.updateValue(food, forKey: String(food.id))
            FileHelper.writeCustomFoods(foodsDict: cachedFood) { result in
                completion(result)
            }
        } else {
            if !isRequest {
                isRequest = true
                HttpHelper.addCustomFoodHistory(foodInfo: foodInfo) {result in
                    completion(result)
                    self.isRequest = false
                }
            }
        }
    }
   
    
    func updateCustomFood(foodInfo : [String:Any], completion: @escaping (_ result: Bool) -> Void) {
        var food : FoodInfo = FoodInfo(dictionary: foodInfo)
        if isLocalMode {
            self.updateLatestKey(key:food.id)
            cachedFood.updateValue(food, forKey: String(food.id))
            FileHelper.writeCustomFoods(foodsDict: cachedFood) { result in
                completion(result)
            }
        } else {
            if !isRequest {
                isRequest = true
                HttpHelper.updateCustomFoodHistory(foodId: food.id, foodInfo: foodInfo) { result in
                    completion(result)
                    self.isRequest = false
                }
            }
        }
    }
    
    func makeCustomFoodList(completion: @escaping (_ getValue: [FoodInfo]) -> Void) {
        
        if isLocalMode {
            FileHelper.readCustomFoods() { (result : [FoodInfo]?) in
                guard let list = result else { return }
                for food in list {
                    self.updateLatestKey(key:food.id)
                    self.cachedFood.updateValue(food, forKey: String(food.id))
                }
                
                completion(list)
            }
        } else {
            HttpHelper.getCustomFoodSearch() { (result : [FoodInfo]?) in
                guard let list = result else { return }
                completion(list)
            }
        }
    }
    
    func deleteCustomFood(id: Int, completion: @escaping (_ getValue: Bool) -> Void) {
        if isLocalMode {
            cachedFood.removeValue(forKey: String(id))
            FileHelper.writeCustomFoods(foodsDict: cachedFood) { result in
                completion(result)
            }
        } else {
            HttpHelper.deleteCustomFoodHistory(foodId: id) { (result : Bool) in
                completion(result)
            }
        }
    }
    
    func getCustomFoodInfo(foodId:Int, completion: @escaping (_ foodInfo: FoodInfo?) -> ()) {
        
        if isLocalMode {
            let info :FoodInfo? = cachedFood[String(foodId)]
            completion(info)
        } else {
            HttpHelper.getCustomFoodInfo(foodId: foodId) { foodInfo in
                completion(foodInfo)
            }
        }
    }
    
    private func updateLatestKey(key:Int) {
        if key > latestKey {
            latestKey = key
        }
    }
}
