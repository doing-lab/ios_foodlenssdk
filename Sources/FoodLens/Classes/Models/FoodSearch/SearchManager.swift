//
//  SearchManager.swift
//  FoodLens
//
//  Created by Eunjin on 01/04/2020.
//

import UIKit

class SearchManager {
    func makeFoodSearchList(by name: String, completion: @escaping (_ getValue: [FoodSearchList]) -> Void) {
        HttpHelper.getFoodSearch(foodName: name) { (result : FoodSearchResult?) in
            var foodSearchResult: [FoodSearchList] = []
            foodSearchResult = result?.foodnamelist ?? []
            completion(foodSearchResult)
        }
    }
}
