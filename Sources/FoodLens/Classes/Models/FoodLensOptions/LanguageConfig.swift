//
//  LanguageConfig.swift
//  Alamofire
//
//  Created by 박병호 on 2023/04/24.
//

import Foundation

public enum LanguageConfig: String, Codable, CaseIterable {
    /**
     Device Setting
     */
    case device
    
    /**
     Korean
     */
    case ko
    
    /**
     English
     */
    case en
    
    var codeString: String {
        switch self {
        case .device:
            guard let localeId = Locale.preferredLanguages.first,
                  let deviceLanguage = (Locale(identifier:localeId).languageCode) else {
                return "en"
            }
            return deviceLanguage == "ko" ? deviceLanguage : "en"
        case .ko:
            return self.rawValue
        case .en:
            return self.rawValue
        }
    }
}
