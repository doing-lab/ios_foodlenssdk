//
//  AddFoodLensType.swift
//  Alamofire
//
//  Created by 박병호 on 2023/04/24.
//

import Foundation

public enum AddFoodLensType {
    case showCamera
    case showGallary
    case searchFood
}
