//
//  NavigationItemTheme.swift
//  FoodLens
//
//  Created by HwangChun Lee on 22/04/2019.
//

import UIKit

/**
 Object which is used for defining theme of navigation bar, top side of app screen
 */
open class NavigationBarTheme {
    private let bundle: Bundle = foodlensResourceBundle
    
    internal var foregroundColor : UIColor = UIColor.black
    internal var backgroundColor : UIColor = UIColor.white
    
    internal var backImage : UIImage? = UIImage(named: "pdf_arrow", in: foodlensResourceBundle, compatibleWith: nil)
    internal var flashAutoImage : UIImage? = UIImage(named: "pdf_flash_auto", in: foodlensResourceBundle, compatibleWith: nil)
    internal var flashOnImage : UIImage? = UIImage(named: "pdf_flash_on", in: foodlensResourceBundle, compatibleWith: nil)
    internal var flashOffImage : UIImage? = UIImage(named: "pdf_flash_off", in: foodlensResourceBundle, compatibleWith: nil)
    internal var closeImage : UIImage? = UIImage(named: "pdf_close", in: foodlensResourceBundle, compatibleWith: nil)
    
    /**
     Default constructor, foreground color and background color are defined as default
     */
    public init() {
        /*_ = CALayer(SVGData: NSDataAsset(name: "svg_close", bundle: bundle)!.data) { (svgLayer) in
            svgLayer.fillColor = UIColor.clear.cgColor
            svgLayer.strokeColor = self.foregroundColor.cgColor
            svgLayer.lineWidth = 1.5
            UIGraphicsBeginImageContextWithOptions(CGSize(width: 17.313, height: 17.313), svgLayer.isOpaque, 0.0)
            svgLayer.render(in: UIGraphicsGetCurrentContext()!)
            self.closeImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
        }
        
        _ = CALayer(SVGData: NSDataAsset(name: "svg_arrow_left", bundle: bundle)!.data) { (svgLayer) in
            svgLayer.fillColor = UIColor.clear.cgColor
            svgLayer.strokeColor = self.foregroundColor.cgColor
            svgLayer.lineWidth = 1.5
            UIGraphicsBeginImageContextWithOptions(CGSize(width: 11, height: 21), svgLayer.isOpaque, 0.0)
            svgLayer.render(in: UIGraphicsGetCurrentContext()!)
            self.backImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
        }
        
        _ = CALayer(SVGData: NSDataAsset(name: "svg_flash_auto", bundle: bundle)!.data) { (svgLayer) in
            svgLayer.fillColor = self.foregroundColor.cgColor
            UIGraphicsBeginImageContextWithOptions(svgLayer.boundingBox.size, svgLayer.isOpaque, 0.0)
            svgLayer.render(in: UIGraphicsGetCurrentContext()!)
            self.flashAutoImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
        }
        
        _ = CALayer(SVGData: NSDataAsset(name: "svg_flash_off", bundle: bundle)!.data) { (svgLayer) in
            svgLayer.fillColor = self.foregroundColor.cgColor
            UIGraphicsBeginImageContextWithOptions(svgLayer.boundingBox.size, svgLayer.isOpaque, 0.0)
            svgLayer.render(in: UIGraphicsGetCurrentContext()!)
            self.flashOffImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
        }
        
        _ = CALayer(SVGData: NSDataAsset(name: "svg_flash_on", bundle: bundle)!.data) { (svgLayer) in
            svgLayer.fillColor = self.foregroundColor.cgColor
            UIGraphicsBeginImageContextWithOptions(svgLayer.boundingBox.size, svgLayer.isOpaque, 0.0)
            svgLayer.render(in: UIGraphicsGetCurrentContext()!)
            self.flashOnImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
        }*/
    }
    
    /**
     Constructor, foreground color and background color are defined
     
     - parameter foregroundColor : color of text and button
     - parameter backgroundColor : color of background
     
     */
    public init(foregroundColor : UIColor, backgroundColor : UIColor) {
        self.foregroundColor = foregroundColor
        self.backgroundColor = backgroundColor
        
        /*_ = CALayer(SVGData: NSDataAsset(name: "svg_close", bundle: bundle)!.data) { (svgLayer) in
            svgLayer.fillColor = UIColor.clear.cgColor
            svgLayer.strokeColor = self.foregroundColor.cgColor
            svgLayer.lineWidth = 1.5
            UIGraphicsBeginImageContextWithOptions(CGSize(width: 17.313, height: 17.313), svgLayer.isOpaque, 0.0)
            svgLayer.render(in: UIGraphicsGetCurrentContext()!)
            self.closeImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
        }
        
        _ = CALayer(SVGData: NSDataAsset(name: "svg_arrow_left", bundle: bundle)!.data) { (svgLayer) in
            svgLayer.fillColor = UIColor.clear.cgColor
            svgLayer.strokeColor = self.foregroundColor.cgColor
            svgLayer.lineWidth = 1.5
            UIGraphicsBeginImageContextWithOptions(CGSize(width: 11, height: 21), svgLayer.isOpaque, 0.0)
            svgLayer.render(in: UIGraphicsGetCurrentContext()!)
            self.backImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
        }
        
        _ = CALayer(SVGData: NSDataAsset(name: "svg_flash_auto", bundle: bundle)!.data) { (svgLayer) in
            svgLayer.fillColor = self.foregroundColor.cgColor
            UIGraphicsBeginImageContextWithOptions(svgLayer.boundingBox.size, svgLayer.isOpaque, 0.0)
            svgLayer.render(in: UIGraphicsGetCurrentContext()!)
            self.flashAutoImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
        }
        
        _ = CALayer(SVGData: NSDataAsset(name: "svg_flash_off", bundle: bundle)!.data) { (svgLayer) in
            svgLayer.fillColor = self.foregroundColor.cgColor
            UIGraphicsBeginImageContextWithOptions(svgLayer.boundingBox.size, svgLayer.isOpaque, 0.0)
            svgLayer.render(in: UIGraphicsGetCurrentContext()!)
            self.flashOffImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
        }
        
        _ = CALayer(SVGData: NSDataAsset(name: "svg_flash_on", bundle: bundle)!.data) { (svgLayer) in
            svgLayer.fillColor = self.foregroundColor.cgColor
            UIGraphicsBeginImageContextWithOptions(svgLayer.boundingBox.size, svgLayer.isOpaque, 0.0)
            svgLayer.render(in: UIGraphicsGetCurrentContext()!)
            self.flashOnImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
        }*/
    }
}

/**
 Object which is used for defining theme of bottom side
 */
open class ToolBarButtonTheme {
    internal var backgroundColor : UIColor = UIColor.white
    internal var buttonTheme : ButtonTheme = ButtonTheme(backgroundColor: UIColor(red:0.80, green:0.17, blue:0.18, alpha:1.0), textColor: UIColor.white, borderColor: UIColor.clear)
    
    /**
     Default constructor, the themes of background color and button are defined as default
     */
    public init() {
        
    }
    
    /**
     Constructor, background color and buttonTheme are defined
     
     - parameter backgroundColor : color of background
     - parameter buttonTheme : theme of button
     */
    public init(backgroundColor : UIColor, buttonTheme : ButtonTheme) {
        self.backgroundColor = backgroundColor
        self.buttonTheme = buttonTheme
    }
}

/**
 Object which is used for defining theme of navigation bar, top side of app screen
 */
open class ButtonTheme {
    var backgroundColor : UIColor =  UIColor(red:0.04, green:0.36, blue:0.42, alpha:1.0) //UIColor(red:0.80, green:0.17, blue:0.18, alpha:1.0)
    var textColor : UIColor = UIColor(red:0.04, green:0.36, blue:0.42, alpha:1.0)
    var borderColor : UIColor = UIColor(red:0.04, green:0.36, blue:0.42, alpha:1.0)
    
    /**
     Default constructor, the themes of button is defined as default
     */
    public init() {
        NSLog("default")
    }
    
    /**
     Constructor, background color and buttonTheme are defined
     
     - parameter backgroundColor : color of background
     - parameter textColor : color of text
     - parameter borderColor : color of border
     */
    public init(backgroundColor : UIColor, textColor : UIColor, borderColor : UIColor) {
        self.backgroundColor = backgroundColor
        self.textColor = textColor
        self.borderColor = borderColor
    }
}
