//
//  ImageAreaButton.swift
//  FoodLens
//
//  Created by HwangChun Lee on 15/03/2019.
//

import UIKit

@available(iOS 10.0, *)
class ImageAreaButton: UIButton {
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentEdgeInsets = UIEdgeInsets(top: 3, left: 5, bottom: 3, right: 5)
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 12
        self.setTitleColor(ColorTable.mainWhite, for: .normal)
        self.setTitleColor(ColorTable.mainWhite, for: .selected)
    }
    
    func setData(title : String, backgroundColor : UIColor, position : CGRect, tag : Int) {
        self.semanticContentAttribute = .forceRightToLeft
        self.titleLabel!.textAlignment = .center
        self.titleLabel!.font = self.titleLabel!.font.withSize(13)
        self.titleLabel!.textAlignment = .center
        self.setTitle(title + "  ", for: .normal)
        self.layer.backgroundColor = backgroundColor.cgColor
        self.tag = tag
        self.sizeToFit()
        
        self.frame.origin.x = position.minX + (position.width/2 - self.frame.width/2)
        self.frame.origin.y = position.minY + (position.height/2 - self.frame.height/2)
    }
}
