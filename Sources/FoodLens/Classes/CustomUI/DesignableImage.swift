//
//  DesignableImage.swift
//  DietCameraAI
//
//  Created by HwangChun Lee on 31/12/2018.
//  Copyright © 2018 leeint. All rights reserved.
//

import UIKit

//
//  ButtonExtension.swift
//  DietDiaryAI
//
//  Created by HwangChun Lee on 2018. 8. 3..
//  Copyright © 2018년 leeint. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableImageView: UIImageView {
    @IBInspectable var borderWidth: CGFloat {
        
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}
