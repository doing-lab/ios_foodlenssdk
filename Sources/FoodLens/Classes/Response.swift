//
//  Response.swift
//  FoodLens
//
//  Created by HwangChun Lee on 22/03/2019.
//

import UIKit

/**
 Returned object when error is occurred
 */
public class BaseError {
    private var message : String = ""
    
    init(message : String) {
        self.message = message
    }
    
    /**
     Get error message
     
     - Returns : String that describes which error is occurred
     */
    public func getMessage() -> String {
        return message
    }
}

/**
 Returned protocol to be passed when recognition is successfully ended
 */
public protocol RecognitionResult {
    /**
     Get Path of recognized image
     
     **Note**: This function returns nil when user uses 'Manual Input'
     
     */
    func getRecognizedImage() -> String?
    
    /**
     Get FoodPosition object that 'index' indicates
     
     - parameter index : index to get
     
     **Note**: This function returns nil when index is over the length of FoodPosition object list
     
     */
    func getFoodPositionAtIndex(index : Int) -> FoodPosition?
    
    /**
     Get whole member of FoodPosition object list
     
     **Note**: This function returns nil when user uses 'Manual Input'
     
     */
    func getFoodPositions() -> [FoodPosition]
    
    /**
     Put FoodPosition object into the end of FoodPosition object list
     
     - parameter food : FoodPosition object that intends to put
     
     **Note**: This function returns nil when user uses 'Manual Input'
     
     */
    func putFoodPosition(_ food : FoodPosition)
    
    /**
     Set Path of recognizeed image
     
     - parameter path : Absolute path of image to store
     
     */
    func setRecognizedImagePath(_ path : String)
    
    /**
     Get type of meal
     
     - returns: MealType value (.breakfast, .lunch, .dinner. .snack)
     */
    func getMealType() -> MealType

    /**
     Set type of meal
     
     - returns: MealType value (.breakfast, .lunch, .dinner. .snack)
     */
    func setMealType(_ type : MealType)

    /**
     Get date of meal
     
     - returns: Date
    */
    
    func getDate() -> Date

    /**
     Set date of meal
     
     - returns: Date
    */
    func setDate(_ date : Date)

    
    /**
     Get JSON string of result
     
     - returns: String
     */
    
    func toJSONString() -> String?
}


/**
 Delegate Protocal to be called when recognition process is ended
 */
public protocol UserServiceResultHandler {
    /**
     Method which is called when recognition is succeeded
     
     - parameter result : result of recognition
     */
    func onSuccess(_ result : RecognitionResult)
    
    /**
     Method which is called when X button is pressed in camera view controller
     */
    func onCancel()
    
    /**
     Method which is called when an error is occurred
     
     - parameter error : reason of error
     */
    func onError(_ error : BaseError)
}
