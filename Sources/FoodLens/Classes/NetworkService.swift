//
//  NetworkService.swift
//  FoodLens
//
//  Created by leeint on 23/01/2019.
//  Copyright © 2019 doinglab. All rights reserved.
//

import UIKit

/**
 Set amount of nutritional data to receive
 */
public enum NutritionRetrieveMode {
    /**
     Don't return any nutrition data
     */
    case noNutrition
    /**
     Return nutrition of only top1 candidate of each box
     */
    case top1NutritionOnly
    /**
     Return nutrition of whole candidate
     */
    case allNutirition
}

public extension UIImage {
    func croppedImage(inRect rect: CGRect) -> UIImage {
        let rad: (Double) -> CGFloat = { deg in
            return CGFloat(deg / 180.0 * .pi)
        }
        var rectTransform: CGAffineTransform
        switch imageOrientation {
        case .left:
            let rotation = CGAffineTransform(rotationAngle: rad(90))
            rectTransform = rotation.translatedBy(x: 0, y: -size.height)
        case .right:
            let rotation = CGAffineTransform(rotationAngle: rad(-90))
            rectTransform = rotation.translatedBy(x: -size.width, y: 0)
        case .down:
            let rotation = CGAffineTransform(rotationAngle: rad(-180))
            rectTransform = rotation.translatedBy(x: -size.width, y: -size.height)
        default:
            rectTransform = .identity
        }
        rectTransform = rectTransform.scaledBy(x: scale, y: scale)
        let transformedRect = rect.applying(rectTransform)
        guard let imageRef = cgImage?.cropping(to: transformedRect) else {
            return UIImage()
        }
        let result = UIImage(cgImage: imageRef, scale: scale, orientation: imageOrientation)
        return result
    }
}
/**
 Set amount of food data to receive
 */
public enum UIServiceMode {
    /**
    Retrieve nutrition information for one of top confidence candidate.
    */
    case userSelectedOnly
    /**
    Retrieve nutrition information for all candidates.
     */
    case userSelectedWithCandidates
}

@available(iOS 10.0, *)
open class NetworkService {
    internal static let CROP_IMG_MAX_SIZE:Double = 3500
    internal static let MULTI_PREDICT_IMG_SIZE:Int = 1024
    private static let SINGLE_PREDICT_IMG_MAX_SIZE:Int = 512
    private static let SINGLE_PREDICT_IMG_MIN_SIZE:Int = 299
    private static let IMG_STORE_PATH:String = "foodlensStore";
    
    internal var saveOriginalImage:Bool = true;
    internal var saveEachFoodImage:Bool = true;
    internal var nutritionRetrieveMode:NutritionRetrieveMode = .allNutirition
    
    private var candidateImage:UIImage?
    private var resizedImage:UIImage?
    private var transmitImageSize : CGSize?
    private var originalImageSize : CGSize?
    private var compleationFunc: ((PredictionResult?,ProcessStatus) -> ())?
    private var predictinoStatus:ProcessStatus = ProcessStatus()
    private var finalResult:PredictionResult = PredictionResult()
    private var itemCount:Int = 0
    private var totalFoodCount:Int = 0
    
    init() {
    }

    private func initValues() {
        candidateImage = nil
        resizedImage = nil
        transmitImageSize = nil
        originalImageSize = nil
        compleationFunc = nil
        predictinoStatus = ProcessStatus()
        finalResult = PredictionResult()
        finalResult.setCurrentVersion()
        itemCount = 0
        totalFoodCount = 0
        Util.deleteDirectory(path : NetworkService.IMG_STORE_PATH)
    }
    /// Predict multiple foods from UIImage. UIImage should contain food image.
    ///
    /// ---- NOTE ----
    /// Prediction method does not support multi thread environment.
    /// DO NOT DUPLICATE CALL until previous call will be returned.
    ///
    /// - parameters:
    ///   - image: UIImage which contains food image. If Image size is not big enough, prediction result can be worse than big image.
    ///   - completion: Callback method to get a prediction result.
    ///
    /// - returns: None
    public func predictMultipleFood(image : UIImage, completion: @escaping (_ result: PredictionResult?,_ status:ProcessStatus) -> ()) {
    
        initValues()
        compleationFunc = completion

        if (HttpHelper.accessToken == nil) && (HttpHelper.appToken == nil || HttpHelper.companyToken == nil) {
            predictinoStatus.state = .fail
            predictinoStatus.reasion = .invalidAccessTokenError
            compleationFunc!(nil,predictinoStatus)
            return
        }
        
        self.originalImageSize = image.size

        if saveOriginalImage {
            finalResult.predictedImagePath = Util.save(image: image, path: NetworkService.IMG_STORE_PATH, fileName: "fullImage.jpg")
        }
        
        let image = image.resize(maxLengthOfShortSide: NetworkService.CROP_IMG_MAX_SIZE)
        
        candidateImage = image
        
        let tempImg:UIImage = candidateImage!.resizeImage(targetSize: CGSize(width: NetworkService.MULTI_PREDICT_IMG_SIZE, height: NetworkService.MULTI_PREDICT_IMG_SIZE))
        transmitImageSize = tempImg.size
        resizedImage = tempImg
        
        let imgData = tempImg.jpegData(compressionQuality: 0.9)
        let countryCode = FoodLens.language.codeString
        
        
        HttpHelper.multiPredictImage(imageData:imgData,locale:countryCode, completion: multiPredictResult)
        
    }
    
    /// Predict single food from UIImage. Byte array should contain food image.
    ///
    /// ---- NOTE ----
    /// Prediction method does not support multi thread environment.
    /// DO NOT DUPLICATE CALL until previous call will be returned.
    ///
    /// - parameters:
    ///   - image: UIImage which contains food image. If Image size is not big enough, prediction result can be worse than big image.
    ///   - completion: Callback method to get a prediction result.
    ///
    /// - returns: None
    public func predictSingleFood(image : UIImage, completion: @escaping (_ result: PredictionResult?,_ status:ProcessStatus) -> ()) {
        
        initValues()
        compleationFunc = completion
        
        if (HttpHelper.accessToken == nil) || (HttpHelper.appToken == nil || HttpHelper.companyToken == nil) {
            predictinoStatus.state = .fail
            predictinoStatus.reasion = .invalidAccessTokenError
            compleationFunc!(nil,predictinoStatus)
            return
        }
        
        candidateImage = image
        
        if saveOriginalImage {
            finalResult.predictedImagePath = Util.save(image: image, path: NetworkService.IMG_STORE_PATH, fileName: "fullImage.jpg")
        }
        
        var tempBox:Box = Box()
        var pos:FoodPosition = FoodPosition()
        
        tempBox.xmin = 0
        tempBox.xmax = Int(self.candidateImage!.size.width)
        tempBox.ymin = 0
        tempBox.ymax = Int(self.candidateImage!.size.height)
        pos.imagePosition = tempBox
        finalResult.foodPositionList.append(pos)
        
        let xmax = CGFloat(tempBox.xmax)
        let ymax = CGFloat(tempBox.ymax)
        
        if saveEachFoodImage {
            pos.foodImagepath = finalResult.predictedImagePath
        }
        
        //let area : MultiPredictBoxes = box
        var croppedImage:UIImage? = nil
        if xmax > ymax {
            croppedImage = candidateImage!.cropToBounds(width: ymax, height: ymax)
        } else {
            croppedImage = candidateImage!.cropToBounds(width: xmax, height: xmax)
        }
        
        var resizedImage:UIImage = croppedImage!
        if resizedImage.size.height >= CGFloat(NetworkService.SINGLE_PREDICT_IMG_MAX_SIZE) ||  resizedImage.size.width >= CGFloat(NetworkService.SINGLE_PREDICT_IMG_MAX_SIZE) {
            resizedImage = resizedImage.resizeImage(targetSize: CGSize(width: CGFloat(NetworkService.SINGLE_PREDICT_IMG_MAX_SIZE), height: CGFloat(NetworkService.SINGLE_PREDICT_IMG_MAX_SIZE)))
        }
        let imgData = resizedImage.jpegData(compressionQuality: 0.9)
        
        self.itemCount = 1
        predictImage(imgData: imgData, position: pos, filename: finalResult.predictedImagePath, completeHandler: {(s3ImagePath, foodCandidates, seq) in
            pos.s3ImagePath = s3ImagePath
            pos.foodCandidates.append(contentsOf: foodCandidates)
            pos.seqLog = seq
        })
        
    }
    
    /// Search food names from food database.
    /// If
    ///
    /// ---- NOTE ----
    /// DO NOT DUPLICATE CALL until previous call will be returned.
    ///  If you call several times, we are NOT garantee sequence of response.
    ///
    /// - parameters:
    ///   - foodName : Name of food for serching
    ///   - completion: Callback method to get a food name result.
    ///
    /// - returns: None
    public func getFoodsByName(foodName:String, completion: @escaping (_ list: [FoodSearchList]) -> ()) {
        HttpHelper.getFoodSearch(foodName: foodName) { (result : FoodSearchResult?) in
            var foodSearchResult: [FoodSearchList] = []
            foodSearchResult = result?.foodnamelist ?? []
            completion(foodSearchResult)
        }
    }
    
    /// Get nutritional information of specified food id
    ///
    /// ---- NOTE ----
    /// Prediction method does not support multi thread environment.
    /// DO NOT DUPLICATE CALL until previous call will be returned.
    ///
    /// - parameters:
    ///   - foodId : Unique id of food
    ///   - completion: Callback method to get a nutritional result.
    ///
    /// - returns: None
    public func getNutritionInfo(foodId:Int, completion: @escaping (_ nutrition: Nutrition?, _ status:ProcessStatus) -> ()) {
        HttpHelper.getFoodInfo(foodId:foodId , completion: { foodInfo, reasion in
            self.totalFoodCount -= 1
            
            if foodInfo != nil {
                
                var nutrition:Nutrition = Nutrition()
                self.copyNutritionData(userNut: &nutrition, rawNut: foodInfo!)
                let status:ProcessStatus = ProcessStatus()
                completion(nutrition, status)
            } else {
                let status:ProcessStatus = ProcessStatus()
                status.state = .fail
                status.reasion = reasion
                completion(nil, status)
            }
        })
    }
    
    
    
    private func overlappedBoxProcessing(boxes:[MultiPredictBoxes]) -> [MultiPredictBoxes] {
        //여기서 겹친 부분을 확인
        //겹쳐진 넓이를 확인하고
        //50% 이상 차지할 경우 타원으로 수정
        
        //duplicate new array
        var retBoxes:[MultiPredictBoxes] = boxes.map{ $0 }
        
        for i in 0..<retBoxes.count {
            for otherBox in boxes {
                var overlappedBox = MultiPredictBoxes()
                if retBoxes[i] == otherBox {
                    continue
                }
                
                //xmin check.
                overlappedBox.xmin = max(retBoxes[i].xmin, otherBox.xmin)
                overlappedBox.xmax = min(retBoxes[i].xmax, otherBox.xmax)
                
                overlappedBox.ymin = max(retBoxes[i].ymin, otherBox.ymin)
                overlappedBox.ymax = min(retBoxes[i].ymax, otherBox.ymax)
                
                let area = overlappedBox.area
                if 0 < area {
                    retBoxes[i].overlappedArea.append(overlappedBox)
                }
            }
            
            let totalOverlappedArea = retBoxes[i].overlappedArea.reduce(0) { (totalArea : Int, overlappedBox) -> Int in
                var totalArea = totalArea
                totalArea += overlappedBox.area
                return totalArea
            }
            
            if (retBoxes[i].area / 2) <= totalOverlappedArea {
                retBoxes[i].isCropedAsEllipse = true
            }
        }
        
        return retBoxes
    }
    
    private func multiPredictResult(result : MultiPredictResult?, reasion : ProcessReason) {
        if predictinoStatus.state == .success {
            if reasion !=  .success {
                predictinoStatus.state = .fail
                predictinoStatus.reasion = reasion
                compleationFunc!(nil, predictinoStatus)
                return
            }
        } else {
            return
        }
        var multiPredictResult: MultiPredictResult? = result
        finalResult.fullImageS3Path = result?.filename ?? ""
        
        itemCount = result?.boxes.count ?? 0
        
        if itemCount == 0 {
            var box = MultiPredictBoxes()
            box.xmin = 0
            box.xmax = Int(transmitImageSize?.height ?? 0)
            box.ymin = 0
            box.ymax = Int(transmitImageSize?.width ?? 0)
            multiPredictResult?.boxes.append(box)
        }
        
        if let boxes = multiPredictResult?.boxes {
            multiPredictResult?.boxes = overlappedBoxProcessing(boxes:boxes)
        }
        
        let candidateImage = self.candidateImage ?? UIImage()
        
        if let boxes = multiPredictResult?.boxes {
            let count = boxes.count
            for i in 0 ..< count {
                var tempBox:Box = Box()
                var pos:FoodPosition = FoodPosition()
                var box = boxes[i]
                let xTransformValue = candidateImage.size.width / (transmitImageSize?.width ?? 1024)
                let yTransformValue = candidateImage.size.height / (transmitImageSize?.height ?? 1024)
                
                tempBox.xmin = Int(CGFloat(box.xmin) * xTransformValue)
                tempBox.xmax = Int(CGFloat(box.xmax) * xTransformValue)
                tempBox.ymin = Int(CGFloat(box.ymin) * yTransformValue)
                tempBox.ymax = Int(CGFloat(box.ymax) * yTransformValue)
                pos.imagePosition = tempBox.resize(scale: (self.originalImageSize?.width ?? 3000) / (candidateImage.size.width))
                
                var xmin = CGFloat(box.xmin)
                var xmax = CGFloat(box.xmax)
                var ymin = CGFloat(box.ymin)
                var ymax = CGFloat(box.ymax)
                
                let width = xmax - xmin
                let height = ymax - ymin
                
                xmin = max(0, xmin - 0.05 * width) * xTransformValue
                xmax = (xmax + 0.05 * width) * xTransformValue
                ymin = max(0, ymin - 0.05 * height) * yTransformValue
                ymax = (ymax + 0.05 * height) * yTransformValue
                
                xmin = max (0, xmin)
                xmax = min (self.candidateImage!.size.width-1, xmax)
                ymin = max (0, ymin)
                ymax = min (self.candidateImage!.size.height-1, ymax)
                
                for i in 0..<box.overlappedArea.count {
                    let overlapped = box.overlappedArea[i]
                    var ov_xmin = CGFloat(overlapped.xmin) * xTransformValue
                    var ov_xmax = CGFloat(overlapped.xmax) * xTransformValue
                    var ov_ymin = CGFloat(overlapped.ymin) * yTransformValue
                    var ov_ymax = CGFloat(overlapped.ymax) * yTransformValue
                    
                    if box.xmin == overlapped.xmin {
                        ov_xmin = xmin
                    }
                    
                    if box.xmax == overlapped.xmax {
                        ov_xmax = xmax
                    }
                    
                    if box.ymin == overlapped.ymin {
                        ov_ymin = ymin
                    }
                    
                    if box.ymax == overlapped.ymax {
                        ov_ymax = ymax
                    }
                    
                    box.overlappedArea[i].xmin = Int(ov_xmin - xmin) - 1
                    box.overlappedArea[i].xmax = Int(ov_xmax - xmin) + 1
                    box.overlappedArea[i].ymin = Int(ov_ymin - ymin) - 1
                    box.overlappedArea[i].ymax = Int(ov_ymax - ymin) + 1
                }
                
                let showImage:UIImage = self.candidateImage!.croppedImage(inRect: CGRect(x: xmin, y: ymin, width: (xmax-xmin), height: (ymax-ymin)))
                /*let showImage:UIImage = Util.cropToBounds(image: self.candidateImage!,
                                                        posX: xmin,
                                                        posY: ymin,
                                                        width: (xmax-xmin),
                                                        height: ymax-ymin)
                */
                let imageSize = showImage.size
                UIGraphicsBeginImageContext(imageSize)
                if box.isCropedAsEllipse == true {
                    UIGraphicsBeginImageContextWithOptions(imageSize, false, 0.0)
                    let context = UIGraphicsGetCurrentContext()
                    
                    showImage.draw(in: CGRect(origin: CGPoint.zero, size: imageSize), blendMode: .copy, alpha: 1.0)
                    
                    context!.setBlendMode(.copy)
                    context!.setFillColor(UIColor.white.cgColor)
                    
                    let rectPath = UIBezierPath(rect: CGRect(origin: CGPoint.zero, size: imageSize))
                    let circlePath = UIBezierPath(ovalIn: CGRect(origin: CGPoint.zero, size: imageSize))
                    rectPath.append(circlePath)
                    rectPath.usesEvenOddFillRule = true
                    rectPath.fill()
                }
                else {
                    let areaSize = CGRect(x: 0, y: 0, width: imageSize.width, height: imageSize.height)
                    showImage.draw(in: areaSize)
                    for overlapped in box.overlappedArea {
                        let overlappedSize = CGRect(x: overlapped.xmin, y: overlapped.ymin, width: overlapped.width+1, height: overlapped.height+1)
                        let overlappedImage = UIImage.from(color: UIColor.white)
                        //overlappedImage.size = CGSize(width: overlapped.width, height: overlapped.height)
                        overlappedImage.draw(in: overlappedSize)
                    }
                }
                let cropImg = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
                UIGraphicsEndImageContext()
                
                if saveEachFoodImage {
                    pos.foodImagepath = Util.save(image: showImage, path: NetworkService.IMG_STORE_PATH, fileName: "food_\(i).jpg")
                }
                
                let imageWidth = max(CGFloat(NetworkService.SINGLE_PREDICT_IMG_MIN_SIZE), xmax - xmin)
                let imageHeight = max(CGFloat(NetworkService.SINGLE_PREDICT_IMG_MIN_SIZE), ymax - ymin)
                let spuareBorder = max(imageWidth, imageHeight)
                
                let tempImage:UIImage = cropImg.resizeImageWithBlank(
                                                                  targetSize: CGSize(width: spuareBorder,
                                                                                     height: spuareBorder)) ?? UIImage()
                
                var resizedImage:UIImage = tempImage
                if resizedImage.size.height >= CGFloat(NetworkService.SINGLE_PREDICT_IMG_MAX_SIZE) ||  resizedImage.size.width >= CGFloat(NetworkService.SINGLE_PREDICT_IMG_MAX_SIZE) {
                    resizedImage = tempImage.resizeImage(targetSize: CGSize(width: CGFloat(NetworkService.SINGLE_PREDICT_IMG_MAX_SIZE), height: CGFloat(NetworkService.SINGLE_PREDICT_IMG_MAX_SIZE)))
                }
                let imgData = resizedImage.jpegData(compressionQuality: 0.9)
                predictImage(imgData: imgData, position: pos, filename: multiPredictResult?.filename) { s3ImagePath, foodCandidates, seq in
                    pos.s3ImagePath = s3ImagePath
                    pos.foodCandidates.append(contentsOf: foodCandidates)
                    pos.seqLog = seq
                    self.finalResult.foodPositionList.append(pos)
                }
            }
        }
    }
    
    private func predictImage(imgData:Data?, position: FoodPosition, filename: String?, completeHandler: @escaping (String, [Food], String) -> Void ) {
        let countryCode = FoodLens.language.codeString
        
        HttpHelper.predictImage(imageData: imgData, locale:countryCode, filename: filename, completion: { [weak self] predictResult, reasion in
            guard let `self` = self else {
                return
            }

            if self.predictinoStatus.state == .success {
                if reasion !=  .success {
                    self.predictinoStatus.state = .fail
                    self.predictinoStatus.reasion = reasion
                    self.compleationFunc!(nil, self.predictinoStatus)
                    return
                }
            } else {
                return
            }
            
            self.itemCount -= 1
            var foodCandidates: [Food] = .init()
            if let predictList = predictResult?.predictlist {
                for predictCount in 0 ..< predictList.count {
                    let predict = predictList[predictCount]
                    for count in 0 ..< (predict.foodlist?.count ?? 0) {
                        let candidate = predict.foodlist![count]
                        var food:Food = Food()
                        food.foodId = candidate.id
                        food.foodName = candidate.foodname
                        food.manufacturer = candidate.manufacturer
                        food.keyName = predict.keyname
                        foodCandidates.append(food)
                    }
                }
            }
            else {
                if self.predictinoStatus.state == .success {
                    self.predictinoStatus.state = .fail
                    self.predictinoStatus.reasion = .noPredictionInfoError
                    self.compleationFunc!(nil, self.predictinoStatus)
                }
            }
            
            completeHandler(
                predictResult?.filename ?? "",
                foodCandidates,
                predictResult?.seqLog ?? ""
            )
            
            if self.itemCount <= 0 && self.predictinoStatus.state == .success {
                if self.nutritionRetrieveMode == .noNutrition {
                    
                    self.compleationFunc!(self.finalResult, self.predictinoStatus)
                } else {
                    self.getFoodNutritions()
                }
            }
        })
    }
  
    
    private func getFoodNutritions() {
        
        if nutritionRetrieveMode == .allNutirition {
            for idx in 0..<finalResult.foodPositionList.count {
                let tempPos:FoodPosition = finalResult.foodPositionList[idx]
                totalFoodCount += tempPos.foodCandidates.count
            }
            
            for idx in 0..<finalResult.foodPositionList.count {
                for foodIdx in 0..<finalResult.foodPositionList[idx].foodCandidates.count {
                    getNutritionInfo(food:finalResult.foodPositionList[idx].foodCandidates[foodIdx], completion: { foodInfo, reasion in
                        self.totalFoodCount -= 1
                        
                        if foodInfo != nil {
                            var nutrition:Nutrition = Nutrition()
                            self.copyNutritionData(userNut: &nutrition, rawNut: foodInfo!)
                            if self.finalResult.foodPositionList[safe: idx] != nil,
                               self.finalResult.foodPositionList[safe: idx]?.foodCandidates[safe: foodIdx] != nil {
                                self.finalResult.foodPositionList[idx].foodCandidates[foodIdx].nutrition = nutrition
                            }
                        }
                        
                        if self.totalFoodCount <= 0 {
                            self.compleationFunc!(self.finalResult, self.predictinoStatus)
                        }
                    })
                }
            }
        } else if nutritionRetrieveMode == .top1NutritionOnly {

            //Count item
            for idx in 0..<finalResult.foodPositionList.count {
                let tempPos:FoodPosition = finalResult.foodPositionList[idx]
                if tempPos.foodCandidates.count > 0 {
                    totalFoodCount += 1
                }
            }
            
            for idx in 0..<finalResult.foodPositionList.count {
                if finalResult.foodPositionList[idx].foodCandidates.count > 0 {
                    getNutritionInfo(food:finalResult.foodPositionList[idx].foodCandidates[0]) { foodInfo, reasion in
                        self.totalFoodCount -= 1
                        
                        if foodInfo != nil {
                            var nutrition:Nutrition = Nutrition()
                            self.copyNutritionData(userNut: &nutrition, rawNut: foodInfo!)
                            if self.finalResult.foodPositionList[safe: idx] != nil,
                               self.finalResult.foodPositionList[safe: idx]?.foodCandidates[safe: 0] != nil {
                                self.finalResult.foodPositionList[idx].foodCandidates[0].nutrition = nutrition
                            }
                        }
                        
                        if self.totalFoodCount <= 0 {
                            self.compleationFunc!(self.finalResult, self.predictinoStatus)
                        }
                    }
                }
            }
        }
    }
    
    private func copyNutritionData(userNut: inout Nutrition, rawNut:FoodInfo) {
        userNut.calcium = rawNut.calcium
        userNut.calories = rawNut.calories
        userNut.carbonhydrate = rawNut.carbonhydrate
        userNut.cholesterol = rawNut.cholesterol
        userNut.dietrayfiber = rawNut.dietrayfiber
        userNut.fat = rawNut.fat
        userNut.protein = rawNut.protein
        userNut.saturatedfat = rawNut.saturatedfat
        userNut.sodium = rawNut.sodium
        userNut.sugar = rawNut.sugar
        userNut.totalgram = rawNut.totalgram
        userNut.transfat = rawNut.transfat
        userNut.vitamina = rawNut.vitamina
        userNut.vitaminb = rawNut.vitaminb
        userNut.vitaminc = rawNut.vitaminc
        userNut.vitamind = rawNut.vitamind
        userNut.vitamine = rawNut.vitamine
        userNut.totalgram = rawNut.totalgram
        userNut.unit = rawNut.unit
        userNut.foodtype = rawNut.foodtype
    }
    
    private func getNutritionInfo(food: Food,  completion: @escaping (_ user: FoodInfo?, _ reason : ProcessReason) -> ())  {
        HttpHelper.getFoodInfo(foodId:food.foodId, completion: completion)
    }
}
